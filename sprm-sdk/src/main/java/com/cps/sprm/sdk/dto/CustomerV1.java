package com.cps.sprm.sdk.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sumeshs on 11/8/2016.
 */
public class CustomerV1 implements Serializable,Comparable<CustomerV1> {
    
    private Long customerid;

    private String customerphonenumber;
    private String name;
    private String email;
    private Date dob;
    private String address;
    private Set<ServiceProviderV1> myfavourites = new HashSet<ServiceProviderV1>(0);

    private Set<AppointmentV1> myappointments = new HashSet<AppointmentV1>();

    public String getCustomerphonenumber() {
        return customerphonenumber;
    }

    public void setCustomerphonenumber(String customerphonenumber) {
        this.customerphonenumber = customerphonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<ServiceProviderV1> getMyfavourites() {
        return myfavourites;
    }

    public void setMyfavourites(Set<ServiceProviderV1> myfavourites) {
        this.myfavourites = myfavourites;
    }

    public Long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Long customerid) {
        this.customerid = customerid;
    }

    public Set<AppointmentV1> getMyappointments() {
        return myappointments;
    }

    public void setMyappointments(Set<AppointmentV1> myappointments) {
        this.myappointments = myappointments;
    }

    @Override
    public String toString() {
        return "CustomerV1{" +
                "customerid=" + customerid +
                ", customerphonenumber='" + customerphonenumber + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", dob=" + dob +
                ", address='" + address + '\'' +
                ", myfavourites=" + myfavourites +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerV1)) return false;

        CustomerV1 that = (CustomerV1) o;

        if (customerid != null ? !customerid.equals(that.customerid) : that.customerid != null) return false;
        if (customerphonenumber != null ? !customerphonenumber.equals(that.customerphonenumber) : that.customerphonenumber != null)
            return false;
        return email != null ? email.equals(that.email) : that.email == null;

    }

    @Override
    public int hashCode() {
        int result = customerid != null ? customerid.hashCode() : 0;
        result = 31 * result + (customerphonenumber != null ? customerphonenumber.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(CustomerV1 o) {
        if (this.customerid > o.customerid)
            return 1;
        else if(this.customerid < o.customerid)
            return  -1;
        else
            return 0;
    }
}
