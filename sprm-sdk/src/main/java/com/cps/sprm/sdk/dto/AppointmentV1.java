package com.cps.sprm.sdk.dto;

import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;

/**
 * Created by sumeshs on 12/28/2016.
 */
public class AppointmentV1 implements Serializable {

    private Long appointmentid;
    private Integer tokenNumber;
    private Long timeSlot;
    private Boolean isFirstTime;
    private String patientIdInHospital;
    private String notes;
    private AppointmentStatus appointmentStatus;

    @JsonBackReference(value = "appointmentToCustomer")
    private CustomerV1 myCustomer;

    @JsonBackReference(value = "appointmentToLocation")
    private LocationV1 myLocation;

    @JsonBackReference(value = "appointmentToSP")
    private ServiceProviderV1 myServiceProvider;

    @JsonBackReference(value = "appointmentToAvailability")
    private AvailabilityV1 fromAvailability;

    public Long getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(Long appointmentid) {
        this.appointmentid = appointmentid;
    }

    public Integer getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(Integer tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public Long getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Long timeSlot) {
        this.timeSlot = timeSlot;
    }

    public Boolean getFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(Boolean firstTime) {
        isFirstTime = firstTime;
    }

    public String getPatientIdInHospital() {
        return patientIdInHospital;
    }

    public void setPatientIdInHospital(String patientIdInHospital) {
        this.patientIdInHospital = patientIdInHospital;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public CustomerV1 getMyCustomer() {
        return myCustomer;
    }

    public void setMyCustomer(CustomerV1 myCustomer) {
        this.myCustomer = myCustomer;
    }

    public LocationV1 getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(LocationV1 myLocation) {
        this.myLocation = myLocation;
    }

    public ServiceProviderV1 getMyServiceProvider() {
        return myServiceProvider;
    }

    public void setMyServiceProvider(ServiceProviderV1 myServiceProvider) {
        this.myServiceProvider = myServiceProvider;
    }

    public AvailabilityV1 getFromAvailability() {
        return fromAvailability;
    }

    public void setFromAvailability(AvailabilityV1 fromAvailability) {
        this.fromAvailability = fromAvailability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AppointmentV1)) return false;

        AppointmentV1 that = (AppointmentV1) o;

        if (timeSlot != null ? !timeSlot.equals(that.timeSlot) : that.timeSlot != null) return false;
        if (myLocation != null ? !myLocation.equals(that.myLocation) : that.myLocation != null) return false;
        if (myServiceProvider != null ? !myServiceProvider.equals(that.myServiceProvider) : that.myServiceProvider != null)
            return false;
        return fromAvailability != null ? fromAvailability.equals(that.fromAvailability) : that.fromAvailability == null;

    }

    @Override
    public int hashCode() {
        int result = timeSlot != null ? timeSlot.hashCode() : 0;
        result = 31 * result + (myLocation != null ? myLocation.hashCode() : 0);
        result = 31 * result + (myServiceProvider != null ? myServiceProvider.hashCode() : 0);
        result = 31 * result + (fromAvailability != null ? fromAvailability.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AppointmentV1{" +
                "appointmentid=" + appointmentid +
                ", tokenNumber=" + tokenNumber +
                ", timeSlot=" + timeSlot +
                ", isFirstTime=" + isFirstTime +
                ", patientIdInHospital='" + patientIdInHospital + '\'' +
                ", notes='" + notes + '\'' +
                ", appointmentStatus=" + appointmentStatus +
                ", myCustomer=" + myCustomer +
                ", myLocation=" + myLocation +
                ", myServiceProvider=" + myServiceProvider +
                ", fromAvailability=" + fromAvailability +
                '}';
    }
}
