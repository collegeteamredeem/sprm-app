package com.cps.sprm.sdk.dto;

import java.io.Serializable;

/**
 * Created by sumeshs on 1/23/2017.
 */
public class LiveUpdateV2 implements Serializable {

    private Long liveupdateid;
    private Long myServiceProviderId;
    private Long myLocationId;
    private Float avgConsultingTime;
    private Integer totalTokens;
    private Integer currentToken;
    private Long consultingStartTime;
    private Long consultingEndTime;
    private Long todayDate;

    public Long getLiveupdateid() {
        return liveupdateid;
    }

    public void setLiveupdateid(Long liveupdateid) {
        this.liveupdateid = liveupdateid;
    }

    public Long getMyServiceProviderId() {
        return myServiceProviderId;
    }

    public void setMyServiceProviderId(Long myServiceProviderId) {
        this.myServiceProviderId = myServiceProviderId;
    }

    public Long getMyLocationId() {
        return myLocationId;
    }

    public void setMyLocationId(Long myLocationId) {
        this.myLocationId = myLocationId;
    }

    public Float getAvgConsultingTime() {
        return avgConsultingTime;
    }

    public void setAvgConsultingTime(Float avgConsultingTime) {
        this.avgConsultingTime = avgConsultingTime;
    }

    public Integer getTotalTokens() {
        return totalTokens;
    }

    public void setTotalTokens(Integer totalTokens) {
        this.totalTokens = totalTokens;
    }

    public Integer getCurrentToken() {
        return currentToken;
    }

    public void setCurrentToken(Integer currentToken) {
        this.currentToken = currentToken;
    }

    public Long getConsultingStartTime() {
        return consultingStartTime;
    }

    public void setConsultingStartTime(Long consultingStartTime) {
        this.consultingStartTime = consultingStartTime;
    }

    public Long getConsultingEndTime() {
        return consultingEndTime;
    }

    public void setConsultingEndTime(Long consultingEndTime) {
        this.consultingEndTime = consultingEndTime;
    }

    public Long getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(Long todayDate) {
        this.todayDate = todayDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LiveUpdateV2)) return false;

        LiveUpdateV2 that = (LiveUpdateV2) o;

        if (liveupdateid != null ? !liveupdateid.equals(that.liveupdateid) : that.liveupdateid != null) return false;
        if (myServiceProviderId != null ? !myServiceProviderId.equals(that.myServiceProviderId) : that.myServiceProviderId != null)
            return false;
        if (myLocationId != null ? !myLocationId.equals(that.myLocationId) : that.myLocationId != null) return false;
        if (avgConsultingTime != null ? !avgConsultingTime.equals(that.avgConsultingTime) : that.avgConsultingTime != null)
            return false;
        if (totalTokens != null ? !totalTokens.equals(that.totalTokens) : that.totalTokens != null) return false;
        return currentToken != null ? currentToken.equals(that.currentToken) : that.currentToken == null;

    }

    @Override
    public int hashCode() {
        int result = liveupdateid != null ? liveupdateid.hashCode() : 0;
        result = 31 * result + (myServiceProviderId != null ? myServiceProviderId.hashCode() : 0);
        result = 31 * result + (myLocationId != null ? myLocationId.hashCode() : 0);
        result = 31 * result + (avgConsultingTime != null ? avgConsultingTime.hashCode() : 0);
        result = 31 * result + (totalTokens != null ? totalTokens.hashCode() : 0);
        result = 31 * result + (currentToken != null ? currentToken.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LiveUpdateV2{" +
                "liveupdateid=" + liveupdateid +
                ", myServiceProviderId=" + myServiceProviderId +
                ", myLocationId=" + myLocationId +
                ", avgConsultingTime=" + avgConsultingTime +
                ", totalTokens=" + totalTokens +
                ", currentToken=" + currentToken +
                ", consultingEndTime=" + consultingEndTime +
                ", consultingStartTime=" + consultingStartTime +
                ", todayDate=" + todayDate +
                '}';
    }
}
