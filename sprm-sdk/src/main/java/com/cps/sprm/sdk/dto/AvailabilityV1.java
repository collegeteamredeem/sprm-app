package com.cps.sprm.sdk.dto;


import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by sumeshs on 11/21/2016.
 */
@SuppressWarnings("Since15")
public class AvailabilityV1 implements Serializable {

    private Long availabilityid;

    private Long dateTimeFrom;

    private Long dateTimeTo;

    private Set<Integer> weeklyoff = new HashSet<Integer>();

    private Set<Long> holidays = new HashSet<Long>();

    private Integer meetingTimeInMins;

    private Integer scheduleForNumDays;

    private Set<AppointmentV1> myappointments = new HashSet<AppointmentV1>();

    @JsonBackReference(value = "availabilityToSP")
    private ServiceProviderV1 myserviceprovider;

    @JsonBackReference(value = "availabilityToLocation")
    private LocationV1 mylocation;

    public Long getAvailabilityid() {
        return availabilityid;
    }

    public void setAvailabilityid(Long availabilityid) {
        this.availabilityid = availabilityid;
    }


    public Set<Integer> getWeeklyoff() {
        return weeklyoff;
    }

    public void setWeeklyoff(Set<Integer> weeklyoff) {
        this.weeklyoff = weeklyoff;
    }

    public Set<Long> getHolidays() {
        return holidays;
    }

    public void setHolidays(Set<Long> holidays) {
        this.holidays = holidays;
    }

    public Long getDateTimeFrom() {
        return dateTimeFrom;
    }

    public void setDateTimeFrom(Long dateTimeFrom) {
        this.dateTimeFrom = dateTimeFrom;
    }

    public Long getDateTimeTo() {
        return dateTimeTo;
    }

    public void setDateTimeTo(Long dateTimeTo) {
        this.dateTimeTo = dateTimeTo;
    }

    public ServiceProviderV1 getMyserviceprovider() {
        return myserviceprovider;
    }

    public void setMyserviceprovider(ServiceProviderV1 myserviceprovider) {
        this.myserviceprovider = myserviceprovider;
    }

    public LocationV1 getMylocation() {
        return mylocation;
    }

    public void setMylocation(LocationV1 mylocation) {
        this.mylocation = mylocation;
    }

    public Integer getMeetingTimeInMins() {
        return meetingTimeInMins;
    }

    public void setMeetingTimeInMins(Integer meetingTimeInMins) {
        this.meetingTimeInMins = meetingTimeInMins;
    }

    public Integer getScheduleForNumDays() {
        return scheduleForNumDays;
    }

    public void setScheduleForNumDays(Integer scheduleForNumDays) {
        this.scheduleForNumDays = scheduleForNumDays;
    }

    public Set<AppointmentV1> getMyappointments() {
        return myappointments;
    }

    public void setMyappointments(Set<AppointmentV1> myappointments) {
        this.myappointments = myappointments;
    }

    @Override
    public String toString() {
        return "AvailabilityV1{" +
                "availabilityid=" + availabilityid +
                ", dateTimeFrom=" + dateTimeFrom +
                ", dateTimeTo=" + dateTimeTo +
                ", weeklyoff=" + weeklyoff +
                ", holidays=" + holidays +
                ", meetingTimeInMins=" + meetingTimeInMins +
                ", scheduleForNumDays=" + scheduleForNumDays +
                '}';
    }
}
