package com.cps.sprm.sdk.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


public class ServiceProviderV1  implements Serializable
{

	private Long serviceproviderid;

	private String providersmsid;
	private String phonenumber;
	private String name;
	private String email;
	private String assistantonephoneno;
	private String assistanttwophoneno;
	private String title;
	private String qualification;
	private int experience;
	private Long validity = System.currentTimeMillis() + 2592000000L;

	//@JsonManagedReference(value = "locations")
    private Set<LocationV1> locations = new HashSet<LocationV1>();

    private Set<AvailabilityV1> myavailability = new HashSet<AvailabilityV1>();

    private Set<AppointmentV1> myappointments = new HashSet<AppointmentV1>();

    private Set<com.cps.sprm.sdk.enums.SPECIALIZATION> specializations = new HashSet<com.cps.sprm.sdk.enums.SPECIALIZATION>();

	public Set<com.cps.sprm.sdk.enums.SPECIALIZATION> getSpecializations() {
		return specializations;
	}

	public void setSpecializations(Set<com.cps.sprm.sdk.enums.SPECIALIZATION> specilizations) {
		this.specializations = specilizations;
	}
	public void addSpecializations(com.cps.sprm.sdk.enums.SPECIALIZATION specilization) {
		this.specializations.add(specilization);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getServiceproviderid() {
		return serviceproviderid;
	}

	public void setServiceproviderid(Long serviceproviderid) {
		this.serviceproviderid = serviceproviderid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public Set<LocationV1> getLocations() {
		return locations;
	}

	public void setLocations(Set<LocationV1> locations) {
		this.locations = locations;
	}

	public String getProvidersmsid() {
		return providersmsid;
	}

	public void addLocation(LocationV1 locationV1){
		this.locations.add(locationV1);
	}

	public void setProvidersmsid(String providersmsid) {
		this.providersmsid = providersmsid;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getAssistantonephoneno() {
		return assistantonephoneno;
	}

	public void setAssistantonephoneno(String assistantonephoneno) {
		this.assistantonephoneno = assistantonephoneno;
	}

	public String getAssistanttwophoneno() {
		return assistanttwophoneno;
	}

	public void setAssistanttwophoneno(String assistanttwophoneno) {
		this.assistanttwophoneno = assistanttwophoneno;
	}

	public Set<AvailabilityV1> getMyavailability() {
		return myavailability;
	}

	public void setMyavailability(Set<AvailabilityV1> myavailability) {
		this.myavailability = myavailability;
	}

	public Set<AppointmentV1> getMyappointments() {
		return myappointments;
	}

	public void setMyappointments(Set<AppointmentV1> myappointments) {
		this.myappointments = myappointments;
	}

	public Long getValidity() {return validity;}

	public void setValidity(Long validity) {this.validity = validity;}

	@Override
	public String toString() {
		return "ServiceProviderV1{" +
				"serviceproviderid=" + serviceproviderid +
				", providersmsid='" + providersmsid + '\'' +
				", phonenumber='" + phonenumber + '\'' +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", assistantonephoneno='" + assistantonephoneno + '\'' +
				", assistanttwophoneno='" + assistanttwophoneno + '\'' +
				", title='" + title + '\'' +
				", qualification='" + qualification + '\'' +
				", experience=" + experience +
				", validity=" + validity +
				", locations=" + locations +
				", myavailability=" + myavailability +
				", specializations=" + specializations +
				'}';
	}
}
