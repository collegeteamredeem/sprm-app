package com.cps.sprm.sdk.enums;

/**
 * Created by sumeshs on 12/28/2016.
 */
public enum AppointmentStatus {

    BOOKED(0, "booked"),
    CANCELLEDBYSP(1, "canceledByServiceProvider"),
    FREE(2, "free"),
    TIMEDOUT(3, "timedout"),
    PROCESSED(4, "processed");
    //CANCELLEDBYCUSTOMER(5, "canceledByCustomer"), // revert to free if cancelled By Customer

    private int id;
    private String name;

    AppointmentStatus(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
