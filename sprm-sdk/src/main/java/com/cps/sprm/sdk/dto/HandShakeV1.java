package com.cps.sprm.sdk.dto;

import java.io.Serializable;

/**
 * Created by sumeshs on 2/19/2017.
 */
public class HandShakeV1 implements Serializable {

    private String phoneNumber;
    private String otp;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
