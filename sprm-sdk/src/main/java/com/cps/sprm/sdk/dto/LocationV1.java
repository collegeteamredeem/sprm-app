package com.cps.sprm.sdk.dto;


import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class LocationV1 implements Serializable
{
	private Long locationid;

	private String fulladdress;
	private String name;
	private String pincode;
	private String city;
	private double lat;
	private double lon;

	private Set<AvailabilityV1> myavailability = new HashSet<AvailabilityV1>(0);

	private Set<AppointmentV1> myappointments = new HashSet<AppointmentV1>();

	@JsonBackReference(value = "locationServiceProviders")
	private Set<ServiceProviderV1> serviceProviders =	new HashSet<ServiceProviderV1>(0);

	public String getFulladdress() {
		return fulladdress;
	}

	public Long getLocationid() {
		return locationid;
	}

	public void setLocationid(Long locationid) {
		this.locationid = locationid;
	}

	public void setFulladdress(String fulladdress) {
		this.fulladdress = fulladdress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Set<ServiceProviderV1> getServiceProviders() {
		return serviceProviders;
	}

	public void setServiceProviders(Set<ServiceProviderV1> serviceProviders) {
		this.serviceProviders = serviceProviders;
	}

	public Set<AvailabilityV1> getMyavailability() {
		return myavailability;
	}

	public void setMyavailability(Set<AvailabilityV1> myavailability) {
		this.myavailability = myavailability;
	}

	public void addToMyavailability(AvailabilityV1 myavailability) {
		this.myavailability.add(myavailability);
	}

	public void deleteFromMyavailability(AvailabilityV1 myavailability) {
		this.myavailability.remove(myavailability);
	}

	public Set<AppointmentV1> getMyappointments() {
		return myappointments;
	}

	public void setMyappointments(Set<AppointmentV1> myappointments) {
		this.myappointments = myappointments;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof LocationV1)) return false;

		LocationV1 that = (LocationV1) o;

		if (name != null ? !name.equals(that.name) : that.name != null) return false;
		return pincode != null ? pincode.equals(that.pincode) : that.pincode == null;

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (pincode != null ? pincode.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "LocationV1{" +
				"locationid=" + locationid +
				", fulladdress='" + fulladdress + '\'' +
				", name='" + name + '\'' +
				", pincode='" + pincode + '\'' +
				", lat=" + lat +
				", lon=" + lon +
				", city='" + city + '\'' +
				'}';
	}
}

