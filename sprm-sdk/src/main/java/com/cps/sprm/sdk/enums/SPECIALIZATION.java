package com.cps.sprm.sdk.enums;

public enum SPECIALIZATION 
{

	ORTHO(1, "ORTHO"),
	DENTAL(2, "DENTAL");
	
	private int id;
	private String name;
	
	private SPECIALIZATION(int id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
