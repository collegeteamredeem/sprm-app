package com.cps.sprm.sdk.dto;

import com.cps.sprm.sdk.enums.AppointmentStatus;

import java.io.Serializable;

/**
 * Created by sumeshs on 1/13/2017.
 */
public class AppointmentV2 implements Serializable {

    private Long appointmentid;
    private Integer tokenNumber;
    private Long timeSlot;
    private Boolean isFirstTime;
    private String patientIdInHospital;
    private String notes;
    private AppointmentStatus appointmentStatus;

    private Long myCustomerId;

    private Long myLocationId;

    private Long myServiceProviderId;

    private Long fromAvailabilityId;

    public Long getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(Long appointmentid) {
        this.appointmentid = appointmentid;
    }

    public Integer getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(Integer tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public Long getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Long timeSlot) {
        this.timeSlot = timeSlot;
    }

    public Boolean getFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(Boolean firstTime) {
        isFirstTime = firstTime;
    }

    public String getPatientIdInHospital() {
        return patientIdInHospital;
    }

    public void setPatientIdInHospital(String patientIdInHospital) {
        this.patientIdInHospital = patientIdInHospital;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public Long getMyCustomerId() {
        return myCustomerId;
    }

    public void setMyCustomerId(Long myCustomerId) {
        this.myCustomerId = myCustomerId;
    }

    public Long getMyLocationId() {
        return myLocationId;
    }

    public void setMyLocationId(Long myLocationId) {
        this.myLocationId = myLocationId;
    }

    public Long getMyServiceProviderId() {
        return myServiceProviderId;
    }

    public void setMyServiceProviderId(Long myServiceProviderId) {
        this.myServiceProviderId = myServiceProviderId;
    }

    public Long getFromAvailabilityId() {
        return fromAvailabilityId;
    }

    public void setFromAvailabilityId(Long fromAvailabilityId) {
        this.fromAvailabilityId = fromAvailabilityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AppointmentV2)) return false;

        AppointmentV2 that = (AppointmentV2) o;

        if (appointmentid != null ? !appointmentid.equals(that.appointmentid) : that.appointmentid != null)
            return false;
        if (tokenNumber != null ? !tokenNumber.equals(that.tokenNumber) : that.tokenNumber != null) return false;
        if (timeSlot != null ? !timeSlot.equals(that.timeSlot) : that.timeSlot != null) return false;
        if (appointmentStatus != that.appointmentStatus) return false;
        if (myCustomerId != null ? !myCustomerId.equals(that.myCustomerId) : that.myCustomerId != null) return false;
        if (myLocationId != null ? !myLocationId.equals(that.myLocationId) : that.myLocationId != null) return false;
        if (myServiceProviderId != null ? !myServiceProviderId.equals(that.myServiceProviderId) : that.myServiceProviderId != null)
            return false;
        return fromAvailabilityId != null ? fromAvailabilityId.equals(that.fromAvailabilityId) : that.fromAvailabilityId == null;

    }

    @Override
    public int hashCode() {
        int result = appointmentid != null ? appointmentid.hashCode() : 0;
        result = 31 * result + (tokenNumber != null ? tokenNumber.hashCode() : 0);
        result = 31 * result + (timeSlot != null ? timeSlot.hashCode() : 0);
        result = 31 * result + (appointmentStatus != null ? appointmentStatus.hashCode() : 0);
        result = 31 * result + (myCustomerId != null ? myCustomerId.hashCode() : 0);
        result = 31 * result + (myLocationId != null ? myLocationId.hashCode() : 0);
        result = 31 * result + (myServiceProviderId != null ? myServiceProviderId.hashCode() : 0);
        result = 31 * result + (fromAvailabilityId != null ? fromAvailabilityId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AppointmentV1{" +
                "appointmentid=" + appointmentid +
                ", tokenNumber=" + tokenNumber +
                ", timeSlot=" + timeSlot +
                ", isFirstTime=" + isFirstTime +
                ", patientIdInHospital='" + patientIdInHospital + '\'' +
                ", notes='" + notes + '\'' +
                ", appointmentStatus=" + appointmentStatus +
                ", myCustomer=" + myCustomerId +
                ", myLocation=" + myLocationId +
                ", myServiceProvider=" + myServiceProviderId +
                ", fromAvailability=" + fromAvailabilityId +
                '}';
    }
}
