package sirius_main.sprm;

import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.joda.time.*;

import java.io.*;
import java.nio.charset.Charset;
import java.time.*;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.zone.ZoneRulesProvider;
import java.util.Arrays;
import java.util.Date;

/**
 * Unit test for simple App.
 */
@SuppressWarnings("Since15")
public class AppTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testJson(){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        Event event1 = new Event();
        event1.setId(1);
        Event event2 = new Event();
        event2.setId(2);

        User user = new User();
        user.setId(10);

        event1.setUsers(Arrays.asList(user));
        event2.setUsers(Arrays.asList(user));
        user.setEvents(Arrays.asList(event1, event2));

        String json = null;
        try {
            json = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(json);

        User deserializedUser = null;
        try {
            deserializedUser = objectMapper.readValue(json, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(deserializedUser.toString());
    }

    public void testDate(){
        Date date = new Date();
        System.out.println(date.toString());
        System.out.println(date.getTime());

        Date utc = new Date();

        System.out.println("______________");
        DateTime dateTime = new DateTime(date);
        System.out.println(dateTime.toLocalDate());
        System.out.println(dateTime.toLocalDateTime());
        System.out.println(dateTime.toLocalTime());

        System.out.println("______________");
        DateTime dateTime1 = dateTime.toDateTime(DateTimeZone.UTC);
        System.out.println(dateTime1.toLocalDate());
        System.out.println(dateTime1.toLocalDateTime());
        System.out.println(dateTime1.toLocalTime());
        System.out.println(dateTime1.toString());
        System.out.println("______________");

        Date dateUtc = dateTime1.toDate();
        System.out.println(dateUtc.toString());
        System.out.println(dateUtc.getTime());
    }

    public void testJavaDate(){
        Date date = new Date();
        System.out.println(date.getTime());
        Instant fromDate = Instant.ofEpochMilli(date.getTime());
        System.out.println(fromDate.toString());
        System.out.println("______________");
        Instant instant = Instant.now();
        System.out.println(instant.toString());
        System.out.println(instant.getEpochSecond());

        System.out.println("______________");
        System.out.println(ZoneRulesProvider.getAvailableZoneIds());
        System.out.println("______________");

        System.out.println("At Calcutta");
        LocalDateTime time = LocalDateTime.now();
        System.out.println(time);
        System.out.println(time.toString());
        System.out.println(time.toLocalDate());
        System.out.println(time.toLocalTime());

        System.out.println("______________");
        System.out.println("At UTC");
        ZonedDateTime zdt = time.atZone(ZoneId.of("Asia/Calcutta"));
        ZonedDateTime utc = zdt.withZoneSameInstant(ZoneId.of("UTC"));
    //    LocalDateTime utc = zdt.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
        System.out.println(utc);

        System.out.println("______________");
        System.out.println("At Calcutta");
        ZonedDateTime back = utc.withZoneSameInstant(ZoneId.of("Asia/Calcutta"));
        System.out.println(back);

    }

    public void testJava8Date(){
        Date date = new Date();
        System.out.println(date.getTime());
        Instant fromDate = Instant.ofEpochMilli(date.getTime());
        System.out.println("Instant:" +fromDate.toString());

        LocalDateTime localDateTime = LocalDateTime.ofInstant(fromDate, ZoneId.of("UTC"));
        System.out.println("UTC: " +localDateTime.toString());
        Instant again = localDateTime.toInstant(ZoneOffset.UTC);
        System.out.println("AGain:" + again.toString());
        System.out.println("Epock Sec:" + again.getEpochSecond());
        long milli = again.getEpochSecond() * 1000;
        System.out.println("Epock Milli Sec:" + milli);
        Instant dateConstrct = Instant.ofEpochMilli(milli);
        System.out.println("Instant Constrxy:" +dateConstrct.toString());

        LocalDateTime mineDateTime = LocalDateTime.ofInstant(fromDate, ZoneId.of("Asia/Calcutta"));
        System.out.println("Mine: " +mineDateTime.toString());
    }

    public void testTest(){

        LocalDate date = LocalDate.now();
        System.out.println("This is " + date.toString());
        System.out.println("This is --" + date.atStartOfDay());
        System.out.println("This is -----" + date.atStartOfDay().toEpochSecond(ZoneOffset.UTC));
        LocalTime midnight = LocalTime.MIDNIGHT;
        System.out.println("MIDNIGHT:" +midnight.toString());
        LocalDate today = LocalDate.now(ZoneId.of("Asia/Calcutta"));
        System.out.println("Today:" +today.toString());
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        System.out.println("todayMidnight:" +todayMidnight.toString());
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);
        System.out.println("tomorrowMidnight:" +tomorrowMidnight.toString());
    }
    public void testJava8DatePlusMonths(){
        Date date = new Date();
        System.out.println(date.getTime());
        Instant fromDate = Instant.ofEpochMilli(date.getTime());
        System.out.println("Instant:" +fromDate.toString());

        LocalDateTime localDateTime = LocalDateTime.ofInstant(fromDate, ZoneId.of("UTC"));
        System.out.println("UTC: " +localDateTime.toString());

        LocalDateTime futureDate = localDateTime.plusMonths(12);
        System.out.println("UTC After Update : " +futureDate.toString());

        Long newMilli = futureDate.toEpochSecond(ZoneOffset.UTC) * 1000;
        System.out.println("New Milli " + newMilli);
    }

    public void testJava8DatePlusMillis(){
        Date date = new Date();
        System.out.println(date.getTime());
        Instant fromDate = Instant.ofEpochMilli(date.getTime());
        System.out.println("Instant:" +fromDate.toString());

        DateTimeUtils.setCurrentMillisOffset(2629746000L);

        Instant newDate = Instant.ofEpochMilli(System.currentTimeMillis());
        System.out.println("Instant New:" +newDate.toString());

        DateTimeUtils.setCurrentMillisSystem();
    }

    public void testJava8Clock(){
        Date date = new Date();
        System.out.println(date.getTime());
        System.out.println(date.toString());

        Clock clock = Clock.systemUTC();
        Instant now = Instant.now(clock);
        System.out.println("Instant with clock:" +now.toString());

        System.out.println("Millis " + clock.millis());
        Instant fromDate = Instant.ofEpochMilli(clock.millis());
        System.out.println("Instant with millis:" +fromDate.toString());
    }

    public void testJava8ClockOffset(){

        Clock clock = Clock.systemUTC();
        Instant now = Instant.now(clock);
        System.out.println("Instant with clock:" +now.toString());

        Duration durationOffset = Duration.ofDays(31);
       Clock offsetClock = Clock.offset(clock, durationOffset);
        Instant nowOffset = Instant.now(offsetClock);
        System.out.println("Instant with clock:" +nowOffset.toString());

    }

    public void testepochtoDate() throws Exception {

        String line;
        try {
            InputStream fis = new FileInputStream("C:\\date.txt");
            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(isr);

            while ((line = br.readLine()) != null) {
               //System.out.println(line);
                Long date1 = Long.parseLong(line);
                String date = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date (date1));
                System.out.println(date);
            }
          }catch (Exception e){
              e.printStackTrace();
        }


        long epoch = 1478174700000L;
        String date = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date (epoch));
        System.out.print(date);
    }

    public void testEnum() throws Exception{
        String str = "CANCELLEDBYSP";
        AppointmentStatus status = AppointmentStatus.valueOf(str);
        System.out.println(status);
        System.out.println(status.getId());
        System.out.println(status.getName());
    }
}
