package com.cps.sprm.util;

import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class IntegrationTestUtil {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static String SERVICE_PROVIDER_URL_ENTITY_NAME = "serviceprovider";
    public static String CUSTOMER_URL_ENTITY_NAME = "customer";
    public static String LOCATION_URL_ENTITY_NAME = "location";
    public static String APPOINTMENT_URL_ENTITY_NAME = "apt";

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    public static void setTimeout(RestTemplate restTemplate, int timeout) {
        //Explicitly setting ClientHttpRequestFactory instance to
        //SimpleClientHttpRequestFactory instance to leverage
        //set*Timeout methods
        restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory());
        SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate
                .getRequestFactory();
        rf.setReadTimeout(timeout);
        rf.setConnectTimeout(timeout);
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void deleteObject(Long id, MockMvc mockMvc, String entity) throws Exception {
        mockMvc.perform(delete("/" + entity + "/purgeit/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    public static void deleteObjectIgnoreStatus(Long id, MockMvc mockMvc, String entity) throws Exception {
        mockMvc.perform(delete("/" + entity + "/purgeit/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    public static void deleteSPAndItsLocations(MockMvc mockMvc, ServiceProviderV1 serviceProviderV1) throws Exception{
        IntegrationTestUtil.deleteObjectIgnoreStatus(serviceProviderV1.getServiceproviderid(), mockMvc, IntegrationTestUtil.SERVICE_PROVIDER_URL_ENTITY_NAME);
        Set<LocationV1> locations = serviceProviderV1.getLocations();
        for(LocationV1 locationV1 : locations){
            IntegrationTestUtil.deleteObjectIgnoreStatus(locationV1.getLocationid(), mockMvc, IntegrationTestUtil.LOCATION_URL_ENTITY_NAME);
        }
    }
}