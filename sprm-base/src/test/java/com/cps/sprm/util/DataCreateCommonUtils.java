package com.cps.sprm.util;

import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.ZoneId;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.Locale;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by sumeshs on 1/27/2017.
 */
public class DataCreateCommonUtils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static AvailabilityV1 generateAvailabilityData(String startTime, String endTime, List<String> holidayList, int meetingTime) {
        // Add Availability
        AvailabilityV1 availabilityV1 = new AvailabilityV1();
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);

        try {
            availabilityV1.setDateTimeFrom(format.parse(startTime).getTime());
            availabilityV1.setDateTimeTo(format.parse(endTime).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Set<Integer> weeklyOffs = new HashSet<Integer>();
        weeklyOffs.add(DayOfWeek.SATURDAY.getValue());
        weeklyOffs.add(DayOfWeek.SUNDAY.getValue());
        availabilityV1.setWeeklyoff(weeklyOffs);

        availabilityV1.setScheduleForNumDays(30);

        // Dealing with pure dates. treat them as UTC
        DateFormat formatholiday = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        formatholiday.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));

        Set<Long> holidays = new HashSet<Long>();
        try {
            for(String  holidayStr : holidayList){
                holidays.add(formatholiday.parse(holidayStr).getTime());
            }
            availabilityV1.setHolidays(holidays);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        availabilityV1.setMeetingTimeInMins(meetingTime);
        return availabilityV1;
    }

    public static ServiceProviderV1 generateAppointmentData(MockMvc mockMvc) throws Exception {

        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675134", "Doc7", "doc7@gmail.com", "560041", "560042");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        List<String> holidays = new ArrayList<String>();
        holidays.add("09/11/2016");
        holidays.add("10/11/2016");

        AvailabilityV1 availability1 = generateAvailabilityData("03/11/2016 09:00", "04/11/2016 10:00", holidays, 5);
        AvailabilityV1 availability2 = generateAvailabilityData("03/11/2016 17:00", "04/11/2016 18:00", holidays, 5);

        LocationV1[] locationV1s = spForAssert.getLocations().toArray(new LocationV1[0]);

        LocationV1 locationOne = locationV1s[0];
        locationOne.addToMyavailability(availability1);
        System.out.println("#Location one id " + locationOne.getLocationid());

        LocationV1 locationTwo = locationV1s[1];
        locationTwo.addToMyavailability(availability2);
        System.out.println("#Location tow id " + locationTwo.getLocationid());

        try {
            result = mockMvc.perform(put("/serviceprovider/availability/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationOne.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availability1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After PUT First availability to Location Result= " + result.getResponse().getContentAsString());

            ServiceProviderV1 spAfterFirstUpdate = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
            Assert.assertEquals(1, spAfterFirstUpdate.getMyavailability().size());

            result = mockMvc.perform(put("/serviceprovider/availability/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationTwo.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availability2))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After PUT Second availability to Location Result= " + result.getResponse().getContentAsString());
            spAfterFirstUpdate = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
            Assert.assertEquals(2, spAfterFirstUpdate.getMyavailability().size());


       /*     result = mockMvc.perform(get("/serviceprovider/{id}", spAfterFirstUpdate.getServiceproviderid()))
                    .andExpect(status().isOk())
                    .andReturn();
            System.out.println("After GET Again Result= " + result.getResponse().getContentAsString());*/



            result = mockMvc.perform(get("/serviceprovider/apt/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("Generate appt at Location one Again Result= " + result.getResponse().getContentAsString());

            result = mockMvc.perform(get("/serviceprovider/apt/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationTwo.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("Generate appt at Location two Again Result= " + result.getResponse().getContentAsString());

        }finally {

        }
        return spForAssert;
    }

    public static CustomerV1 createCustomerData(MockMvc mockMvc, CustomerV1 customer) throws Exception {

        MvcResult result = mockMvc.perform(post("/customer")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(customer))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        CustomerV1 persistedCustomerV1 = objectMapper.readValue(result.getResponse().getContentAsByteArray(), CustomerV1.class);
        return persistedCustomerV1;
    }
}
