package com.cps.sprm.util;

import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.cps.sprm.sdk.enums.SPECIALIZATION;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sumeshs on 11/12/2016.
 */
public class DTOUtil {
    
    public static ServiceProviderV1 createSPDTO(String phoneNumber, String name, String email, String assistantOnePhoneNo, String assistantTwoPhoneNo, String title, String qualification, int experience) {
        ServiceProviderV1 dto = new ServiceProviderV1();

        dto.setPhonenumber(phoneNumber);
        dto.setName(name);
        dto.setEmail(email);
        dto.setAssistantonephoneno(assistantOnePhoneNo);
        dto.setAssistanttwophoneno(assistantTwoPhoneNo);
        dto.setTitle(title);
        dto.setQualification(qualification);
        dto.setExperience(experience);
        return dto;
    }

    public static ServiceProviderV1 createSPDTO(String phoneNumber, String name, String email, String pinCode1, String pinCode2) {
        ServiceProviderV1 dto = new ServiceProviderV1();

        dto.setPhonenumber(phoneNumber);
        dto.setName(name);
        dto.setEmail(email);
        dto.setAssistantonephoneno("1111111111");
        dto.setAssistanttwophoneno("2222222222");
        dto.setTitle("Title");
        dto.setQualification("MBBS");
        dto.setExperience(10);
        dto.addSpecializations(SPECIALIZATION.DENTAL);
        dto.addSpecializations(SPECIALIZATION.ORTHO);

        LocationV1 location1 = new LocationV1();
        location1.setFulladdress("Full Address 1");
        location1.setName("Name 1");
        location1.setCity("City 1");
        location1.setLat(18.450);
        location1.setLon(-18.550);
        location1.setPincode(pinCode1);

        LocationV1 location2 = new LocationV1();
        location2.setFulladdress("Full Address 2");
        location2.setName("Name 2");
        location2.setCity("City 2");
        location2.setLat(18.451);
        location2.setLon(-18.551);
        location2.setPincode(pinCode2);

        dto.addLocation(location1);
        dto.addLocation(location2);
        return dto;
    }

    public static LocationV1 createLocationDTO(String fullAddress, String name, String city,
                                               double lat, double lon, String pinCode) {
        LocationV1 location1 = new LocationV1();
        location1.setFulladdress(fullAddress);
        location1.setName(name);
        location1.setCity(city);
        location1.setLat(lat);
        location1.setLon(lon);
        location1.setPincode(pinCode);
        return location1;
    }


    public static AppointmentV1 createAppointmentDTO(String timeSlot, String notes, String patientId,
                                                 int tokenNumber) {

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);

        AppointmentV1 appointmentV1 = new AppointmentV1();

        appointmentV1.setAppointmentStatus(AppointmentStatus.FREE);
        appointmentV1.setFirstTime(true);
        appointmentV1.setNotes(notes);
        appointmentV1.setPatientIdInHospital(patientId);
        appointmentV1.setTokenNumber(tokenNumber);
        try {
            appointmentV1.setTimeSlot(format.parse(timeSlot).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return appointmentV1;
    }

    public static CustomerV1 createCustomerDTO(String phoneNumber, String name, String email,
                                               Date dob, String address) {
        CustomerV1 dto = new CustomerV1();

        dto.setCustomerphonenumber(phoneNumber);
        dto.setName(name);
        dto.setEmail(email);
        dto.setDob(dob);
        dto.setAddress(address);
        return dto;
    }

    public static String createStringWithLength(int length) {
        StringBuilder builder = new StringBuilder();

        for (int index = 0; index < length; index++) {
            builder.append("a");
        }

        return builder.toString();
    }

}
