package com.cps.sprm.controllers;

import com.cps.sprm.configuration.ApplicationContextConfig;
import com.cps.sprm.dao.AppointmentDao;
import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.cps.sprm.util.DTOUtil;
import com.cps.sprm.util.IntegrationTestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by sumeshs on 12/29/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(classes = ApplicationContextConfig.class)
@WebAppConfiguration
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class ITAppointmentControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AppointmentDao appointmentDao;

    private MockMvc mockMvc;

    private RestTemplate restTemplate;

    private AppointmentV1 AppointmentV1ForPost = DTOUtil.createAppointmentDTO( "03/11/2016 09:00:05", "notes Custom", "HOSP1234" , 1);
    private AppointmentV1 AppointmentV12 = DTOUtil.createAppointmentDTO( "03/11/2016 09:00:10", "notes Custom", "HOSP1234" , 1);
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        restTemplate = new RestTemplate();
        IntegrationTestUtil.setTimeout(restTemplate, 5000);
    }

    @After
    public void teardown() throws SQLException {
        restTemplate = null;
        this.mockMvc = null;
    }

    @Test
    public void getAllAptUsingMockMVC() throws Exception{
        AppointmentV1 persistedApt1 = createData(AppointmentV1ForPost);
        AppointmentV1 persistedApt2 = createData(AppointmentV12);

        MvcResult result = mockMvc.perform(get("/apt"))
                .andExpect(status().isOk())
                .andReturn();
        try {
            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(2, myAppointments.size());
        }finally {
            IntegrationTestUtil.deleteObject(persistedApt1.getAppointmentid(), mockMvc, IntegrationTestUtil.APPOINTMENT_URL_ENTITY_NAME);
            IntegrationTestUtil.deleteObject(persistedApt2.getAppointmentid(), mockMvc, IntegrationTestUtil.APPOINTMENT_URL_ENTITY_NAME);
        }
    }

    @Test
    public void getAptByIdUsingMockMVC() throws Exception{
        AppointmentV1 persistedAptV1 = createData(AppointmentV1ForPost);
        MvcResult result = mockMvc.perform(get("/apt/{id}", persistedAptV1.getAppointmentid()))
                .andExpect(status().isOk())
                .andReturn();

        AppointmentV2 aptV2ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), AppointmentV2.class);
        try {
            assertV2AppointmentObjects(aptV2ForAssert, persistedAptV1);
        }finally {
            IntegrationTestUtil.deleteObject(aptV2ForAssert.getAppointmentid(), mockMvc, IntegrationTestUtil.APPOINTMENT_URL_ENTITY_NAME);
        }
    }

    @Test
    public void updateAppointmentUsingMockMVCObject() throws Exception{
        AppointmentV1 persistedAptV1 = createData(AppointmentV1ForPost);

        persistedAptV1.setAppointmentStatus(AppointmentStatus.CANCELLEDBYSP);

        MvcResult result = mockMvc.perform(put("/apt/update/{id}", persistedAptV1.getAppointmentid())
                .content(IntegrationTestUtil.convertObjectToJsonBytes(persistedAptV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        AppointmentV1 AppointmentV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), AppointmentV1.class);
        try {
            assertV1AppointmentObjects(AppointmentV1ForAssert, persistedAptV1);
        }finally {
            IntegrationTestUtil.deleteObject(AppointmentV1ForAssert.getAppointmentid(), mockMvc, IntegrationTestUtil.APPOINTMENT_URL_ENTITY_NAME);
        }

    }

    @Test
    public void removeAptUsingMockMVCObject() throws Exception{
        AppointmentV1 persistedAptV1 = createData(AppointmentV1ForPost);

        mockMvc.perform(delete("/apt/purgeit/{id}", persistedAptV1.getAppointmentid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get("/customer/{id}", persistedAptV1.getAppointmentid()))
                .andExpect(status().is4xxClientError())
                .andReturn();

    }

    @Test
    public void testExceptionWhenRemovingBookedApt() throws Exception{
        AppointmentV1 persistedAptV1 = createData(AppointmentV1ForPost);

        persistedAptV1.setAppointmentStatus(AppointmentStatus.BOOKED);

        MvcResult result = mockMvc.perform(put("/apt/update/{id}", persistedAptV1.getAppointmentid())
                .content(IntegrationTestUtil.convertObjectToJsonBytes(persistedAptV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        AppointmentV1 AppointmentV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), AppointmentV1.class);
        assertV1AppointmentObjects(AppointmentV1ForAssert, persistedAptV1);

        try {
            mockMvc.perform(delete("/apt/purgeit/{id}", persistedAptV1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

            persistedAptV1.setAppointmentStatus(AppointmentStatus.FREE);

            result = mockMvc.perform(put("/apt/update/{id}", persistedAptV1.getAppointmentid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(persistedAptV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            AppointmentV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), AppointmentV1.class);
            assertV1AppointmentObjects(AppointmentV1ForAssert, persistedAptV1);

        }finally {
            IntegrationTestUtil.deleteObject(AppointmentV1ForAssert.getAppointmentid(), mockMvc, IntegrationTestUtil.APPOINTMENT_URL_ENTITY_NAME);
        }


    }

    private AppointmentV1 createData(AppointmentV1 AppointmentV1) throws Exception {

        MvcResult result = mockMvc.perform(post("/apt")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(AppointmentV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        AppointmentV1 persistedApt = objectMapper.readValue(result.getResponse().getContentAsByteArray(), AppointmentV1.class);
        return persistedApt;
    }


    private void assertV1AppointmentObjects(AppointmentV1 obj1, AppointmentV1 obj2){
        Assert.assertEquals(obj1.getAppointmentStatus(), obj2.getAppointmentStatus());
        Assert.assertEquals(obj1.getFirstTime(), obj2.getFirstTime());
        Assert.assertEquals(obj1.getNotes(), obj2.getNotes());
        Assert.assertEquals(obj1.getTimeSlot(), obj2.getTimeSlot());
        Assert.assertEquals(obj1.getTokenNumber(), obj2.getTokenNumber());

    }

    private void assertV2AppointmentObjects(AppointmentV2 obj1, AppointmentV1 obj2){
        Assert.assertEquals(obj1.getAppointmentStatus(), obj2.getAppointmentStatus());
        Assert.assertEquals(obj1.getFirstTime(), obj2.getFirstTime());
        Assert.assertEquals(obj1.getNotes(), obj2.getNotes());
        Assert.assertEquals(obj1.getTimeSlot(), obj2.getTimeSlot());
        Assert.assertEquals(obj1.getTokenNumber(), obj2.getTokenNumber());

    }
}
