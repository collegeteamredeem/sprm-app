package com.cps.sprm.controllers;

/**
 * Created by sumeshs on 11/14/2016.
 */

import com.cps.sprm.configuration.ApplicationContextConfig;
import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sdk.dto.HandShakeV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.util.DTOUtil;
import com.cps.sprm.util.IntegrationTestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(classes = ApplicationContextConfig.class)
@WebAppConfiguration
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class ITCustomerControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private RestTemplate restTemplate;

    private CustomerV1 customerV1ForPost = DTOUtil.createCustomerDTO( "919987675124", "Customer" , "customer1@gmail.com", new Date(), "address");
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        restTemplate = new RestTemplate();
        IntegrationTestUtil.setTimeout(restTemplate, 5000);
    }

    @After
    public void teardown() throws SQLException {
        restTemplate = null;
        this.mockMvc = null;
    }

    private CustomerV1 createData() throws Exception {

        MvcResult result = mockMvc.perform(post("/customer")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(customerV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        CustomerV1 persistedCustomerV1 = objectMapper.readValue(result.getResponse().getContentAsByteArray(), CustomerV1.class);
        return persistedCustomerV1;
    }

    private void assertV1Objects(CustomerV1 obj1, CustomerV1 obj2){
        Assert.assertEquals(obj1.getName(), obj2.getName());
        Assert.assertEquals(obj1.getCustomerphonenumber(), obj2.getCustomerphonenumber());
        Assert.assertEquals(obj1.getEmail(), obj2.getEmail());
        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd");
        Assert.assertEquals(sdf.format(obj1.getDob()), sdf.format(obj2.getDob()));
        Assert.assertEquals(obj1.getAddress(), obj2.getAddress());
    }


    @Test
    public void getByIdUsingMockMVC() throws Exception{
        CustomerV1 persistedCustomerV1 = createData();
        MvcResult result = mockMvc.perform(get("/customer/{id}", persistedCustomerV1.getCustomerid()))
                .andExpect(status().isOk())
                .andReturn();

        CustomerV1 customerV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), CustomerV1.class);
        try {
            assertV1Objects(customerV1ForAssert, persistedCustomerV1);
        }finally {
            IntegrationTestUtil.deleteObject(customerV1ForAssert.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
        }
    }

    @Test
    public void getByInvalidIdUsingMockMVC() throws Exception{
        mockMvc.perform(get("/customer/{id}", 1000L))
                .andExpect(status().is4xxClientError())
                .andReturn();

    }

    @Test
    public void postUsingMockMVC() throws Exception{
        MvcResult result = mockMvc.perform(post("/customer").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"name\": \"Customer 6\",\n" +
                        "\t\"customerphonenumber\": \"+91 9986033569\",\n" +
                        "\t\"email\": \"customer6@rediffmail.com\",\n" +
                        "\t\"dob\": \"1982-10-20\",\n" +
                        "\t\"address\" : \"No 60 Madras\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andReturn();

        CustomerV1 customerV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), CustomerV1.class);
        try{
            Assert.assertEquals("Customer 6", customerV1ForAssert.getName());
            Assert.assertEquals("+91 9986033569", customerV1ForAssert.getCustomerphonenumber());
            Assert.assertEquals("customer6@rediffmail.com", customerV1ForAssert.getEmail());
            java.text.SimpleDateFormat sdf =
                    new java.text.SimpleDateFormat("yyyy-MM-dd");
            Assert.assertEquals("1982-10-20", sdf.format(customerV1ForAssert.getDob()));
            Assert.assertEquals("No 60 Madras", customerV1ForAssert.getAddress());
        }finally {
            IntegrationTestUtil.deleteObject(customerV1ForAssert.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
        }
    }


    @Test
    public void postUsingMockMVCObject() throws Exception{
        CustomerV1 customerV1 = DTOUtil.createCustomerDTO( "919987675125", "Customer1" , "customer22@gmail.com", new Date(), "address 22");
        MvcResult result = mockMvc.perform(post("/customer")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(customerV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        CustomerV1 customerV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), CustomerV1.class);
        try {
            assertV1Objects(customerV1ForAssert, customerV1);
        }finally {
            IntegrationTestUtil.deleteObject(customerV1ForAssert.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
        }
    }

    @Test
    public void updateUsingMockMVCObject() throws Exception{
        CustomerV1 persistedCustomerV1 = createData();

        String newPhoneNumber = "915634218909";

        persistedCustomerV1.setCustomerphonenumber(newPhoneNumber);
        MvcResult result = mockMvc.perform(put("/customer/update/{id}", persistedCustomerV1.getCustomerid())
                .content(IntegrationTestUtil.convertObjectToJsonBytes(persistedCustomerV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        CustomerV1 customerV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), CustomerV1.class);
        try {
            assertV1Objects(customerV1ForAssert, persistedCustomerV1);
        }finally {
            IntegrationTestUtil.deleteObject(customerV1ForAssert.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
        }

    }

    @Test
    public void removeUsingMockMVCObject() throws Exception{
        CustomerV1 persistedCustomerV1 = createData();

        mockMvc.perform(delete("/customer/purgeit/{id}", persistedCustomerV1.getCustomerid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get("/customer/{id}", persistedCustomerV1.getCustomerid()))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void crudForCustomerFav() throws Exception{
        CustomerV1 customerV1 = DTOUtil.createCustomerDTO("919987675125", "Customer1", "customer22@gmail.com", new Date(), "address 22");
        MvcResult resultCust = mockMvc.perform(post("/customer")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(customerV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();


        ServiceProviderV1 serviceProviderV1ForPost1 = DTOUtil.createSPDTO("919987675123", "Doctor 1", "doc1@gmail.com", "560016", "560017");
        ServiceProviderV1 serviceProviderV1ForPost2 = DTOUtil.createSPDTO("919987675124", "Doctor 2", "doc2@gmail.com", "560001", "560002");

        MvcResult resultDoc1 = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult resultDoc2 = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost2))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();


        CustomerV1 customerV1Persisted = objectMapper.readValue(resultCust.getResponse().getContentAsByteArray(), CustomerV1.class);
        ServiceProviderV1 Doc1Persisted = objectMapper.readValue(resultDoc1.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        ServiceProviderV1 Doc2Persisted = objectMapper.readValue(resultDoc2.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        try {
            //Add 1st Fav
            mockMvc.perform(put("/customer/favourites/{custid}/sp/{spid}", customerV1Persisted.getCustomerid(), Doc1Persisted.getServiceproviderid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            MvcResult getCustomerresult = mockMvc.perform(get("/customer/{id}", customerV1Persisted.getCustomerid()))
                    .andExpect(status().isOk())
                    .andReturn();

            CustomerV1 customerV1ForAssert = objectMapper.readValue(getCustomerresult.getResponse().getContentAsByteArray(), CustomerV1.class);
            Assert.assertEquals(1, customerV1ForAssert.getMyfavourites().size());

            // Add 2nd Fav
            mockMvc.perform(put("/customer/favourites/{id}/sp/{spid}", customerV1Persisted.getCustomerid(), Doc2Persisted.getServiceproviderid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            getCustomerresult = mockMvc.perform(get("/customer/{id}", customerV1Persisted.getCustomerid()))
                    .andExpect(status().isOk())
                    .andReturn();

            customerV1ForAssert = objectMapper.readValue(getCustomerresult.getResponse().getContentAsByteArray(), CustomerV1.class);
            Assert.assertEquals(2, customerV1ForAssert.getMyfavourites().size());

            for(Iterator<ServiceProviderV1> iter = customerV1ForAssert.getMyfavourites().iterator(); iter.hasNext();){
                System.out.println("The Favourite Doctor ");
                System.out.println(iter.next().toString());
            }

            // Del a fav
            mockMvc.perform(delete("/customer/favourites/{id}/sp/{spid}", customerV1Persisted.getCustomerid(), Doc2Persisted.getServiceproviderid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(customerV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            getCustomerresult = mockMvc.perform(get("/customer/{id}", customerV1Persisted.getCustomerid()))
                    .andExpect(status().isOk())
                    .andReturn();

            customerV1ForAssert = objectMapper.readValue(getCustomerresult.getResponse().getContentAsByteArray(), CustomerV1.class);
            Assert.assertEquals(1, customerV1ForAssert.getMyfavourites().size());

            for(Iterator<ServiceProviderV1> iter = customerV1ForAssert.getMyfavourites().iterator(); iter.hasNext();){
                ServiceProviderV1 doc1 = iter.next();
                Assert.assertEquals("Doctor 1", doc1.getName()); // make sure Doc1 is present as favourite still
            }

            IntegrationTestUtil.deleteObject(customerV1Persisted.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);

            // make sure customer is deleted
            getCustomerresult = mockMvc.perform(get("/customer/{id}", customerV1Persisted.getCustomerid()))
                    .andExpect(status().is4xxClientError())
                    .andReturn();
            System.out.println("Customer is deleted " + getCustomerresult.getResponse().getContentAsString());

            // make sure doc is still present
            getCustomerresult = mockMvc.perform(get("/serviceprovider/{id}", Doc1Persisted.getServiceproviderid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("Doc1 is still there " + getCustomerresult.getResponse().getContentAsString());

            IntegrationTestUtil.deleteObject(Doc1Persisted.getServiceproviderid(), mockMvc, IntegrationTestUtil.SERVICE_PROVIDER_URL_ENTITY_NAME);

            // Make sure doc is deleted
            getCustomerresult = mockMvc.perform(get("/serviceprovider/{id}", Doc1Persisted.getServiceproviderid()))
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            System.out.println("Doctor1 is deleted " + getCustomerresult.getResponse().getContentAsString());


            getCustomerresult = mockMvc.perform(get("/serviceprovider/{id}", Doc2Persisted.getServiceproviderid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("Doc2 is still there " + getCustomerresult.getResponse().getContentAsString());

            IntegrationTestUtil.deleteObject(Doc2Persisted.getServiceproviderid(), mockMvc, IntegrationTestUtil.SERVICE_PROVIDER_URL_ENTITY_NAME);

            // Make sure doc is deleted
            getCustomerresult = mockMvc.perform(get("/serviceprovider/{id}", Doc2Persisted.getServiceproviderid()))
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            System.out.println("Doctor2 is deleted " + getCustomerresult.getResponse().getContentAsString());

        }finally {
           // delete anyway
            IntegrationTestUtil.deleteObjectIgnoreStatus(customerV1Persisted.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, Doc1Persisted);
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, Doc2Persisted);
        }

    }


    @Test
    @Ignore  // To run this test, change timeout for otp cache from 10 mins to 1min
    public void testOtpForFirstTimeCustomer() throws Exception{

        String invalidphoneNumber = "99860326";
        HandShakeV1 shakeV1 = new HandShakeV1();
        shakeV1.setPhoneNumber(invalidphoneNumber);
        MvcResult resultCust = mockMvc.perform(post("/customer/genotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();


        String phoneNumber = "9986032697";
        shakeV1 = new HandShakeV1();
        shakeV1.setPhoneNumber(phoneNumber);
        resultCust = mockMvc.perform(post("/customer/genotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String originalotp = resultCust.getResponse().getContentAsString();
        System.out.println(originalotp);

        resultCust = mockMvc.perform(post("/customer/verifyotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String verifiedOtp = resultCust.getResponse().getContentAsString();
        Assert.assertEquals(originalotp, verifiedOtp);

        System.out.println("Sleeping");
        Thread.sleep(120000);
        System.out.println("Sleeping done");

        resultCust = mockMvc.perform(post("/customer/verifyotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isExpectationFailed())
                .andReturn();
        verifiedOtp = resultCust.getResponse().getContentAsString();
        Assert.assertNotEquals(originalotp, verifiedOtp);

    }
}
