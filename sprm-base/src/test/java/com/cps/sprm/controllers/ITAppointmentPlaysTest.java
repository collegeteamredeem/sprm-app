package com.cps.sprm.controllers;

import com.cps.sprm.configuration.ApplicationContextConfig;
import com.cps.sprm.dao.AppointmentDao;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.cps.sprm.util.DTOUtil;
import com.cps.sprm.util.IntegrationTestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.cps.sprm.util.DataCreateCommonUtils.createCustomerData;
import static com.cps.sprm.util.DataCreateCommonUtils.generateAppointmentData;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by sumeshs on 1/6/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(classes = ApplicationContextConfig.class)
@WebAppConfiguration
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class ITAppointmentPlaysTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AppointmentDao appointmentDao;

    private MockMvc mockMvc;

    private RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();
    private CustomerV1 customerOne = DTOUtil.createCustomerDTO( "919987675124", "Customer1" , "customer1@gmail.com", new Date(), "address1");
    private CustomerV1 customerTwo = DTOUtil.createCustomerDTO( "919987675125", "Customer2" , "customer2@gmail.com", new Date(), "address2");

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        restTemplate = new RestTemplate();
        IntegrationTestUtil.setTimeout(restTemplate, 5000);
    }

    @After
    public void teardown() throws SQLException {
        restTemplate = null;
        this.mockMvc = null;
    }



    @Test
    public void testBookAptFailForValidyExpiredSP() throws Exception{
        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        CustomerV1 cust1 = createCustomerData(mockMvc, customerOne);
        System.out.println("Create customer1 with details " + cust1.toString());
        CustomerV1 cust2 = createCustomerData(mockMvc, customerTwo);
        System.out.println("Create customer2 with details " + cust2.toString());

        String startTime = "03/11/2016 09:00";
        String endTime  = "03/11/2016 10:00";
        AvailabilityV1 availabilityFirstHalfV1 = new AvailabilityV1();
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);

        try {
            availabilityFirstHalfV1.setDateTimeFrom(format.parse(startTime).getTime());
            availabilityFirstHalfV1.setDateTimeTo(format.parse(endTime).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        LocationV1[] locationV1s = serviceProviderV1.getLocations().toArray(new LocationV1[0]);
        LocationV1 locationOne = locationV1s[0];
        String uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&durationForTestOnly=31&startTime="
                + availabilityFirstHalfV1.getDateTimeFrom().toString()+"&endTime="+ availabilityFirstHalfV1.getDateTimeTo().toString();
        try{
            MvcResult result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isPaymentRequired())
                    .andReturn();

            // Increase validity by one more month
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", serviceProviderV1.getServiceproviderid(), 1)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            // now it should suceed
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

        }finally{
           deleteEntities(cust1, cust2, serviceProviderV1);
        }
    }

    private void deleteEntities(CustomerV1 customerV1, CustomerV1 customerTwo, ServiceProviderV1 serviceProvider) throws Exception{
        Set<LocationV1> locations = serviceProvider.getLocations();
        IntegrationTestUtil.deleteObjectIgnoreStatus(customerV1.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
        IntegrationTestUtil.deleteObjectIgnoreStatus(customerTwo.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
        IntegrationTestUtil.deleteObject(serviceProvider.getServiceproviderid(), mockMvc, IntegrationTestUtil.SERVICE_PROVIDER_URL_ENTITY_NAME);
        for(LocationV1 locationV1 : locations){
            IntegrationTestUtil.deleteObjectIgnoreStatus(locationV1.getLocationid(), mockMvc, IntegrationTestUtil.LOCATION_URL_ENTITY_NAME);
        }
    }

    @Test
    public void bookAndCancelAptByCustomer() throws Exception{

        // Cust1 books suceeds, tries to book same again fails
        // Cust2 tries to book slot of cust1, fails
        // Cust2 books his own slot suceeds
        // Cust1 and 2 try to cancel each others appt, fails
        // they cancel their own apt and suceed
        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        CustomerV1 cust1 = createCustomerData(mockMvc, customerOne);
        System.out.println("Create customer1 with details " + cust1.toString());
        CustomerV1 cust2 = createCustomerData(mockMvc, customerTwo);
        System.out.println("Create customer2 with details " + cust2.toString());

        try {

            // Get all appointments
            MvcResult result = mockMvc.perform(get("/apt"))
                    .andExpect(status().isOk())
                    .andReturn();

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(48, myAppointments.size());

            // Book an appointment
            AppointmentV2 toBook1 = myAppointments.get(1);

            MvcResult bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", cust1.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            AppointmentV1 appointmentV1Booked = objectMapper.readValue(bookedResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
            System.out.println("Booooked appointment " + appointmentV1Booked.toString());
            Assert.assertEquals(AppointmentStatus.BOOKED, appointmentV1Booked.getAppointmentStatus());

            MvcResult getresult =  mockMvc.perform(get("/apt/{id}", appointmentV1Booked.getAppointmentid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter get " + getresult.getResponse().getContentAsString());
            AppointmentV2 appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);
            Assert.assertEquals(cust1.getCustomerid(), appointmentV2BookedGet.getMyCustomerId());

            //Try to book again
            bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", cust1.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

            //Try Customer2 book same appointment
            bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", cust2.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

            //Try Customer2 book diff appointment
            AppointmentV2 toBook2 = myAppointments.get(2);
            bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", cust2.getCustomerid(),  toBook2.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            appointmentV1Booked = objectMapper.readValue(bookedResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
            Assert.assertEquals(AppointmentStatus.BOOKED, appointmentV1Booked.getAppointmentStatus());

            getresult =  mockMvc.perform(get("/apt/{id}", appointmentV1Booked.getAppointmentid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter get second appointment " + getresult.getResponse().getContentAsString());
            appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);
            Assert.assertEquals(cust2.getCustomerid(), appointmentV2BookedGet.getMyCustomerId());


            // Try cancelling appointment not owned by customer 2
            bookedResult = mockMvc.perform(put("/customer/cancelmyapt/{id}/{aptid}", cust2.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();


            // Cancel appointment by cust1
            bookedResult = mockMvc.perform(put("/customer/cancelmyapt/{id}/{aptid}", cust1.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            appointmentV1Booked = objectMapper.readValue(bookedResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
            Assert.assertEquals(AppointmentStatus.FREE, appointmentV1Booked.getAppointmentStatus());

             getresult =  mockMvc.perform(get("/apt/{id}", appointmentV1Booked.getAppointmentid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter get again " + getresult.getResponse().getContentAsString());
            appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);
            Assert.assertNull(appointmentV2BookedGet.getMyCustomerId());


            // Cancel appointment by cust2
            bookedResult = mockMvc.perform(put("/customer/cancelmyapt/{id}/{aptid}", cust2.getCustomerid(),  toBook2.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            appointmentV1Booked = objectMapper.readValue(bookedResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
            Assert.assertEquals(AppointmentStatus.FREE, appointmentV1Booked.getAppointmentStatus());

             getresult =  mockMvc.perform(get("/apt/{id}", appointmentV1Booked.getAppointmentid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter get again " + getresult.getResponse().getContentAsString());
            appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);
            Assert.assertNull(appointmentV2BookedGet.getMyCustomerId());


        }finally {
            deleteEntities(cust1, cust2, serviceProviderV1);
        }
    }


    @Test
    public void cancelAptBySPById() throws Exception{

        // Customer books appointments - sucess
        // SP tries to cancel without force - fail
        // SP cancels it with force - suceeds
        // Fail to book the cancelled apt
        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        CustomerV1 cust1 = createCustomerData(mockMvc, customerOne);
        CustomerV1 cust2 = createCustomerData(mockMvc, customerTwo);
        try{
            // Get all appointments
            MvcResult result = mockMvc.perform(get("/apt"))
                    .andExpect(status().isOk())
                    .andReturn();

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(48, myAppointments.size());

            // Book an appointment
            AppointmentV2 toBook1 = myAppointments.get(1);

            MvcResult bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", cust1.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            AppointmentV1 appointmentV1Booked = objectMapper.readValue(bookedResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
            Assert.assertEquals(AppointmentStatus.BOOKED, appointmentV1Booked.getAppointmentStatus());

            MvcResult getresult =  mockMvc.perform(get("/apt/{id}", appointmentV1Booked.getAppointmentid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter get " + getresult.getResponse().getContentAsString());
            AppointmentV2 appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);
            Assert.assertEquals(cust1.getCustomerid(), appointmentV2BookedGet.getMyCustomerId());

            // Cancel that same appointment by serviceprovider without force parameter
            MvcResult cancelledBySPResult = mockMvc.perform(get("/serviceprovider/apt/cancelbyid/{serviceProviderId}/{aptId}", serviceProviderV1.getServiceproviderid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

            // Cancel that same appointment by serviceprovider WITH force parameter
             cancelledBySPResult = mockMvc.perform(get("/serviceprovider/apt/cancelbyid/{serviceProviderId}/{aptId}?force=true", serviceProviderV1.getServiceproviderid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            // Get again to check status
            getresult =  mockMvc.perform(get("/apt/{id}", appointmentV1Booked.getAppointmentid()))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter get again " + getresult.getResponse().getContentAsString());
            appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);
            Assert.assertEquals(AppointmentStatus.CANCELLEDBYSP, appointmentV2BookedGet.getAppointmentStatus());
            Assert.assertNull(appointmentV2BookedGet.getMyCustomerId());

            // Fail to book an SP Cancelled appoitment
            bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", cust1.getCustomerid(),  appointmentV2BookedGet.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

        }finally{
            deleteEntities(cust1, cust2, serviceProviderV1);
        }

    }

    @Test
    public void cancelAptBySPInLocationWithoutEndDate() throws Exception{

        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        CustomerV1 cust1 = createCustomerData(mockMvc, customerOne);
        CustomerV1 cust2 = createCustomerData(mockMvc, customerTwo);
        try{
            // Get all appointments
            MvcResult result = mockMvc.perform(get("/apt"))
                    .andExpect(status().isOk())
                    .andReturn();

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(48, myAppointments.size());

            // Cancel all appoitments in a location

            LocationV1[] locationV1s = serviceProviderV1.getLocations().toArray(new LocationV1[0]);

            LocationV1 locationOne = locationV1s[0];
            LocationV1 locationTwo = locationV1s[1];

            Assert.assertNotEquals(locationOne.getLocationid(), locationTwo.getLocationid());

            System.out.println("DEaling with location " + locationOne.toString());

            String startTime = "03/11/2016 09:00";
            String endTime  = "03/11/2016 10:00";
            AvailabilityV1 availabilityFirstHalfV1 = new AvailabilityV1();
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);

            try {
                availabilityFirstHalfV1.setDateTimeFrom(format.parse(startTime).getTime());
                availabilityFirstHalfV1.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            startTime = "04/11/2016 09:00";
            endTime  = "04/11/2016 10:00";
            AvailabilityV1 availabilitySecondHalfV1 = new AvailabilityV1();
            try {
                availabilitySecondHalfV1.setDateTimeFrom(format.parse(startTime).getTime());
                availabilitySecondHalfV1.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }


            // cancel without force - suceeds, since none is booked
            result = mockMvc.perform(post("/serviceprovider/apt/cancelbyloc/{serviceProviderId}/{locationid}", serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityFirstHalfV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();


/*            String uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime="
                    + availabilityFirstHalfV1.getDateTimeFrom().toString()+"&endTime="+ availabilityFirstHalfV1.getDateTimeTo().toString();

            //all apts in that availability are cancelled, so free =0 and cancelledBySP is 12
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();*/

            String uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime=" + availabilityFirstHalfV1.getDateTimeFrom().toString();

            //all apts in that availability are cancelled, so free =0 and cancelledBySP is 12
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            List<AppointmentV2> myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(0, myFreeAppointments.size());

            uri = "/apt/{serviceProviderId}/{locationid}?status=CANCELLEDBYSP&startTime=" + availabilityFirstHalfV1.getDateTimeFrom().toString();
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            List<AppointmentV2> mySPCancelledAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(12, mySPCancelledAppointments.size());

            startTime = "03/11/2016 17:00";
            endTime  = "04/11/2016 18:00";

            AvailabilityV1 availabilityForSecondLocation = new AvailabilityV1();
            try {
                availabilityForSecondLocation.setDateTimeFrom(format.parse(startTime).getTime());
                availabilityForSecondLocation.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime="
                    + availabilityForSecondLocation.getDateTimeFrom().toString();
            System.out.println("Location 2 id is " + locationTwo.getLocationid());
            // No impact in location 2, all are free
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationTwo.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(12, myFreeAppointments.size());


        }finally{
            deleteEntities(cust1, cust2, serviceProviderV1);
        }

    }

    @Test
    public void cancelAptBySPInLocation() throws Exception{

        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        CustomerV1 cust1 = createCustomerData(mockMvc, customerOne);
        CustomerV1 cust2 = createCustomerData(mockMvc, customerTwo);
        try{
            // Get all appointments
            MvcResult result = mockMvc.perform(get("/apt"))
                    .andExpect(status().isOk())
                    .andReturn();

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(48, myAppointments.size());

            // Cancel all appoitments in a location

            LocationV1[] locationV1s = serviceProviderV1.getLocations().toArray(new LocationV1[0]);

            LocationV1 locationOne = locationV1s[0];
            LocationV1 locationTwo = locationV1s[1];

            Assert.assertNotEquals(locationOne.getLocationid(), locationTwo.getLocationid());

            System.out.println("DEaling with location " + locationOne.toString());

            String startTime = "03/11/2016 09:00";
            String endTime  = "03/11/2016 10:00";
            AvailabilityV1 availabilityFirstHalfV1 = new AvailabilityV1();
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);

            try {
                availabilityFirstHalfV1.setDateTimeFrom(format.parse(startTime).getTime());
                availabilityFirstHalfV1.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            startTime = "04/11/2016 09:00";
            endTime  = "04/11/2016 10:00";
            AvailabilityV1 availabilitySecondHalfV1 = new AvailabilityV1();
            try {
                availabilitySecondHalfV1.setDateTimeFrom(format.parse(startTime).getTime());
                availabilitySecondHalfV1.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }


            // cancel without force - suceeds, since none is booked
            result = mockMvc.perform(post("/serviceprovider/apt/cancelbyloc/{serviceProviderId}/{locationid}", serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityFirstHalfV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();


            String uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime="
                    + availabilityFirstHalfV1.getDateTimeFrom().toString()+"&endTime="+ availabilityFirstHalfV1.getDateTimeTo().toString();

            //all apts in that availability are cancelled, so free =0 and cancelledBySP is 12
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            List<AppointmentV2> myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(0, myFreeAppointments.size());

            uri = "/apt/{serviceProviderId}/{locationid}?status=CANCELLEDBYSP&startTime="
                    + availabilityFirstHalfV1.getDateTimeFrom().toString()+"&endTime="+ availabilityFirstHalfV1.getDateTimeTo().toString();
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            List<AppointmentV2> mySPCancelledAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(12, mySPCancelledAppointments.size());

            startTime = "03/11/2016 17:00";
            endTime  = "04/11/2016 18:00";

            AvailabilityV1 availabilityForSecondLocation = new AvailabilityV1();
            try {
                availabilityForSecondLocation.setDateTimeFrom(format.parse(startTime).getTime());
                availabilityForSecondLocation.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime="
                    + availabilityForSecondLocation.getDateTimeFrom().toString()+"&endTime="+ availabilityForSecondLocation.getDateTimeTo().toString();
            System.out.println("Location 2 id is " + locationTwo.getLocationid());
            // No impact in location 2, all are free
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationTwo.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(24, myFreeAppointments.size());


        }finally{
            deleteEntities(cust1, cust2, serviceProviderV1);
        }

    }

    @Test
    public void cancelAptBySPInAllLocation() throws Exception{

        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        CustomerV1 cust1 = createCustomerData(mockMvc, customerOne);
        CustomerV1 cust2 = createCustomerData(mockMvc, customerTwo);


        try{
            // Get all appointments
            MvcResult result = mockMvc.perform(get("/apt"))
                    .andExpect(status().isOk())
                    .andReturn();

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(48, myAppointments.size());

            // Cancel all appoitments in a location

            LocationV1[] locationV1s = serviceProviderV1.getLocations().toArray(new LocationV1[0]);

            LocationV1 locationOne = locationV1s[0];
            LocationV1 locationTwo = locationV1s[1];

            Assert.assertNotEquals(locationOne.getLocationid(), locationTwo.getLocationid());

            System.out.println("DEaling with location " + locationOne.toString());

            String startTime = "03/11/2016 09:00";
            String endTime  = "04/11/2016 10:00";
            AvailabilityV1 availabilityMorning = new AvailabilityV1();
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);

            try {
                availabilityMorning.setDateTimeFrom(format.parse(startTime).getTime());
                availabilityMorning.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            startTime = "03/11/2016 17:00";
            endTime = "04/11/2016 18:00";
            AvailabilityV1 availabilityEvening = new AvailabilityV1();

            try {
                availabilityEvening.setDateTimeFrom(format.parse(startTime).getTime());
                availabilityEvening.setDateTimeTo(format.parse(endTime).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }


            // cancel without force - suceeds, since none is booked
            result = mockMvc.perform(post("/serviceprovider/apt/cancelbyallloc/{serviceProviderId}", serviceProviderV1.getServiceproviderid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityMorning))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            result = mockMvc.perform(post("/serviceprovider/apt/cancelbyallloc/{serviceProviderId}", serviceProviderV1.getServiceproviderid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityEvening))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();


           String uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime="
                    + availabilityMorning.getDateTimeFrom().toString()+"&endTime="+ availabilityMorning.getDateTimeTo().toString();

            //all apts in that availability are cancelled, so free =0 and cancelledBySP is 12
            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationOne.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            List<AppointmentV2> myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(0, myFreeAppointments.size());

            uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime="
                    + availabilityEvening.getDateTimeFrom().toString()+"&endTime="+ availabilityEvening.getDateTimeTo().toString();

            result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationTwo.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(0, myFreeAppointments.size());


        }finally{
            deleteEntities(cust1, cust2, serviceProviderV1);
        }
    }




    @Test
    public void testBookAppointmentBySameCustomerAgainSameDaySameSPNotPossible() throws Exception{

    }

}
