package com.cps.sprm.controllers;

import com.cps.sprm.configuration.ApplicationContextConfig;
import com.cps.sprm.dao.AppointmentDao;
import com.cps.sprm.sdk.dto.*;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.cps.sprm.util.DTOUtil;
import com.cps.sprm.util.IntegrationTestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.List;
import java.util.TreeSet;
import java.util.Locale;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import static com.cps.sprm.util.DataCreateCommonUtils.createCustomerData;
import static com.cps.sprm.util.DataCreateCommonUtils.generateAppointmentData;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by sumeshs on 1/26/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(classes = ApplicationContextConfig.class)
@WebAppConfiguration
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class ITLiveUpdateControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AppointmentDao appointmentDao;

    private MockMvc mockMvc;

    private RestTemplate restTemplate;

    private static final Logger logger = Logger.getLogger(ITLiveUpdateControllerTest.class);

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        restTemplate = new RestTemplate();
        IntegrationTestUtil.setTimeout(restTemplate, 5000);
    }

    @After
    public void teardown() throws SQLException {
        restTemplate = null;
        this.mockMvc = null;
    }

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void createLiveDataForSP() throws Exception{

        Long phoneNumber = 919987675124L;
        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        Set<CustomerV1> customerV1Set = new TreeSet<CustomerV1>();
        for(int i = 1; i<= 10; i++){
            phoneNumber += 1 ;
            String customer = "Customer" + Integer.toString(i);
            String address = "Address" + Integer.toString(i);
            String email = customer + "@gmail.com";

            customerV1Set.add(createCustomerData(mockMvc,
                    DTOUtil.createCustomerDTO(phoneNumber.toString() , customer , email, new Date(), address)));
        }

        LocationV1[] locationV1s = serviceProviderV1.getLocations().toArray(new LocationV1[0]);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
        AvailabilityV1 availabilityFirstHalfV1 = new AvailabilityV1();

        try{
            // Get all appointments
            String startTime = "03/11/2016 09:00";
            String endTime = "03/11/2016 10:00";

            List<AppointmentV2> myFreeAppointments = getAppointmentsForTimeStamp(serviceProviderV1, format.parse(startTime).getTime(),
                    format.parse(endTime).getTime(), availabilityFirstHalfV1, locationV1s[0], false);
            bookAppointmentsByCustomer(customerV1Set, myFreeAppointments);


          /*  startTime = "04/11/2016 09:00";
            endTime = "04/11/2016 10:00";
            myFreeAppointments = getAppointmentsForTimeStamp(serviceProviderV1, format.parse(startTime).getTime(),
                    format.parse(endTime).getTime(), availabilityFirstHalfV1, locationV1s[0], false);

            bookAppointmentsByCustomer(customerV1Set, myFreeAppointments);


            startTime = "03/11/2016 17:00";
            endTime = "03/11/2016 18:00";
            myFreeAppointments = getAppointmentsForTimeStamp(serviceProviderV1, format.parse(startTime).getTime(),
                    format.parse(endTime).getTime(), availabilityFirstHalfV1, locationV1s[1], false);

            bookAppointmentsByCustomer(customerV1Set, myFreeAppointments);

            startTime = "04/11/2016 17:00";
            endTime = "04/11/2016 18:00";
            myFreeAppointments = getAppointmentsForTimeStamp(serviceProviderV1, format.parse(startTime).getTime(),
                    format.parse(endTime).getTime(), availabilityFirstHalfV1, locationV1s[1], false);

            bookAppointmentsByCustomer(customerV1Set, myFreeAppointments);*/


            startTime = "03/11/2016 09:00";
            List<AppointmentV2> myAllAppointmentsInLocationOneMorning = getAppointmentsForTimeStamp(serviceProviderV1, format.parse(startTime).getTime(),
                    format.parse(endTime).getTime(), availabilityFirstHalfV1, locationV1s[0], true);

            Assert.assertEquals(12, myAllAppointmentsInLocationOneMorning.size());
            // start processing the appointments
            if(myAllAppointmentsInLocationOneMorning.size() > 0) {
                LiveUpdateV2 liveUpdateV2 = new LiveUpdateV2();
                liveUpdateV2.setMyServiceProviderId(serviceProviderV1.getServiceproviderid());
                liveUpdateV2.setMyLocationId(locationV1s[0].getLocationid());
                liveUpdateV2.setTotalTokens(myAllAppointmentsInLocationOneMorning.size());

                MvcResult result = mockMvc.perform(post("/live/{serviceProviderId}/{locationid}", serviceProviderV1.getServiceproviderid(), locationV1s[0].getLocationid())
                        .content(IntegrationTestUtil.asJsonString(liveUpdateV2))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();

                System.out.println("AFter post of liveUpdate " + result.getResponse().getContentAsString());
                logger.info("##############AFter post of liveUpdate " + result.getResponse().getContentAsString());
                LiveUpdateV2 liveUpdateV2AfterPost = objectMapper.readValue(result.getResponse().getContentAsByteArray(), LiveUpdateV2.class);

                int totalProcessed = 0;
                for(AppointmentV2 appointmentV2: myAllAppointmentsInLocationOneMorning){
                    // Get each time
                    logger.info("#### Dealing with appointment " + appointmentV2.getTokenNumber());
                    MvcResult getresult =  mockMvc.perform(get("/apt/{id}", appointmentV2.getAppointmentid()))
                            .andExpect(status().isOk())
                            .andReturn();
                    AppointmentV2 appointmentV2BookedGet = objectMapper.readValue(getresult.getResponse().getContentAsByteArray(), AppointmentV2.class);

                    tokenBeingProcessed(liveUpdateV2AfterPost, appointmentV2BookedGet);
                    if(appointmentV2BookedGet.getAppointmentStatus().equals(AppointmentStatus.BOOKED)){
                        totalProcessed++;
                        //CallOut()
                        int randomNum = ThreadLocalRandom.current().nextInt(1, 5 + 1);
                        Thread.sleep(randomNum*1000);
                        markAsProcessed(appointmentV2BookedGet);
                    }else {
                        markAsProcessed(appointmentV2BookedGet);
                    }
                }

                MvcResult liveUpdateAtTheEndGetResult =  mockMvc.perform(get("/live/{id}", liveUpdateV2AfterPost.getLiveupdateid()))
                        .andExpect(status().isOk())
                        .andReturn();
                LiveUpdateV2 livUpdateAtTheEnd = objectMapper.readValue(liveUpdateAtTheEndGetResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);

                // Reset the number of tokens actually processed
                livUpdateAtTheEnd.setTotalTokens(totalProcessed);
                MvcResult putResult = mockMvc.perform(put("/live/end/{id}", livUpdateAtTheEnd.getLiveupdateid())
                        .content(IntegrationTestUtil.asJsonString(livUpdateAtTheEnd))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andReturn();
                System.out.println("AFter put at end of liveUpdate " + putResult.getResponse().getContentAsString());
                livUpdateAtTheEnd = objectMapper.readValue(putResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
                Assert.assertEquals(10, livUpdateAtTheEnd.getTotalTokens().intValue());
                Assert.assertEquals(12, livUpdateAtTheEnd.getCurrentToken().intValue());
                Assert.assertNotNull(livUpdateAtTheEnd.getAvgConsultingTime());
                Assert.assertNotNull(livUpdateAtTheEnd.getConsultingStartTime());
                Assert.assertNotNull(livUpdateAtTheEnd.getConsultingEndTime());
            }



        }finally{
            for(CustomerV1 customerV1 : customerV1Set){
                IntegrationTestUtil.deleteObjectIgnoreStatus(customerV1.getCustomerid(), mockMvc, IntegrationTestUtil.CUSTOMER_URL_ENTITY_NAME);
            }
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, serviceProviderV1);
        }

    }

    private void tokenBeingProcessed(LiveUpdateV2 liveUpdateV2, AppointmentV2 appointmentV2) throws Exception{

        logger.info("### Updating liveUpdateCurrent token to " + appointmentV2.getTokenNumber());
        liveUpdateV2.setCurrentToken(appointmentV2.getTokenNumber());
        MvcResult putResult = mockMvc.perform(put("/live/update/{id}", liveUpdateV2.getLiveupdateid())
                .content(IntegrationTestUtil.asJsonString(liveUpdateV2))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        LiveUpdateV2 liveUpdate = objectMapper.readValue(putResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
        logger.info("#### After liveUpdate " + putResult.getResponse().getContentAsString());
        Assert.assertEquals(appointmentV2.getTokenNumber(), liveUpdate.getCurrentToken());
    }
    private void markAsProcessed(AppointmentV2 appointmentV2) throws Exception {
        appointmentV2.setAppointmentStatus(AppointmentStatus.PROCESSED);
        MvcResult putResult = mockMvc.perform(put("/apt/update/{id}", appointmentV2.getAppointmentid())
                .content(IntegrationTestUtil.asJsonString(appointmentV2))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        AppointmentV1 appointmentV1 = objectMapper.readValue(putResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
        Assert.assertEquals(AppointmentStatus.PROCESSED, appointmentV1.getAppointmentStatus());
    }

    private List<AppointmentV2> getAppointmentsForTimeStamp(ServiceProviderV1 serviceProviderV1, long fromTime, long toTime,
                                                            AvailabilityV1 availabilityFirstHalfV1, LocationV1 locationV1, boolean igNoreStatus) throws Exception {
        String uri;
        MvcResult result;
        List<AppointmentV2> myFreeAppointments;
        try {
            availabilityFirstHalfV1.setDateTimeFrom(fromTime);
            availabilityFirstHalfV1.setDateTimeTo(toTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(igNoreStatus){
            uri = "/aptall/{serviceProviderId}/{locationid}?startTime=" + availabilityFirstHalfV1.getDateTimeFrom().toString();
        }else {
            uri = "/apt/{serviceProviderId}/{locationid}?status=FREE&startTime=" + availabilityFirstHalfV1.getDateTimeFrom().toString();
        }

        result = mockMvc.perform(get(uri, serviceProviderV1.getServiceproviderid(), locationV1.getLocationid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        myFreeAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
        Assert.assertEquals(12, myFreeAppointments.size());
        return myFreeAppointments;
    }

    private void bookAppointmentsByCustomer(Set<CustomerV1> customerV1Set, List<AppointmentV2> myFreeAppointments) throws Exception {
        int i = 0;
        for(CustomerV1 customerV1 : customerV1Set){
            AppointmentV2 toBook1 = myFreeAppointments.get(i++);

            MvcResult bookedResult = mockMvc.perform(put("/customer/bookapt/{id}/{aptid}", customerV1.getCustomerid(),  toBook1.getAppointmentid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            AppointmentV1 appointmentV1Booked = objectMapper.readValue(bookedResult.getResponse().getContentAsByteArray(), AppointmentV1.class);
            System.out.println("Booooked appointment " + appointmentV1Booked.toString());
            Assert.assertEquals(AppointmentStatus.BOOKED, appointmentV1Booked.getAppointmentStatus());

        }
    }


    @Test
    @Ignore
    public void createLiveDataCaching() throws Exception{

        ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);

        MvcResult spresult = mockMvc.perform(get("/serviceprovider/{serviceProviderId}", serviceProviderV1.getServiceproviderid()))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("After GET Result= "  + spresult.getResponse().getContentAsString());
        serviceProviderV1 = objectMapper.readValue(spresult.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

      ///  ServiceProviderV1 serviceProviderV1 = generateAppointmentData(mockMvc);
        LocationV1[] locationV1s = serviceProviderV1.getLocations().toArray(new LocationV1[0]);

        try{
            LiveUpdateV2 liveUpdateV2 = new LiveUpdateV2();
            liveUpdateV2.setMyServiceProviderId(serviceProviderV1.getServiceproviderid());
            liveUpdateV2.setMyLocationId(locationV1s[0].getLocationid());
            liveUpdateV2.setTotalTokens(10);

            MvcResult result = mockMvc.perform(post("/live/{serviceProviderId}/{locationid}", serviceProviderV1.getServiceproviderid(), locationV1s[0].getLocationid())
                    .content(IntegrationTestUtil.asJsonString(liveUpdateV2))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("AFter post of liveUpdate " + result.getResponse().getContentAsString());
            logger.info("##############AFter post of liveUpdate " + result.getResponse().getContentAsString());
            LiveUpdateV2 liveUpdateV2AfterPost = objectMapper.readValue(result.getResponse().getContentAsByteArray(), LiveUpdateV2.class);


            System.out.println("Trying GET 1");
            MvcResult liveUpdateAtTheEndGetResult =  mockMvc.perform(get("/live/{id}", liveUpdateV2AfterPost.getLiveupdateid()))
                    .andExpect(status().isOk())
                    .andReturn();
            LiveUpdateV2 livUpdateAtTheEnd = objectMapper.readValue(liveUpdateAtTheEndGetResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
            System.out.println("AFter GET 1 of liveUpdate " + liveUpdateAtTheEndGetResult.getResponse().getContentAsString());
            System.out.println("Sleeping");
            Thread.sleep(120000);
            System.out.println("Sleeping done");
            System.out.println("Trying GET 2 ");
            liveUpdateAtTheEndGetResult =  mockMvc.perform(get("/live/{id}", liveUpdateV2AfterPost.getLiveupdateid()))
                    .andExpect(status().isOk())
                    .andReturn();
            livUpdateAtTheEnd = objectMapper.readValue(liveUpdateAtTheEndGetResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
            System.out.println("AFter GET 2 of liveUpdate " + liveUpdateAtTheEndGetResult.getResponse().getContentAsString());


            livUpdateAtTheEnd.setCurrentToken(2);
            MvcResult putResult = mockMvc.perform(put("/live/update/{id}", livUpdateAtTheEnd.getLiveupdateid())
                    .content(IntegrationTestUtil.asJsonString(livUpdateAtTheEnd))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            LiveUpdateV2 liveUpdate = objectMapper.readValue(putResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
            logger.info("#### After liveUpdate PUT " + putResult.getResponse().getContentAsString());
            Assert.assertEquals(new Integer(2), liveUpdate.getCurrentToken());

            System.out.println("Trying GET 3");
            liveUpdateAtTheEndGetResult =  mockMvc.perform(get("/live/{id}", liveUpdateV2AfterPost.getLiveupdateid()))
                    .andExpect(status().isOk())
                    .andReturn();
            livUpdateAtTheEnd = objectMapper.readValue(liveUpdateAtTheEndGetResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
            System.out.println("AFter GET 3 of liveUpdate " + liveUpdateAtTheEndGetResult.getResponse().getContentAsString());

            putResult = mockMvc.perform(put("/live/end/{id}", livUpdateAtTheEnd.getLiveupdateid())
                    .content(IntegrationTestUtil.asJsonString(livUpdateAtTheEnd))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            System.out.println("AFter put at end of liveUpdate " + putResult.getResponse().getContentAsString());
            livUpdateAtTheEnd = objectMapper.readValue(putResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);

            System.out.println("Trying GET 4");
            liveUpdateAtTheEndGetResult =  mockMvc.perform(get("/live/{id}", liveUpdateV2AfterPost.getLiveupdateid()))
                    .andExpect(status().isOk())
                    .andReturn();
            livUpdateAtTheEnd = objectMapper.readValue(liveUpdateAtTheEndGetResult.getResponse().getContentAsByteArray(), LiveUpdateV2.class);
            System.out.println("AFter GET 4 of liveUpdate " + liveUpdateAtTheEndGetResult.getResponse().getContentAsString());

        }finally{
            //IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, serviceProviderV1);
        }

    }

}
