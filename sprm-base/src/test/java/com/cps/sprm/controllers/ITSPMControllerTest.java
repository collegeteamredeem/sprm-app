package com.cps.sprm.controllers;

import com.cps.sprm.configuration.ApplicationContextConfig;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.sdk.dto.HandShakeV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.util.DTOUtil;
import com.cps.sprm.util.IntegrationTestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.cps.sprm.util.DataCreateCommonUtils.generateAvailabilityData;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by sumeshs on 11/12/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(classes = ApplicationContextConfig.class)
@WebAppConfiguration
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class ITSPMControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup(){

        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        restTemplate = new RestTemplate();
        IntegrationTestUtil.setTimeout(restTemplate, 5000);
    }

    @After
    public void after() throws SQLException {
        restTemplate = null;
        this.mockMvc = null;
    }


    @Test
    public void getByInvalidIdUsingMockMVC() throws Exception{
        MvcResult result = mockMvc.perform(get("/serviceprovider/{id}", 100L))
                .andExpect(status().is4xxClientError())
                .andReturn();

        System.out.println("Result= "  + result.getResponse().getContentAsString());
    }

    @Test
    public void getUsingMockMVCOnEmptyDB() throws Exception{
        MvcResult result = mockMvc.perform(get("/serviceprovider"))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("Result= "  + result.getResponse().getContentAsString());
    }

    @Test
    public void postUsingMockMVC() throws Exception{
        MvcResult result = mockMvc.perform(post("/serviceprovider").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"phonenumber\": \"919987675124\",\n" +
                        "\t\"name\": \"Umesh\",\n" +
                        "\t\"email\": \"Umesh@gmail.com\",\n" +
                        "\t\"assistantonephoneno\": \"1111111122\",\n" +
                        "\t\"assistanttwophoneno\": \"2222222233\",\n" +
                        "\t\"title\": \"Dr\",\n" +
                        "\t\"qualification\": \"MBBS\",\n" +
                        "\t\"experience\": 10,\n" +
                        "\t\"locations\": [{\n" +
                        "\t\t\"fulladdress\": \"Full Address 1 New\",\n" +
                        "\t\t\"name\": \"Name 1 New\",\n" +
                        "\t\t\"pincode\": \"560017\",\n" +
                        "\t\t\"city\": \"City 1\",\n" +
                        "\t\t\"lat\": 18.45,\n" +
                        "\t\t\"lon\": -18.55,\n" +
                        "\t\t\"myavailability\": [],\n" +
                        "\t\t\"serviceProviders\": []\n" +
                        "\t}, {\n" +
                        "\t\t\"fulladdress\": \"Full Address 2 New\",\n" +
                        "\t\t\"name\": \"Name 2 New\",\n" +
                        "\t\t\"pincode\": \"560018\",\n" +
                        "\t\t\"city\": \"City 2\",\n" +
                        "\t\t\"lat\": 18.451,\n" +
                        "\t\t\"lon\": -18.551,\n" +
                        "\t\t\"myavailability\": [],\n" +
                        "\t\t\"serviceProviders\": []\n" +
                        "\t}],\n" +
                        "\t\"myavailability\": [],\n" +
                        "\t\"specializations\": [\"DENTAL\", \"ORTHO\"]\n" +
                        "}"))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("Result= "  + result.getResponse().getContentAsString());

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        try {
            Assert.assertEquals("919987675124", spForAssert.getPhonenumber());
            Assert.assertEquals("Umesh@gmail.com", spForAssert.getEmail());
        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }
    }


    @Test
    public void postUsingMockMVCObject() throws Exception{
        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675123", "Sumesh", "Sumesh@gmail.com", "560016", "560017");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("After POST Result= "  + result.getResponse().getContentAsString());
        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        //GET
        result = mockMvc.perform(get("/serviceprovider/{serviceProviderId}", spForAssert.getServiceproviderid()))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("After GET Result= "  + result.getResponse().getContentAsString());
        spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        System.out.println(spForAssert.toString());
        try {
            Assert.assertEquals("919987675123", spForAssert.getPhonenumber());
            Assert.assertEquals("Sumesh@gmail.com", spForAssert.getEmail());
        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }
    }

    @Test
    public void testDeleteForServiceProvider () throws Exception{
        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675133", "Doc5", "doc5@gmail.com", "560036", "560037");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("After POST Result= " + result.getResponse().getContentAsString());
        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        try {

            IntegrationTestUtil.deleteObject(spForAssert.getServiceproviderid(), mockMvc, IntegrationTestUtil.SERVICE_PROVIDER_URL_ENTITY_NAME);

            mockMvc.perform(get("/serviceprovider/{id}", spForAssert.getServiceproviderid()))
                    .andExpect(status().is4xxClientError())
                    .andReturn();
        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }
    }

    @SuppressWarnings("Since15")
    @Test
    public void testCreateForAvailability() throws Exception{

        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675134", "Doc7", "doc7@gmail.com", "560041", "560042");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        List<String> holidays = new ArrayList<String>();
        holidays.add("09/11/2016");
        holidays.add("10/11/2016");

        AvailabilityV1 availabilityV1 = generateAvailabilityData("03/11/2016 09:00", "03/11/2016 10:00", holidays, 5);

        LocationV1 locationV1 = spForAssert.getLocations().iterator().next();
        locationV1.addToMyavailability(availabilityV1);

        System.out.println("Data to be sent " + availabilityV1.toString());
        System.out.println("Json to be sent " + IntegrationTestUtil.convertObjectToJsonBytes(availabilityV1));
        System.out.println("Json to be sent String " + IntegrationTestUtil.asJsonString(availabilityV1));
        try {
            result = mockMvc.perform(put("/serviceprovider/availability/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationV1.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After PUT Result= "  + result.getResponse().getContentAsString());

            ServiceProviderV1 spAfterFirstUpdate = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
            Assert.assertEquals(1, spAfterFirstUpdate.getMyavailability().size());
            AvailabilityV1 availability = spAfterFirstUpdate.getMyavailability().iterator().next();
            Assert.assertEquals(2, availability.getHolidays().size());
            Assert.assertEquals(2, availability.getWeeklyoff().size());

            result = mockMvc.perform(get("/serviceprovider/{id}", spAfterFirstUpdate.getServiceproviderid()))
                    .andExpect(status().isOk())
                    .andReturn();
            System.out.println("After GET Again Result= "  + result.getResponse().getContentAsString());
            ServiceProviderV1 spAfterGet = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

            boolean found = false;
            for(Iterator<LocationV1> locationV1Iterator = spAfterGet.getLocations().iterator(); locationV1Iterator.hasNext();){
                LocationV1 loc = locationV1Iterator.next();
                if(loc.getLocationid().equals(locationV1.getLocationid())){
                    found = true;
                    Assert.assertEquals(1, loc.getMyavailability().size());
                    break;
                }
            }
            Assert.assertTrue(found);

        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }

    }


    @Test
    public void testValidityPopulation() throws Exception{
        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675123", "Sumesh", "Sumesh@gmail.com", "560016", "560017");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        Set<LocationV1> locations = spForAssert.getLocations();

        //GET
        result = mockMvc.perform(get("/serviceprovider/{id}", spForAssert.getServiceproviderid()))
                .andExpect(status().isOk())
                .andReturn();

        spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        System.out.println(spForAssert.toString());
        try {
            Assert.assertEquals("919987675123", spForAssert.getPhonenumber());
            Assert.assertEquals("Sumesh@gmail.com", spForAssert.getEmail());

            Long currentTime = System.currentTimeMillis();
            java.time.LocalDateTime toDayDateTime = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(currentTime), ZoneId.of("UTC"));
            java.time.LocalDate todayDate = toDayDateTime.toLocalDate();

            java.time.LocalDateTime dateTimeIDB = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(spForAssert.getValidity()), ZoneId.of("UTC"));
            java.time.LocalDate dateInDB = dateTimeIDB.toLocalDate();
            java.time.LocalDate expectedDate = todayDate.plusDays(30);

            Assert.assertEquals(dateInDB, expectedDate);

        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }
    }

    @Test
    public void testValidityUpdation() throws Exception{
        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675123", "Sumesh", "Sumesh@gmail.com", "560016", "560017");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        //GET
        result = mockMvc.perform(get("/serviceprovider/{id}", spForAssert.getServiceproviderid()))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("After GET Result= "  + result.getResponse().getContentAsString());
        spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
        System.out.println(spForAssert.toString());
        try {

            Long currentTime = System.currentTimeMillis();
            java.time.LocalDateTime toDayDateTime = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(currentTime), ZoneId.of("UTC"));
            java.time.LocalDate todayDate = toDayDateTime.toLocalDate();

            java.time.LocalDateTime dateTimeInDB = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(spForAssert.getValidity()), ZoneId.of("UTC"));
            java.time.LocalDate dateInDB = dateTimeInDB.toLocalDate();

            java.time.LocalDate expectedDate = todayDate.plusDays(30);

            Assert.assertEquals(dateInDB, expectedDate);

            // try 3 months - fail
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", spForAssert.getServiceproviderid(), 3)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

            // try 8 months - fail
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", spForAssert.getServiceproviderid(), 8)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andReturn();

            // try 1 months - pass
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", spForAssert.getServiceproviderid(), 1)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After One month update Result= "  + result.getResponse().getContentAsString());
            spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

            java.time.LocalDateTime dateTimeAfterOneMonthInDB = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(spForAssert.getValidity()), ZoneId.of("UTC"));

            dateInDB = dateTimeAfterOneMonthInDB.toLocalDate();
            expectedDate = expectedDate.plusMonths(1);
            Assert.assertEquals(dateInDB, expectedDate);

            // try 6 months - pass
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", spForAssert.getServiceproviderid(), 6)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After Six month update Result= "  + result.getResponse().getContentAsString());
            spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

            java.time.LocalDateTime dateTimeAfterSixMonthInDB = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(spForAssert.getValidity()), ZoneId.of("UTC"));

            dateInDB = dateTimeAfterSixMonthInDB.toLocalDate();
            expectedDate = expectedDate.plusMonths(6);
            Assert.assertEquals(dateInDB, expectedDate);

            // try 12 months - pass
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", spForAssert.getServiceproviderid(), 12)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After Twelve month update Result= "  + result.getResponse().getContentAsString());
            spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

            java.time.LocalDateTime dateTimeAfter12MonthInDB = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(spForAssert.getValidity()), ZoneId.of("UTC"));

            dateInDB = dateTimeAfter12MonthInDB.toLocalDate();
            expectedDate = expectedDate.plusMonths(12);
            Assert.assertEquals(dateInDB, expectedDate);

        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }
    }

    @Test
    public void testValidityExpiryAndUnableToGenerateAppointments() throws Exception{

        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675134", "Doc7", "doc7@gmail.com", "560041", "560042");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        List<String> holidays = new ArrayList<String>();
        holidays.add("09/11/2016");
        holidays.add("10/11/2016");

        AvailabilityV1 availabilityV1 = generateAvailabilityData("03/11/2016 09:00", "11/11/2016 10:00", holidays, 30);

        LocationV1 locationV1 = spForAssert.getLocations().iterator().next();
        locationV1.addToMyavailability(availabilityV1);

        try {
            result = mockMvc.perform(put("/serviceprovider/availability/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationV1.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After PUT Result= "  + result.getResponse().getContentAsString());

            ServiceProviderV1 spAfterFirstUpdate = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
            Assert.assertEquals(1, spAfterFirstUpdate.getMyavailability().size());
            AvailabilityV1 availability = spAfterFirstUpdate.getMyavailability().iterator().next();
            Assert.assertEquals(2, availability.getHolidays().size());
            Assert.assertEquals(2, availability.getWeeklyoff().size());

            result = mockMvc.perform(get("/serviceprovider/{id}", spAfterFirstUpdate.getServiceproviderid()))
                    .andExpect(status().isOk())
                    .andReturn();
            System.out.println("After GET Again Result= "  + result.getResponse().getContentAsString());
            ServiceProviderV1 spAfterGet = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

            boolean found = false;
            for(Iterator<LocationV1> locationV1Iterator = spAfterGet.getLocations().iterator(); locationV1Iterator.hasNext();){
                LocationV1 loc = locationV1Iterator.next();
                if(loc.getLocationid().equals(locationV1.getLocationid())){
                    found = true;
                    Assert.assertEquals(1, loc.getMyavailability().size());
                    break;
                }
            }
            Assert.assertTrue(found);

            result = mockMvc.perform(get("/serviceprovider/apt/{serviceProviderId}/{locationid}?durationForTestOnly=32", spForAssert.getServiceproviderid(), locationV1.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isPaymentRequired())
                    .andReturn();

            // Increase validity by one more month
            result = mockMvc.perform(patch("/serviceprovider/{serviceProviderId}/validity/{validMonth}", spForAssert.getServiceproviderid(), 1)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            // now it should suceed
            result = mockMvc.perform(get("/serviceprovider/apt/{serviceProviderId}/{locationid}?durationForTestOnly=32", spForAssert.getServiceproviderid(), locationV1.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(10, myAppointments.size());

            System.out.println("After generation of appointments Again Result= "  + result.getResponse().getContentAsString());

        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }
    }


    @SuppressWarnings("Since15")
    @Test
    public void testAutoGenerateAppointments() throws Exception{

        ServiceProviderV1 serviceProviderV1ForPost = DTOUtil.createSPDTO("919987675134", "Doc7", "doc7@gmail.com", "560041", "560042");
        MvcResult result = mockMvc.perform(post("/serviceprovider")
                .content(IntegrationTestUtil.asJsonString(serviceProviderV1ForPost))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ServiceProviderV1 spForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

        List<String> holidays = new ArrayList<String>();
        holidays.add("09/11/2016");
        holidays.add("10/11/2016");

        AvailabilityV1 availabilityV1 = generateAvailabilityData("03/11/2016 09:00", "11/11/2016 10:00", holidays, 30);

        LocationV1 locationV1 = spForAssert.getLocations().iterator().next();
        locationV1.addToMyavailability(availabilityV1);

        try {
            result = mockMvc.perform(put("/serviceprovider/availability/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationV1.getLocationid())
                    .content(IntegrationTestUtil.convertObjectToJsonBytes(availabilityV1))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After PUT Result= "  + result.getResponse().getContentAsString());

            ServiceProviderV1 spAfterFirstUpdate = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);
            Assert.assertEquals(1, spAfterFirstUpdate.getMyavailability().size());
            AvailabilityV1 availability = spAfterFirstUpdate.getMyavailability().iterator().next();
            Assert.assertEquals(2, availability.getHolidays().size());
            Assert.assertEquals(2, availability.getWeeklyoff().size());

            result = mockMvc.perform(get("/serviceprovider/{id}", spAfterFirstUpdate.getServiceproviderid()))
                    .andExpect(status().isOk())
                    .andReturn();
            System.out.println("After GET Again Result= "  + result.getResponse().getContentAsString());
            ServiceProviderV1 spAfterGet = objectMapper.readValue(result.getResponse().getContentAsByteArray(), ServiceProviderV1.class);

            boolean found = false;
            for(Iterator<LocationV1> locationV1Iterator = spAfterGet.getLocations().iterator(); locationV1Iterator.hasNext();){
                LocationV1 loc = locationV1Iterator.next();
                if(loc.getLocationid().equals(locationV1.getLocationid())){
                    found = true;
                    Assert.assertEquals(1, loc.getMyavailability().size());
                    break;
                }
            }
            Assert.assertTrue(found);

            result = mockMvc.perform(get("/serviceprovider/apt/{serviceProviderId}/{locationid}", spForAssert.getServiceproviderid(), locationV1.getLocationid())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            System.out.println("After generation of appointments Again Result= "  + result.getResponse().getContentAsString());

            List<AppointmentV2> myAppointments = objectMapper.readValue(result.getResponse().getContentAsByteArray(), objectMapper.getTypeFactory().constructCollectionType(List.class, AppointmentV2.class));
            Assert.assertEquals(10, myAppointments.size());

        }finally {
            IntegrationTestUtil.deleteSPAndItsLocations(mockMvc, spForAssert);
        }

    }


    @Test
    @Ignore // To run this test, change timeout for otp cache from 10 mins to 1min
    public void testOtpForFirstTimeSP() throws Exception{

        String invalidphoneNumber = "99860326";
        HandShakeV1 shakeV1 = new HandShakeV1();
        shakeV1.setPhoneNumber(invalidphoneNumber);
        MvcResult resultCust = mockMvc.perform(post("/serviceprovider/genotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();


        String phoneNumber = "9986032697";
        shakeV1 = new HandShakeV1();
        shakeV1.setPhoneNumber(phoneNumber);
        resultCust = mockMvc.perform(post("/serviceprovider/genotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String originalotp = resultCust.getResponse().getContentAsString();
        System.out.println(originalotp);

        resultCust = mockMvc.perform(post("/serviceprovider/verifyotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String verifiedOtp = resultCust.getResponse().getContentAsString();
        Assert.assertEquals(originalotp, verifiedOtp);

        System.out.println("Sleeping");
        Thread.sleep(120000);
        System.out.println("Sleeping done");

        resultCust = mockMvc.perform(post("/serviceprovider/verifyotp")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(shakeV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isExpectationFailed())
                .andReturn();
        verifiedOtp = resultCust.getResponse().getContentAsString();
        Assert.assertNotEquals(originalotp, verifiedOtp);

    }

    public void testDeleteAvailability() throws Exception{

    }

    public void testDeleteAvailabilityWithForceFieldSet() throws Exception{

    }

    public void testUpdateAvailability() throws Exception{

    }


}
