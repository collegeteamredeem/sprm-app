package com.cps.sprm.controllers;

import com.cps.sprm.configuration.ApplicationContextConfig;
import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.util.DTOUtil;
import com.cps.sprm.util.IntegrationTestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by sumeshs on 12/28/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(classes = ApplicationContextConfig.class)
@WebAppConfiguration
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class ITLocationControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private RestTemplate restTemplate;

    private LocationV1 locationForPost = DTOUtil.createLocationDTO( "Full Address", "LocationName", "Bangalore" , 18.450, -18.450,  "560016");;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        restTemplate = new RestTemplate();
        IntegrationTestUtil.setTimeout(restTemplate, 5000);
    }

    @After
    public void teardown() throws SQLException{
        restTemplate = null;
        this.mockMvc = null;
    }

    @Test
    public void getLocationByIdUsingMockMVC() throws Exception{
        LocationV1 persistedLocationV1 = createData(locationForPost);
        MvcResult result = mockMvc.perform(get("/location/{id}", persistedLocationV1.getLocationid()))
                .andExpect(status().isOk())
                .andReturn();

        LocationV1 locationV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), LocationV1.class);
        try {
            assertV1LocationObjects(locationV1ForAssert, persistedLocationV1);
        }finally {
            IntegrationTestUtil.deleteObject(locationV1ForAssert.getLocationid(), mockMvc, IntegrationTestUtil.LOCATION_URL_ENTITY_NAME);
        }
    }

    @Test
    public void updateLocationUsingMockMVCObject() throws Exception{
        LocationV1 persistedLocationV1 = createData(locationForPost);

        String newFullAddress = "New Full Address";

        persistedLocationV1.setFulladdress(newFullAddress);
        MvcResult result = mockMvc.perform(put("/location/update/{id}", persistedLocationV1.getLocationid())
                .content(IntegrationTestUtil.convertObjectToJsonBytes(persistedLocationV1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        LocationV1 locationV1ForAssert = objectMapper.readValue(result.getResponse().getContentAsByteArray(), LocationV1.class);
        try {
            assertV1LocationObjects(locationV1ForAssert, persistedLocationV1);
        }finally {
            IntegrationTestUtil.deleteObject(locationV1ForAssert.getLocationid(), mockMvc, IntegrationTestUtil.LOCATION_URL_ENTITY_NAME);
        }

    }

    @Test
    public void removeUsingMockMVCObject() throws Exception{
        LocationV1 persistedLocationV1 = createData(locationForPost);

        mockMvc.perform(delete("/location/purgeit/{id}", persistedLocationV1.getLocationid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get("/customer/{id}", persistedLocationV1.getLocationid()))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void createMultiLocationsAndGetAll() throws Exception{
        LocationV1 persistedLocationOne = createData(locationForPost);

        LocationV1 secondLocation = DTOUtil.createLocationDTO( "Full Address Mahadevapura", "LocationNameMaha", "Bangalore" , 19.450, -19.450,  "560048");
        LocationV1 persistedLocationTwo = createData(secondLocation);

        try {
            MvcResult result = mockMvc.perform(get("/location"))
                    .andExpect(status().isOk())
                    .andReturn();
            System.out.println("After GetALL Result= "  + result.getResponse().getContentAsString());

            LocationV1[] myLocations = objectMapper.readValue(result.getResponse().getContentAsByteArray(), LocationV1[].class);

            Assert.assertEquals(2, myLocations.length);
            int match = 0;
            for(int i = 0 ;i < myLocations.length;i++){
               if(persistedLocationOne.getLocationid().equals(myLocations[i].getLocationid())){
                   match ++;
                   assertV1LocationObjects(persistedLocationOne, myLocations[i]);
               }else if(persistedLocationTwo.getLocationid().equals(myLocations[i].getLocationid())){
                   match ++;
                   assertV1LocationObjects(persistedLocationTwo, myLocations[i]);
               }
            }
            Assert.assertEquals(2, match);

        }finally {
            IntegrationTestUtil.deleteObject(persistedLocationOne.getLocationid(), mockMvc, IntegrationTestUtil.LOCATION_URL_ENTITY_NAME);
            IntegrationTestUtil.deleteObject(persistedLocationTwo.getLocationid(), mockMvc, IntegrationTestUtil.LOCATION_URL_ENTITY_NAME);
        }
    }

    private LocationV1 createData(LocationV1 location) throws Exception {

        MvcResult result = mockMvc.perform(post("/location")
                .content(IntegrationTestUtil.convertObjectToJsonBytes(location))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        LocationV1 persistedLocation = objectMapper.readValue(result.getResponse().getContentAsByteArray(), LocationV1.class);
        return persistedLocation;
    }


    private void assertV1LocationObjects(LocationV1 obj1, LocationV1 obj2){
        Assert.assertEquals(obj1.getName(), obj2.getName());
        Assert.assertEquals(obj1.getPincode(), obj2.getPincode());
        Assert.assertEquals(obj1.getFulladdress(), obj2.getFulladdress());
        Assert.assertEquals(obj1.getCity(), obj2.getCity());
        Assert.assertEquals(obj1.getLat(), obj2.getLat(),0);
        Assert.assertEquals(obj1.getLon(), obj2.getLon(),0);
    }

}
