package com.cps.sprm.controllers;

import com.cps.sprm.sdk.dto.LocationV1;
import com.cps.sprm.service.ILocationService;
import com.cps.sprm.validator.LocationValidator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by sumeshs on 12/28/2016.
 */
@Controller("/location")
public class BaseLocationController {

    private static final Logger logger = Logger.getLogger(BaseLocationController.class);

    @Resource(name="locationServiceImpl")
    ILocationService locationService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new LocationValidator());
    }

    @RequestMapping(method = RequestMethod.GET, value="/location",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody  ResponseEntity<List<LocationV1>> getLocations()
    {
        if(logger.isDebugEnabled()){
            logger.debug("getLocations called ");
        }
        try {
            List<LocationV1> locationV1s = locationService.getAllLocations();
            if(null != locationV1s){
                return new ResponseEntity<List<LocationV1>>(locationV1s, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during getAllLocations ", e);
        }
        return new ResponseEntity<List<LocationV1>>(HttpStatus.NOT_FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value="/location/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody  ResponseEntity<LocationV1> getLocation(@PathVariable("id") Long id) {
        if(logger.isDebugEnabled()){
            logger.debug("getLocation by Id called " + id);
        }
        try {
            LocationV1 locationV1 = locationService.getLocationById(id);
            if(null != locationV1){
                return new ResponseEntity(locationV1, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during getLocationById ", e);
        }

        return new ResponseEntity("No Location found for ID " + id, HttpStatus.NOT_FOUND);
    }


    @RequestMapping(method = RequestMethod.POST,value="/location",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LocationV1> postLocation(
            @RequestBody LocationV1 locationDto) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("save Location called " + locationDto.toString());
        }
        try {
            LocationV1 locationV1 =  locationService.createLocation(locationDto);
            if(null != locationV1){
                return new ResponseEntity<LocationV1>(locationV1, HttpStatus.OK);
            }
        }catch (org.hibernate.exception.ConstraintViolationException e){
            logger.error("ConstraintViolation error when saving location ", e);
            return new ResponseEntity<LocationV1>(HttpStatus.CONFLICT);
        } catch (Exception e){
            logger.error("Exception during saveLocations ", e);
        }

        return new ResponseEntity<LocationV1>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/location/update/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LocationV1> updateLocation(@PathVariable("id") Long id, @RequestBody LocationV1 locationDto) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("update  Location called for id "  + id + "  " + locationDto.toString());
        }
        try {
            LocationV1 locationV1 = locationService.getLocationById(id);
            if (null != locationV1) {
                locationV1 = locationService.updateLocation(id, locationDto);
                if (locationV1 == null) {
                    return new ResponseEntity<LocationV1>(HttpStatus.BAD_REQUEST);
                }
                return new ResponseEntity<LocationV1>(locationV1, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during update of Locations ", e);
        }
        return new ResponseEntity("No Location found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.DELETE,value="/location/purgeit/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LocationV1> deleteLocation(@PathVariable("id") Long id) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("delete of Location called for id "  + id) ;
        }
        try {
            LocationV1 locationV1 = locationService.getLocationById(id);
            if(null != locationV1){
                locationService.deleteLocationById(locationV1.getLocationid());
                return new ResponseEntity<LocationV1>(locationV1, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during delete of Locations ", e);
        }

        return new ResponseEntity("No Location found for ID " + id, HttpStatus.NOT_FOUND);
    }
}
