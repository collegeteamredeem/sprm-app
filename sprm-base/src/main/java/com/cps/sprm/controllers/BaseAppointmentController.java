package com.cps.sprm.controllers;

import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.cps.sprm.service.IAppointmentService;
import com.cps.sprm.service.IServiceProviderService;
import com.cps.sprm.util.ValidityChecker;
import com.cps.sprm.validator.AppointmentValidator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

/**
 * Created by sumeshs on 12/29/2016.
 */
@Controller("/apt")
public class BaseAppointmentController {

    private static final Logger logger = Logger.getLogger(BaseAppointmentController.class);
    private Clock clock = Clock.systemUTC();

    @Resource(name="appointmentServiceImpl")
    IAppointmentService appointmentService;

    @Resource(name="serviceProviderServiceImpl")
    IServiceProviderService serviceProviderService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new AppointmentValidator());
    }

    @RequestMapping(method = RequestMethod.GET, value="/apt",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody  ResponseEntity<List<AppointmentV2>> getAllAppointments()
    {
        try {
            List<AppointmentV2> appointmentV1s = appointmentService.getAllAppointments();
            if(null != appointmentV1s){
                return new ResponseEntity<List<AppointmentV2>>(appointmentV1s, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during getAllAppointments ", e);
            e.printStackTrace();
        }
        return new ResponseEntity<List<AppointmentV2>>(HttpStatus.NOT_FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value="/apt/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody  ResponseEntity<AppointmentV2> getAppointment(@PathVariable("id") Long id) {

        try {
            AppointmentV2 appointmentById = appointmentService.getAppointmentById(id);
            if(null != appointmentById){
                return new ResponseEntity(appointmentById, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during getAppointment  By id ", e);
            e.printStackTrace();
        }

        return new ResponseEntity("No Appointment found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.GET,value="/apt/{serviceProviderId}/{locationid}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<AppointmentV2>> getAptBySPLocAndStatus(
                                                                              @PathVariable("serviceProviderId") Long spId,
                                                                              @PathVariable("locationid") Long locId,
                                                                              @RequestParam(value="status", defaultValue="FREE", required = false) String status,
                                                                              @RequestParam(value="startTime", required = true) String startTimeStr,
                                                                              @RequestParam(value="endTime", required = false) String endTimeStr,
                                                                              @RequestParam(value ="durationForTestOnly", defaultValue = "0", required = false) String duration) throws Exception
    {
        HttpStatus httpStatus= HttpStatus.BAD_REQUEST;

        if(startTimeStr == null || startTimeStr.isEmpty()){
            logger.error("StartTimeNot Specified, cannot proceed" + spId);
            httpStatus = HttpStatus.NOT_FOUND;
        }else {

            Long startTime = Long.parseLong(startTimeStr);
            Long endTime = 0L;
            if(endTimeStr != null && !endTimeStr.trim().isEmpty()){
                endTime = Long.parseLong(endTimeStr);
            }else {
                LocalTime midnight = LocalTime.MIDNIGHT;
                Instant startTimeInstant = Instant.ofEpochMilli(startTime);
                LocalDateTime localDateTime = LocalDateTime.ofInstant(startTimeInstant, ZoneId.of("UTC"));

                LocalDateTime todayMidnight = LocalDateTime.of(localDateTime.toLocalDate(), midnight);
                LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);
                endTime = tomorrowMidnight.toEpochSecond(ZoneOffset.UTC) * 1000;
            }
            System.out.println("The startTime is detected as ## " + startTime);
            System.out.println("The endTime is detected as ## " + endTime);

            if(endTime >= startTime) {

                System.out.println("The startTime is detected as " + startTime);
                System.out.println("The endTime is detected as " + endTime);

                AvailabilityV1 availabilityDto = new AvailabilityV1();
                availabilityDto.setDateTimeFrom(startTime);
                availabilityDto.setDateTimeTo(endTime);

                Clock clockToUse = clock;
                Long durationLong = Long.parseLong(duration);
                if (durationLong > 0) {
                    Duration durationOffset = Duration.ofDays(durationLong);
                    logger.debug("The offset requested " + durationOffset.toString());
                    clockToUse = Clock.offset(clock, durationOffset);
                }

                AppointmentStatus appointmentStatus = AppointmentStatus.valueOf(status);
                try {
                    ServiceProviderV1 serviceProviderV1 = serviceProviderService.getServiceProviderById(spId);
                    if (null != serviceProviderV1) {

                        if (!ValidityChecker.isMembershipStillValid(serviceProviderV1.getValidity(), clockToUse)) {
                            logger.error("The validity has expired, cannot get appointments");
                            httpStatus = HttpStatus.PAYMENT_REQUIRED;
                        } else {

                            httpStatus = HttpStatus.OK;
                            List<AppointmentV2> appointmentV2s = appointmentService.getAppointmentsBySPLocAndStatus(spId, locId, availabilityDto, appointmentStatus);
                            return new ResponseEntity<List<AppointmentV2>>(appointmentV2s, httpStatus);
                        }
                    } else {
                        logger.error("Cannot find SP with id for cancelling apts  by location" + spId);
                        httpStatus = HttpStatus.NOT_FOUND;
                    }
                } catch (Exception e) {
                    httpStatus = HttpStatus.BAD_REQUEST;
                    e.printStackTrace();
                }
            }else {
                logger.error("Invalid start and End Time detected " + startTime + "  " + endTime);
                System.out.println("Invalid start and End Time detected " + startTime + "  " + endTime);
            }
        }

        return new ResponseEntity(httpStatus);
    }

    @RequestMapping(method = RequestMethod.GET,value="/aptall/{serviceProviderId}/{locationid}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<AppointmentV2>> getAllAptBySPLoc(
            @PathVariable("serviceProviderId") Long spId,
            @PathVariable("locationid") Long locId,
            @RequestParam(value="startTime", required = true) String startTimeStr,
            @RequestParam(value="endTime", required = false) String endTimeStr,
            @RequestParam(value ="durationForTestOnly", defaultValue = "0", required = false) String duration) throws Exception
    {
        HttpStatus httpStatus= HttpStatus.BAD_REQUEST;

        if(startTimeStr == null || startTimeStr.isEmpty()){
            logger.error("StartTimeNot Specified, cannot proceed" + spId);
            httpStatus = HttpStatus.NOT_FOUND;
        }else {

            Long startTime = Long.parseLong(startTimeStr);
            Long endTime = 0L;
            if(endTimeStr != null && !endTimeStr.trim().isEmpty()){
                endTime = Long.parseLong(endTimeStr);
            }else {
                LocalTime midnight = LocalTime.MIDNIGHT;
                Instant startTimeInstant = Instant.ofEpochMilli(startTime);
                LocalDateTime localDateTime = LocalDateTime.ofInstant(startTimeInstant, ZoneId.of("UTC"));

                LocalDateTime todayMidnight = LocalDateTime.of(localDateTime.toLocalDate(), midnight);
                LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);
                endTime = tomorrowMidnight.toEpochSecond(ZoneOffset.UTC) * 1000;
            }

            if(endTime >= startTime) {

                AvailabilityV1 availabilityDto = new AvailabilityV1();
                availabilityDto.setDateTimeFrom(startTime);
                availabilityDto.setDateTimeTo(endTime);

                Clock clockToUse = clock;
                Long durationLong = Long.parseLong(duration);
                if (durationLong > 0) {
                    Duration durationOffset = Duration.ofDays(durationLong);
                    logger.debug("The offset requested " + durationOffset.toString());
                    clockToUse = Clock.offset(clock, durationOffset);
                }

                try {
                    ServiceProviderV1 serviceProviderV1 = serviceProviderService.getServiceProviderById(spId);
                    if (null != serviceProviderV1) {

                        if (!ValidityChecker.isMembershipStillValid(serviceProviderV1.getValidity(), clockToUse)) {
                            logger.error("The validity has expired, cannot get appointments");
                            httpStatus = HttpStatus.PAYMENT_REQUIRED;
                        } else {

                            httpStatus = HttpStatus.OK;
                            List<AppointmentV2> appointmentV2s = appointmentService.getAllAppointmentsBySPLoc(spId, locId, availabilityDto);
                            return new ResponseEntity<List<AppointmentV2>>(appointmentV2s, httpStatus);
                        }
                    } else {
                        logger.error("Cannot find SP with id for cancelling apts  by location" + spId);
                        httpStatus = HttpStatus.NOT_FOUND;
                    }
                } catch (Exception e) {
                    httpStatus = HttpStatus.BAD_REQUEST;
                    e.printStackTrace();
                }
            }else {
                logger.error("Invalid start and End Time detected " + startTime + "  " + endTime);
                System.out.println("Invalid start and End Time detected " + startTime + "  " + endTime);
            }
        }

        return new ResponseEntity(httpStatus);
    }

    @RequestMapping(method = RequestMethod.POST,value="/apt",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<AppointmentV1> createAppointments(
            @RequestBody AppointmentV1 appointmentDto) throws Exception
    {
        try {
            AppointmentV1 appointmentV1 =  appointmentService.createAppointment(appointmentDto);
            if(null != appointmentV1){
                return new ResponseEntity<AppointmentV1>(appointmentV1, HttpStatus.OK);
            }
        }catch (org.hibernate.exception.ConstraintViolationException e){
            return new ResponseEntity<AppointmentV1>(HttpStatus.CONFLICT);
        } catch (Exception e){
            logger.error("Exception during save appointment  ", e);
            e.printStackTrace();
        }

        return new ResponseEntity<AppointmentV1>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(method = RequestMethod.PUT,value="/apt/update/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<AppointmentV1> updateAppointment(@PathVariable("id") Long id, @RequestBody AppointmentV1 appointmentDto) throws Exception
    {
        try {
            AppointmentV1 appointmentV1 = appointmentService.getAppointmentV1ById(id);
            if (null != appointmentV1) {
                appointmentV1 = appointmentService.updateAppointment(id, appointmentDto);
                if (appointmentV1 == null) {
                    return new ResponseEntity<AppointmentV1>(HttpStatus.BAD_REQUEST);
                }
                return new ResponseEntity<AppointmentV1>(appointmentV1, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during update Appointment  ", e);
            e.printStackTrace();
        }
        return new ResponseEntity("No Appointment found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.DELETE,value="/apt/purgeit/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<AppointmentV1> deleteAppointment(@PathVariable("id") Long id) throws Exception
    {
        try {
            AppointmentV1 appointmentV1 = appointmentService.getAppointmentV1ById(id);
            if(null != appointmentV1){
                if(appointmentV1.getAppointmentStatus().equals(AppointmentStatus.BOOKED)){
                    logger.error("Cannot delete appointment with booked status, cancel it first " + id);
                    return new ResponseEntity<AppointmentV1>(HttpStatus.BAD_REQUEST);
                }
                appointmentService.deleteAppointmentById(appointmentV1.getAppointmentid());
                return new ResponseEntity<AppointmentV1>(appointmentV1, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during delete Appointment  By id ", e);
            e.printStackTrace();
        }

        return new ResponseEntity("No Appointment found for ID " + id, HttpStatus.NOT_FOUND);
    }

}
