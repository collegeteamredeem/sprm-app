package com.cps.sprm.controllers;

import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.HandShakeV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.service.IAppointmentService;
import com.cps.sprm.service.IOtpService;
import com.cps.sprm.service.IServiceProviderService;
import com.cps.sprm.util.ValidityChecker;
import com.cps.sprm.validator.ServiceProviderValidator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import javax.annotation.Resource;
import java.time.Clock;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller("/serviceprovider")
public class BaseSPMController
{

	private static final Logger logger = Logger.getLogger(BaseSPMController.class);

	private Clock clock = Clock.systemUTC();

	@Resource(name="serviceProviderServiceImpl")
	IServiceProviderService serviceProvider;

	@Resource(name="appointmentServiceImpl")
	IAppointmentService appointmentService;

	@Resource(name="otpServiceImpl")
	IOtpService otpService;


	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new ServiceProviderValidator());
	}

	@RequestMapping(method = RequestMethod.GET, value="/serviceprovider",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public 	@ResponseBody ResponseEntity<List<ServiceProviderV1>> getServiceProvider()
	{
		List<ServiceProviderV1> spList = serviceProvider.getAllServiceProviders();
		if(null == spList) {
			logger.debug("No Service Providers found, return empty list");
			return new ResponseEntity<List<ServiceProviderV1>>(HttpStatus.NOT_FOUND);
		}
		logger.debug("Returning serviceproviders " + spList.size());
		return new ResponseEntity<List<ServiceProviderV1>>(spList, HttpStatus.OK);
	}

	private String generateSPSMSId(ServiceProviderV1 serviceProviderV1){
		Random random = new Random(System.currentTimeMillis());
		int number = 10000 + random.nextInt(89900);
		String spSMSId = serviceProviderV1.getName().substring(0,2).toUpperCase().concat(Integer.toString(number));
		return spSMSId;
	}


	@RequestMapping(method = RequestMethod.POST, value="/serviceprovider",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ServiceProviderV1> postServiceProvider(
			@RequestBody ServiceProviderV1 serviceProviderDto) throws Exception
	{
		try {
			serviceProviderDto.setProvidersmsid(generateSPSMSId(serviceProviderDto));
			ServiceProviderV1 serviceProviderObj = serviceProvider.createServiceProvider(serviceProviderDto);
			if(null != serviceProviderObj){
				return new ResponseEntity<ServiceProviderV1>(serviceProviderObj, HttpStatus.OK);
			}
		}catch (org.hibernate.exception.ConstraintViolationException e){
			logger.error("ConstraintViolation exception when persisting SP " + serviceProviderDto.toString());
			return new ResponseEntity<ServiceProviderV1>(HttpStatus.CONFLICT);
		} catch (Exception e){
			logger.error("Error creating the SP " + serviceProviderDto.toString());
			e.printStackTrace();
		}

		return new ResponseEntity<ServiceProviderV1>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET, value="/serviceprovider/{serviceProviderId}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public 	@ResponseBody  ResponseEntity<ServiceProviderV1> getServiceProviderById(@PathVariable Long serviceProviderId)
	{
		ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(serviceProviderId);
		if (serviceProviderV1 == null)
		{
			logger.error("Cannot find SP with id for get " + serviceProviderId);
			return new ResponseEntity<ServiceProviderV1>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ServiceProviderV1>(serviceProviderV1, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value="/serviceprovider/{serviceProviderId}",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ServiceProviderV1> updateServiceProvider(
			@RequestBody ServiceProviderV1 serviceProviderUpdateDto,
			@PathVariable Long serviceProviderId) throws Exception
	{
		ServiceProviderV1 serviceProviderObj = serviceProvider.updateServiceProvider(serviceProviderUpdateDto,
				serviceProviderId);
		if (serviceProviderObj == null)
		{
			logger.error("Cannot find SP with id for update " + serviceProviderId);
			return new ResponseEntity<ServiceProviderV1>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ServiceProviderV1>(serviceProviderObj, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PATCH, value="/serviceprovider/{serviceProviderId}/validity/{validMonth}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ServiceProviderV1> updateServiceProviderValidity(
			@PathVariable Long serviceProviderId, @PathVariable Long validMonth) throws Exception
	{

		if(!isValidMonthValuePassed(validMonth)){
			logger.error("Invalid value for validity  " + serviceProviderId + "  " + validMonth);
			return new ResponseEntity<ServiceProviderV1>(HttpStatus.BAD_REQUEST);
		}

		ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(serviceProviderId);
		if (serviceProviderV1 == null)
		{
			logger.error("Cannot find SP with id for get " + serviceProviderId);
			return new ResponseEntity<ServiceProviderV1>(HttpStatus.NOT_FOUND);
		}

		ServiceProviderV1 serviceProviderObj = serviceProvider.updateServiceProviderValidity(serviceProviderId, validMonth);
		if (serviceProviderObj == null)
		{
			logger.error("Cannot find SP with id for updating validity" + serviceProviderId);
			return new ResponseEntity<ServiceProviderV1>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ServiceProviderV1>(serviceProviderObj, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.DELETE,value="/serviceprovider/purgeit/{id}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ServiceProviderV1> deleteServiceProvider(@PathVariable("id") Long id) throws Exception
	{
		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(id);
			if(null != serviceProviderV1){
				serviceProvider.deleteServiceProviderById(serviceProviderV1.getServiceproviderid());
				return new ResponseEntity<ServiceProviderV1>(serviceProviderV1, HttpStatus.OK);
			}else {
				logger.error("Cannot find SP with id delete" + id);
			}
		}catch (Exception e){
			logger.error("Error deleting the SP ");
			e.printStackTrace();
		}
		return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
	}


	@RequestMapping(method = RequestMethod.PUT,value="/serviceprovider/availability/{serviceProviderId}/{locationid}",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ServiceProviderV1> addAvailability(@PathVariable("serviceProviderId") Long spId,
																		   @PathVariable("locationid")	Long locId,
																		   @RequestBody AvailabilityV1 availabilityDto) throws Exception
	{
		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(spId);
			if(null != serviceProviderV1){
				ServiceProviderV1 serviceProviderV1AddAvailability = serviceProvider.addAvailability(spId, locId, availabilityDto);
				return new ResponseEntity<ServiceProviderV1>(serviceProviderV1AddAvailability, HttpStatus.OK);
			}else {
				logger.error("Cannot find SP with id for availability " + spId);
			}
		}catch (Exception e){
			logger.error("Error adding availability for SP " + spId);
			e.printStackTrace();
		}

		return new ResponseEntity("No ServiceProvider found for ID " + spId, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.DELETE,value="/serviceprovider/availability/{serviceProviderId}/{locationid}",
			consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ServiceProviderV1> deleteAvialability(@PathVariable("serviceProviderId") Long spId,
																			  @PathVariable("locationid")	Long locId,
																			  @RequestBody AvailabilityV1 availabilityDto,
																			  @RequestParam(value="force", defaultValue="false") String forceStr) throws Exception
	{
		boolean force= Boolean.parseBoolean(forceStr);
		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(spId);
			if(null != serviceProviderV1){
				ServiceProviderV1 serviceProviderV1Del = serviceProvider.deleteAvailability(spId, locId, availabilityDto, force);
				return new ResponseEntity<ServiceProviderV1>(serviceProviderV1Del, HttpStatus.OK);
			}else {
				logger.error("Cannot find SP with id for delete availability " + spId);
			}
		}catch (Exception e){
			logger.error("Error deleting availability for SP " + spId);
			e.printStackTrace();
		}

		return new ResponseEntity("No ServiceProvider found for ID " + spId, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET,value="/serviceprovider/apt/{serviceProviderId}/{locationid}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<AppointmentV2>> genApts(@PathVariable("serviceProviderId") Long spId,
																	 @PathVariable("locationid")	Long locId,
																	 @RequestParam(value = "durationForTestOnly", defaultValue = "0") String duration) throws Exception
	{
		Clock clockToUse = clock;
		Long durationLong = Long.parseLong(duration);
		if(durationLong > 0){
			Duration durationOffset = Duration.ofDays(durationLong);
			clockToUse = Clock.offset(clock, durationOffset);
		}

		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(spId);
			if(null != serviceProviderV1){
				// Check Validity
				if(!ValidityChecker.isMembershipStillValid(serviceProviderV1.getValidity(), clockToUse)){
					logger.error("The validity has expired, cannot generate appointments");
					List<AppointmentV2> emptyAppointments = new ArrayList<AppointmentV2>();
					return new ResponseEntity<List<AppointmentV2>>(emptyAppointments, HttpStatus.PAYMENT_REQUIRED);
				}

				List<AppointmentV2> serviceProviderV1Appointments = serviceProvider.genAppointmentsForThisLocation(spId, locId);
				return new ResponseEntity<List<AppointmentV2>>(serviceProviderV1Appointments, HttpStatus.OK);
			}else {
				logger.error("Cannot find SP with id for generating apts " + spId);
			}
		}catch (Exception e){
			logger.error("Error generating apts for SP " + spId);
			e.printStackTrace();
		}

		return new ResponseEntity("No ServiceProvider found for ID " + spId, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.POST,value="/serviceprovider/apt/cancelbyloc/{serviceProviderId}/{locationid}",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> cancelAptByLocId(@RequestBody AvailabilityV1 availabilityDto,
																			  @PathVariable("serviceProviderId") Long spId,
																			  @PathVariable("locationid") Long locId,
																			  @RequestParam(value="force", defaultValue="false") String forceStr) throws Exception
	{
		HttpStatus status;
		String message = "Error in cancelling appointment ";

		boolean force= Boolean.parseBoolean(forceStr);
		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(spId);
			if(null != serviceProviderV1){
				status = HttpStatus.OK;
				appointmentService.cancelAppointmentsByLocSP(spId, locId, availabilityDto, force);
				return new ResponseEntity("Appointments Cancelled Successfull " + spId, status);
			}else {
				logger.error("Cannot find SP with id for cancelling apts  by location" + spId);
				status = HttpStatus.NOT_FOUND;
			}
		}catch (Exception e){
			status = HttpStatus.BAD_REQUEST;
			e.printStackTrace();
		}

		return new ResponseEntity(message + spId, status);
	}

	@RequestMapping(method = RequestMethod.POST,value="/serviceprovider/apt/cancelbyallloc/{serviceProviderId}",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> cancelAptByAllLoc(@RequestBody AvailabilityV1 availabilityDto,
																			   @PathVariable("serviceProviderId") Long spId,
																			   @RequestParam(value="force", defaultValue="false") String forceStr) throws Exception
	{
		HttpStatus status;
		String message = "Error in cancelling appointment ";
		boolean force= Boolean.parseBoolean(forceStr);
		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(spId);
			if(null != serviceProviderV1){
				status = HttpStatus.OK;
				appointmentService.cancelAppointmentsByAllLocSP(spId, availabilityDto, force);
				return new ResponseEntity("Appointments Cancelled Successfull " + spId, status);
			}else {
				logger.error("Cannot find SP with id for cancelling apts  by all location" + spId);
				status = HttpStatus.NOT_FOUND;
			}
		}catch (Exception e){
			status = HttpStatus.BAD_REQUEST;
			e.printStackTrace();
		}

		return new ResponseEntity(message + spId, status);
	}

	@RequestMapping(method = RequestMethod.GET,value="/serviceprovider/apt/cancelbyid/{serviceProviderId}/{aptId}",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> cancelAptByIndividualAptId(@PathVariable("serviceProviderId") Long spId,
																						@PathVariable("aptId") Long aptId,
																						@RequestParam(value="force", defaultValue="false") String forceStr) throws Exception
	{
		HttpStatus status;
		String message = "Error in cancelling appointment ";
		boolean force= Boolean.parseBoolean(forceStr);
		try {
			ServiceProviderV1 serviceProviderV1 = serviceProvider.getServiceProviderById(spId);
			if(null != serviceProviderV1){
				status = HttpStatus.OK;
				appointmentService.cancelAppointmentByIdOfSP(spId, aptId, force);
				return new ResponseEntity("Appointments Cancelled Successfull " + spId, status);
			}else {
				logger.error("Cannot find SP with id for cancelling apts  by id" + spId);
				status = HttpStatus.NOT_FOUND;
			}
		}catch (Exception e){
			status = HttpStatus.BAD_REQUEST;
			e.printStackTrace();
		}

		return new ResponseEntity(message + spId, status);
	}

	@RequestMapping(method = RequestMethod.POST,value="/serviceprovider/genotp",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> genOtpForServiceProvider(
			@RequestBody HandShakeV1 handShakeV1) throws Exception
	{
		try {
			ServiceProviderV1 serviceProviderV1 =  serviceProvider.getServiceProviderByPhoneNumber(handShakeV1.getPhoneNumber());
			if(null != serviceProviderV1){
				return new ResponseEntity<String>("ServiceProvider with phone number already reported", HttpStatus.ALREADY_REPORTED);
			}
			otpService.clearOtp(handShakeV1.getPhoneNumber());
			String otp = otpService.genOtp(handShakeV1.getPhoneNumber(), true);
			if(null != otp) {
				return new ResponseEntity<String>(otp, HttpStatus.OK);
			}else {
				return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e){
			logger.error("Error querying the serviceprovider");
			e.printStackTrace();
		}

		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.POST,value="/serviceprovider/verifyotp",
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> verifyOtpServiceProvider(
			@RequestBody HandShakeV1 handShakeV1) throws Exception
	{
		try {
			String otp = otpService.genOtp(handShakeV1.getPhoneNumber(), false);
			if(null != otp) {
				return new ResponseEntity<String>(otp, HttpStatus.OK);
			}else {
				return new ResponseEntity<String>(HttpStatus.EXPECTATION_FAILED);
			}
		} catch (Exception e){
			logger.error("Error querying the serviceprovider");
			e.printStackTrace();
		}

		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}

	private boolean isValidMonthValuePassed(Long monthValue){

		if(monthValue == 1 || monthValue == 6 || monthValue == 12){
			return true;
		}else {
			return false;
		}
	}
}
