package com.cps.sprm.controllers;


import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sdk.dto.HandShakeV1;
import com.cps.sprm.service.IAppointmentService;
import com.cps.sprm.service.ICustomerService;
import com.cps.sprm.service.IOtpService;
import com.cps.sprm.validator.CustomerValidator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by sumeshs on 11/8/2016.
 */
@Controller("/customer")
public class BaseCustomerController {

    private static final Logger logger = Logger.getLogger(BaseCustomerController.class);

    @Resource(name="customerServiceImpl")
    ICustomerService customerService;

    @Resource(name="appointmentServiceImpl")
    IAppointmentService appointmentService;

    @Resource(name="otpServiceImpl")
    IOtpService otpService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new CustomerValidator());
    }


    @RequestMapping(method = RequestMethod.POST,value="/customer/genotp",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<String> genOtpForCustomer(
            @RequestBody HandShakeV1 handShakeV1) throws Exception
    {
        try {
            CustomerV1 customerV1 =  customerService.getCustomerByPhoneNumber(handShakeV1.getPhoneNumber());
            if(null != customerV1){
                return new ResponseEntity<String>("Customer with phone number already reported", HttpStatus.ALREADY_REPORTED);
            }
            otpService.clearOtp(handShakeV1.getPhoneNumber());
            String otp = otpService.genOtp(handShakeV1.getPhoneNumber(), true);
            if(null != otp) {
                return new ResponseEntity<String>(otp, HttpStatus.OK);
            }else {
                return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            logger.error("Error querying the customer");
            e.printStackTrace();
        }

        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.POST,value="/customer/verifyotp",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<String> verifyOtpForCustomer(
            @RequestBody HandShakeV1 handShakeV1) throws Exception
    {
        try {
            String otp = otpService.genOtp(handShakeV1.getPhoneNumber(), false);
            if(null != otp) {
                return new ResponseEntity<String>(otp, HttpStatus.OK);
            }else {
                return new ResponseEntity<String>(HttpStatus.EXPECTATION_FAILED);
            }
        } catch (Exception e){
            logger.error("Error querying the customer");
            e.printStackTrace();
        }

        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(method = RequestMethod.GET, value="/customer",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody
    ResponseEntity<List<CustomerV1>> getCustomers()
    {
        try {
            List<CustomerV1> customerEntityList = customerService.getAllCustomers();
            if(null != customerEntityList){
                return new ResponseEntity<List<CustomerV1>>(customerEntityList, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error trying to getCustomers");
            e.printStackTrace();
        }
        return new ResponseEntity<List<CustomerV1>>(HttpStatus.NOT_FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value="/customer/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody
    ResponseEntity<CustomerV1> getCustomer(@PathVariable("id") Long id) {

        try {
            CustomerV1 customer = customerService.getCustomerById(id);
            if(null != customer){
                return new ResponseEntity(customer, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error trying to getCustomerBy Id " + id);
            e.printStackTrace();
        }

        return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
    }


    @RequestMapping(method = RequestMethod.POST,value="/customer",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<CustomerV1> postCustomer(
            @RequestBody CustomerV1 customerDto) throws Exception
    {
        try {
            CustomerV1 customerV1 =  customerService.createCustomer(customerDto);
            if(null != customerV1){
                return new ResponseEntity<CustomerV1>(customerV1, HttpStatus.OK);
            }
        }catch (org.hibernate.exception.ConstraintViolationException e){
            return new ResponseEntity<CustomerV1>(HttpStatus.CONFLICT);
        } catch (Exception e){
            logger.error("Error creating the customer");
            e.printStackTrace();
        }

        return new ResponseEntity<CustomerV1>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/customer/update/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<CustomerV1> updateCustomer(@PathVariable("id") Long id, @RequestBody CustomerV1 customerDto) throws Exception
    {
        try {
            CustomerV1 customer = customerService.getCustomerById(id);
            if (null != customer) {
                customer = customerService.updateCustomer(id, customerDto);
                if (customer == null) {
                    return new ResponseEntity<CustomerV1>(HttpStatus.BAD_REQUEST);
                }
                return new ResponseEntity<CustomerV1>(customer, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error updating the customer " + id );
            e.printStackTrace();
        }
        return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.DELETE,value="/customer/purgeit/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<CustomerV1> deleteCustomer(@PathVariable("id") Long id) throws Exception
    {
        try {
            CustomerV1 customer = customerService.getCustomerById(id);
            if(null != customer){
                customerService.deleteCustomerById(customer.getCustomerid());
                return new ResponseEntity<CustomerV1>(customer, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error deleting the customer " + id);
            e.printStackTrace();
        }

        return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/customer/favourites/{custid}/sp/{spid}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<CustomerV1> addToFavourites(@PathVariable("custid") Long custId, @PathVariable("spid") Long spId) throws Exception
    {
        try {
            CustomerV1 customer = customerService.getCustomerById(custId);
            if(null != customer){
                customerService.addToCustomerFavourites(custId, spId);
                return new ResponseEntity<CustomerV1>(customer, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error adding to favourites " + custId + "  " + spId);
            e.printStackTrace();
        }

        return new ResponseEntity("No Customer found for ID " + custId, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.DELETE,value="/customer/favourites/{custid}/sp/{spid}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<CustomerV1> delFromFavourites(@PathVariable("custid") Long custId, @PathVariable("spid") Long spId) throws Exception
    {
        try {
            CustomerV1 customer = customerService.getCustomerById(custId);
            if(null != customer){
                customerService.deleteFromCustomerFavourites(custId, spId);
                return new ResponseEntity<CustomerV1>(customer, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error deleting from favourites " + custId + "  " + spId);
            e.printStackTrace();
        }
        return new ResponseEntity("No Customer found for ID " + custId, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/customer/bookapt/{id}/{aptid}",
           produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<AppointmentV1> bookAppointment(@PathVariable("id") Long id, @PathVariable("aptid") Long aptid) throws Exception
    {
        HttpStatus status = HttpStatus.NOT_FOUND;
        String message = "Error in booking appointment ";
        try {
            CustomerV1 customer = customerService.getCustomerById(id);
            if (null != customer) {
                AppointmentV1 appointmentV1 = appointmentService.bookAppointmentByIdOfCustomer(id, aptid);
                status = HttpStatus.OK;
                return new ResponseEntity<AppointmentV1>(appointmentV1, status);
            }
        }catch (Exception e){
            logger.error("Error booking appointment " + id + "  " + aptid);
            status = HttpStatus.BAD_REQUEST;
            e.printStackTrace();
        }
        return new ResponseEntity(message , status);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/customer/cancelmyapt/{id}/{aptid}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<AppointmentV1> cancelAppointment(@PathVariable("id") Long id, @PathVariable("aptid") Long aptid) throws Exception
    {
        HttpStatus status = HttpStatus.NOT_FOUND;
        String message = "Error in booking appointment ";
        try {
            CustomerV1 customer = customerService.getCustomerById(id);
            if (null != customer) {
                AppointmentV1 appointmentV1 = appointmentService.cancelAppointmentByIdOfCustomer(id, aptid);
                status = HttpStatus.OK;
                return new ResponseEntity<AppointmentV1>(appointmentV1, status);
            }
        }catch (Exception e){
            logger.error("Error in cancelling appointment " + id + "  " + aptid);
            status = HttpStatus.BAD_REQUEST;
            e.printStackTrace();
        }
        return new ResponseEntity(message, status);
    }
}
