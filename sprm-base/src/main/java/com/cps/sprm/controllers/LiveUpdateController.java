package com.cps.sprm.controllers;

import com.cps.sprm.sdk.dto.LiveUpdateV2;
import com.cps.sprm.service.ILiveUpdateService;
import com.cps.sprm.validator.LiveUpdateValidator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;


import javax.annotation.Resource;
import java.util.List;

/**
 * Created by sumeshs on 1/26/2017.
 */
@Controller("/live")
public class LiveUpdateController {

    private static final Logger logger = Logger.getLogger(LiveUpdateController.class);

    @Resource(name="liveUpdateServiceImpl")
    ILiveUpdateService liveUpdateService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new LiveUpdateValidator());
    }

    @RequestMapping(method = RequestMethod.GET, value="/live",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody
    ResponseEntity<List<LiveUpdateV2>> getAllLiveUpdates()
    {
        try {
            List<LiveUpdateV2> liveUpdateV2s = liveUpdateService.getLiveUpdates();
            if(null != liveUpdateV2s){
                return new ResponseEntity<List<LiveUpdateV2>>(liveUpdateV2s, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during getAllLiveUpdates ", e);
            e.printStackTrace();
        }
        return new ResponseEntity<List<LiveUpdateV2>>(HttpStatus.NOT_FOUND);

    }

    @RequestMapping(method = RequestMethod.GET, value="/live/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public 	@ResponseBody  ResponseEntity<LiveUpdateV2> getLiveUpdateById(@PathVariable("id") Long id) {

        try {
            LiveUpdateV2 liveUpdateV2ById = liveUpdateService.getLiveUpdateById(id);
            if(null != liveUpdateV2ById){
                return new ResponseEntity(liveUpdateV2ById, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during getLiveUpdate  By id ", e);
            e.printStackTrace();
        }

        return new ResponseEntity("No LiveUpdate found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.POST,value="/live/{serviceProviderId}/{locationid}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LiveUpdateV2> createLiveUpdate(@RequestBody LiveUpdateV2 liveUpdateDto,
                                                                                    @PathVariable("serviceProviderId") Long spId,
                                                                                    @PathVariable("locationid") Long locId) throws Exception
    {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            LiveUpdateV2 liveUpdateV2 = liveUpdateService.createLiveUpdate(liveUpdateDto, spId, locId);
            return new ResponseEntity<LiveUpdateV2>(liveUpdateV2, httpStatus);
        }catch (Exception e){
            httpStatus = HttpStatus.BAD_REQUEST;
            e.printStackTrace();
        }
        return new ResponseEntity(httpStatus);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/live/update/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LiveUpdateV2> updateLiveUpdate(@PathVariable("id") Long id, @RequestBody LiveUpdateV2 liveUpdateDto) throws Exception
    {
        try {
            LiveUpdateV2 liveUpdateV2 = liveUpdateService.getLiveUpdateById(id);
            if (null != liveUpdateV2) {
                liveUpdateV2 = liveUpdateService.updateLiveUpdate(id, liveUpdateDto);
                if (liveUpdateV2 == null) {
                    return new ResponseEntity<LiveUpdateV2>(HttpStatus.BAD_REQUEST);
                }
                return new ResponseEntity<LiveUpdateV2>(liveUpdateV2, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during update LiveUpdate  ", e);
            e.printStackTrace();
        }
        return new ResponseEntity("No LiveUpdate found for ID " + id, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.PUT,value="/live/end/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LiveUpdateV2> endLiveUpdate(@PathVariable("id") Long id, @RequestBody LiveUpdateV2 liveUpdateDto) throws Exception
    {
        try {
            LiveUpdateV2 liveUpdateV2 = liveUpdateService.getLiveUpdateById(id);
            if (null != liveUpdateV2) {
                liveUpdateV2 = liveUpdateService.endLiveUpdate(id, liveUpdateDto);
                if (liveUpdateV2 == null) {
                    return new ResponseEntity<LiveUpdateV2>(HttpStatus.BAD_REQUEST);
                }
                return new ResponseEntity<LiveUpdateV2>(liveUpdateV2, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during update LiveUpdate  ", e);
            e.printStackTrace();
        }
        return new ResponseEntity("No LiveUpdate found for ID " + id, HttpStatus.NOT_FOUND);
    }


    @RequestMapping(method = RequestMethod.DELETE,value="/live/purgeit/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<LiveUpdateV2> deleteLiveUpdate(@PathVariable("id") Long id) throws Exception
    {
        try {
            LiveUpdateV2 liveUpdateV2 = liveUpdateService.getLiveUpdateById(id);
            if(null != liveUpdateV2){
                liveUpdateService.deleteLiveUpdate(liveUpdateV2);
                return new ResponseEntity<LiveUpdateV2>(liveUpdateV2, HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Exception during delete LiveUpdate  By id ", e);
            e.printStackTrace();
        }

        return new ResponseEntity("No LiveUpdate found for ID " + id, HttpStatus.NOT_FOUND);
    }

}
