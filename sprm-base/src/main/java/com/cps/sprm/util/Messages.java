package com.cps.sprm.util;

/**
 * Created by sumeshs on 2/21/2017.
 */
public class Messages {

    public static String EMAIL_SUBJECT_WELCOME = "WelCome";
    public static String EMAIL_SUBJECT_CANCEL = "Cancelled";
    public static String EMAIL_BODY_WELCOME = "WelCome";
    public static String EMAIL_BODY_CANCEL = "Cancelled";

    public static String SMS_MESSAGE_CANCEL = "Cancelled";
    public static String SMS_MESSAGE_REMINDER = "Reminder";

    public static final String SMS_OTP_MESSAGE = " is OTP for txn. OTP is valid for 10 mins only. Pls do not share it with anyone";
}
