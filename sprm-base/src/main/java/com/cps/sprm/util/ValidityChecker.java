package com.cps.sprm.util;

import org.apache.log4j.Logger;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Created by sumeshs on 1/18/2017.
 */
public class ValidityChecker {

    private static final Logger logger = Logger.getLogger(ValidityChecker.class);

    public static boolean isMembershipStillValid(Long validTimeMillis, Clock clock){

        boolean isValid = true;
        java.time.LocalDate todayDate = LocalDate.now(clock);

        logger.debug("The date to be checked " + todayDate.toString());
        java.time.LocalDateTime validDateTime = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(validTimeMillis), ZoneId.of("UTC"));
        java.time.LocalDate validDate = validDateTime.toLocalDate();

        if(todayDate.isAfter(validDate)){
            isValid = false;
         }
        logger.debug("The validity is till " + validDate.toString());
        logger.debug("The validity being returned " + isValid);
        return isValid;
    }

    public static boolean isValidPhoneNo(String phoneNo) {
        if (null != phoneNo && phoneNo.matches("\\d{10}")) {
            return true;
        }
        return false;
    }
}
