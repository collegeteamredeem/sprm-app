package com.cps.sprm.util;

/**
 * Created by sumeshs on 2/22/2017.
 */
public class Constants {

    public static final String SEND_EMAIL_SWITCH = "SEND_EMAIL_SWITCH";
    public static final String EMAIL_FROM_ADDR = "EMAIL_FROM_ADDR";
    public static final String SEND_SMS_SWITCH = "SEND_SMS_SWITCH";
    public static final String SMS_USERNAME = "SMS_USERNAME";
    public static final String SMS_HASH = "SMS_HASH";
    public static final String SMS_SENDER_ID = "SMS_SENDER_ID";
    public static final String SMS_HTTP_URL = "SMS_HTTP_URL";
    public static final String SMS_TEST_ONLY = "SMS_TEST_ONLY";

}
