package com.cps.sprm.service;

import com.cps.sprm.customExceptions.AptAlreadyCancelledException;
import com.cps.sprm.customExceptions.AptCannotBeCancelledException;
import com.cps.sprm.customExceptions.AptNotFreeException;
import com.cps.sprm.customExceptions.AptTimedOutException;
import com.cps.sprm.customExceptions.AptNotOwnedException;
import com.cps.sprm.dao.AppointmentDao;
import com.cps.sprm.dao.CustomerDao;
import com.cps.sprm.dao.LocationDao;
import com.cps.sprm.dao.ServiceProviderDao;
import com.cps.sprm.email.IEmailAPI;
import com.cps.sprm.entity.AppointmentEntity;
import com.cps.sprm.entity.CustomerEntity;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.entity.ServiceProviderEntity;
import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import com.cps.sprm.sms.ISmsProvider;
import com.cps.sprm.util.Messages;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by sumeshs on 12/29/2016.
 */
@Service(value="appointmentServiceImpl")
@Transactional
public class AppointmentService implements IAppointmentService {

    //TODO : Change this to singleton
    DozerBeanMapper mapper;

    @Autowired
    private AppointmentDao appointmentDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ServiceProviderDao serviceProviderDao;

    @Autowired
    private LocationDao locationDao;

    @Resource(name="emailAPIServiceImpl")
    private IEmailAPI emailAPI;

    @Resource(name="smsAPIServiceImpl")
    private ISmsProvider smsAPI;

    @PostConstruct
    public void populateAppointment()
    {
        //initialize session factory on Service provider DAO Object
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-hibernate.xml");
        mapper = (DozerBeanMapper) context.getBean("mapper");
        appointmentDao.setMapper(mapper);
        customerDao.setMapper(mapper);
        serviceProviderDao.setMapper(mapper);
        locationDao.setMapper(mapper);

    }

    @Override
    public AppointmentV1 createAppointment(AppointmentV1 appointment) throws Exception {
        AppointmentEntity appointmentEntity = mapper.map(appointment,
                AppointmentEntity.class);
        appointmentDao.save(appointmentEntity);
        AppointmentV1 persistedAppointment = mapper.map(appointmentEntity, AppointmentV1.class);
        return persistedAppointment;
    }

    @Override
    public AppointmentV1 updateAppointment(Long id, AppointmentV1 appointment) throws Exception {
        AppointmentEntity appointmentEntity = mapper.map(appointment,
                AppointmentEntity.class);
        appointmentDao.update(id, appointmentEntity);
        AppointmentV1 updatedAppointment = mapper.map(appointmentEntity, AppointmentV1.class);
        return updatedAppointment;
    }

    @Override
    public void deleteAppointmentById(Long appId) throws Exception{
        try {
            AppointmentEntity appointmentEntity  = appointmentDao.listById(appId);
            if(null != appointmentEntity) {
                appointmentDao.delete(appointmentEntity);
            }
        }catch (Exception e){
            // log this
            e.printStackTrace();
        }
    }

    @Override
    public List<AppointmentV2> getAllAppointments() throws Exception {
        List<AppointmentEntity> appointmentEntityList;
        List<AppointmentV2> appointmentV2s = null;

        appointmentEntityList = appointmentDao.list();
        appointmentV2s = new ArrayList<AppointmentV2>();
        for(AppointmentEntity appointmentEntity : appointmentEntityList)
        {
            appointmentV2s.add(mapper.map(appointmentEntity, AppointmentV2.class));
        }
        return appointmentV2s;
    }

    @Override
    public AppointmentV2 getAppointmentById(Long id) throws Exception {
        AppointmentEntity appointmentEntity = appointmentDao.listById(id);
        if(null != appointmentEntity) {
            AppointmentV2 appointmentV2 = mapper.map(appointmentEntity, AppointmentV2.class);
            return appointmentV2;
        }
        return null;
    }

    @Override
    public AppointmentV1 getAppointmentV1ById(Long id) throws Exception {
        AppointmentEntity appointmentEntity = appointmentDao.listById(id);
        if(null != appointmentEntity) {
            AppointmentV1 appointmentV1 = mapper.map(appointmentEntity, AppointmentV1.class);
            return appointmentV1;
        }
        return null;
    }

    @Override
    public List<AppointmentV2> getAppointmentsBySPLocAndStatus(Long spId, Long locId, AvailabilityV1 availabilityV1, AppointmentStatus status) throws Exception{
        List<AppointmentEntity> appointmentEntityList;
        List<AppointmentV2> appointmentV2s = null;

        appointmentEntityList = appointmentDao.listBySPLocAvailAndStatus(spId, locId, availabilityV1.getDateTimeFrom(), availabilityV1.getDateTimeTo(), status);
        appointmentV2s = new ArrayList<AppointmentV2>();
        for(AppointmentEntity appointmentEntity : appointmentEntityList)
        {
            System.out.println("#####Adding apt with token number " + appointmentEntity.getTimeSlot() + "   " + appointmentEntity.getTokenNumber());
            appointmentV2s.add(mapper.map(appointmentEntity, AppointmentV2.class));
        }
        return appointmentV2s;
    }

    @Override
    public List<AppointmentV2> getAllAppointmentsBySPLoc(Long spId, Long locId, AvailabilityV1 availabilityV1) throws Exception{
        List<AppointmentEntity> appointmentEntityList;
        List<AppointmentV2> appointmentV2s = null;

        appointmentEntityList = appointmentDao.listBySPLocAndAvail(spId, locId, availabilityV1.getDateTimeFrom(), availabilityV1.getDateTimeTo());
        appointmentV2s = new ArrayList<AppointmentV2>();
        for(AppointmentEntity appointmentEntity : appointmentEntityList)
        {
            System.out.println("#####Adding apt with token number " + appointmentEntity.getTimeSlot() + "   " + appointmentEntity.getTokenNumber());
            appointmentV2s.add(mapper.map(appointmentEntity, AppointmentV2.class));
        }
        return appointmentV2s;
    }

    //Book by Customer
    @Override
    public AppointmentV1 bookAppointmentByIdOfCustomer(Long custId, Long aptId) throws Exception {

        CustomerEntity customerEntity = customerDao.listById(custId);
        if (null != customerEntity) {
            AppointmentEntity appointmentEntity = appointmentDao.listById(aptId);
            if (null != appointmentEntity) {
                AppointmentStatus appointmentStatus = appointmentEntity.getAppointmentStatus();
                if(appointmentStatus.equals(AppointmentStatus.FREE)){
                    if(appointmentEntity.getMyCustomer() != null){
                        throw new AptNotFreeException("The customer is already set, Internal Error cannot book appointment");
                    }
                    appointmentEntity.setAppointmentStatus(AppointmentStatus.BOOKED);
                    appointmentEntity.setMyCustomer(customerEntity);
                    appointmentDao.update(aptId, appointmentEntity);
                    AppointmentEntity updatedApptEntity = appointmentDao.listById(aptId);
                    return mapper.map(updatedApptEntity, AppointmentV1.class);
                }
                else if(appointmentStatus.equals(AppointmentStatus.BOOKED)){
                    throw  new AptNotFreeException("This slot is not free, is already booked");
                }else if(appointmentStatus.equals(AppointmentStatus.CANCELLEDBYSP)){
                    throw  new AptAlreadyCancelledException("Cannot book this slot, the SP is not free");
                }else if(appointmentStatus.equals(AppointmentStatus.TIMEDOUT)){
                    throw new AptTimedOutException("Cannot book this slot, SP is timeout, should be deleted");
                }
            }
        }

        return null;
    }

    // Cancel by Customer
    @Override
    public AppointmentV1 cancelAppointmentByIdOfCustomer(Long custId, Long aptId) throws Exception {
        CustomerEntity customerEntity = customerDao.listById(custId);
        if (null != customerEntity) {
            AppointmentEntity appointmentEntity = appointmentDao.listById(aptId);
            if (null != appointmentEntity) {
                AppointmentStatus appointmentStatus = appointmentEntity.getAppointmentStatus();
                if(appointmentStatus.equals(AppointmentStatus.BOOKED)){
                    if(appointmentEntity.getMyCustomer() != null && !appointmentEntity.getMyCustomer().equals(customerEntity)){
                        throw new AptNotOwnedException("This customer does not own the appointment, Internal Error cannot cancel appointment");
                    }
                    appointmentEntity.setAppointmentStatus(AppointmentStatus.FREE);
                    appointmentEntity.setMyCustomer(null);
                    appointmentDao.update(aptId, appointmentEntity);
                    AppointmentEntity updatedApptEntity = appointmentDao.listById(aptId);
                    return mapper.map(updatedApptEntity, AppointmentV1.class);
                }else if(appointmentStatus.equals(AppointmentStatus.FREE)){
                    throw new AptCannotBeCancelledException("This slot is already free, cannot cancel");
                }else if(appointmentStatus.equals(AppointmentStatus.CANCELLEDBYSP)){
                    throw new AptAlreadyCancelledException("This slot is already cancelled by SP, cannot cancel");
                }else if(appointmentStatus.equals(AppointmentStatus.TIMEDOUT)){
                    throw new AptTimedOutException("This slot is timedout, cannot cancel, should be deleted");
                }
            }
        }
        return null;
    }

    // Cancel a single appointment by SP
    @Override
    public AppointmentV1 cancelAppointmentByIdOfSP(Long spId, Long aptId, boolean force) throws Exception{
        ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(spId);
        if(null != serviceProviderEntity){
            AppointmentEntity appointmentEntity = appointmentDao.listById(aptId);
            if(null != appointmentEntity){
                AppointmentStatus appointmentStatus = appointmentEntity.getAppointmentStatus();
                if(appointmentStatus.equals(AppointmentStatus.BOOKED)){
                    if(appointmentEntity.getMyServiceProvider() != null && !appointmentEntity.getMyServiceProvider().getServiceproviderid().equals(serviceProviderEntity.getServiceproviderid())){
                        throw new AptNotOwnedException("This SP does not own the appointment, Internal Error cannot cancel appointment");
                    }

                    if(appointmentEntity.getMyCustomer() != null &&  force != true){
                        throw new AptNotFreeException("This appointment is already booked by a customer, cannot cancel ");
                    }

                    appointmentEntity.setAppointmentStatus(AppointmentStatus.CANCELLEDBYSP);
                    //appointmentEntity.setMyServiceProvider(null);
                    appointmentEntity.setMyCustomer(null);
                    appointmentDao.update(aptId, appointmentEntity);
                    AppointmentEntity updatedApptEntity = appointmentDao.listById(aptId);
                    return mapper.map(updatedApptEntity, AppointmentV1.class);
                }else if(appointmentStatus.equals(AppointmentStatus.FREE)){
                    throw new AptCannotBeCancelledException("This slot is already free, cannot cancel");
                }else if(appointmentStatus.equals(AppointmentStatus.CANCELLEDBYSP)){
                    throw new AptAlreadyCancelledException("This slot is already cancelled by SP, cannot cancel");
                }else if(appointmentStatus.equals(AppointmentStatus.TIMEDOUT)){
                    throw new AptTimedOutException("This slot is timedout, cannot cancel, should be deleted");
                }
            }
        }
        return null;
    }

    // Cancel all apts by SP in a location for a duration
    public void cancelAppointmentsByLocSP(Long spId, Long locId, AvailabilityV1 availabilityV1, boolean force) throws Exception{
       List<AppointmentEntity> appointmentEntityList = appointmentDao.listBySPLocAndAvail(spId, locId, availabilityV1.getDateTimeFrom(), availabilityV1.getDateTimeTo());
        for(AppointmentEntity appointment : appointmentEntityList){

            if(appointment.getMyCustomer() != null &&  force != true){
                throw new AptNotFreeException("This appointment is already booked by a customer, cannot cancel ");
            }
            AppointmentStatus appointmentStatus = appointment.getAppointmentStatus();
            if(!appointmentStatus.equals(AppointmentStatus.CANCELLEDBYSP) && !appointmentStatus.equals(AppointmentStatus.TIMEDOUT)){
                appointment.setAppointmentStatus(AppointmentStatus.CANCELLEDBYSP);
                appointmentDao.update(appointment.getAppointmentid(), appointment);

                if(appointment.getMyCustomer() != null) {
                    emailAPI.sendEmail(appointment.getMyCustomer().getEmail(), Messages.EMAIL_SUBJECT_CANCEL, Messages.EMAIL_BODY_CANCEL);
                    smsAPI.sendSms(Messages.SMS_MESSAGE_CANCEL, appointment.getMyCustomer().getCustomerphonenumber());
                }
            }
        }

    }

    // Cancel all apts by SP in all his locations for a duration
    public void cancelAppointmentsByAllLocSP(Long spId, AvailabilityV1 availabilityV1, boolean force) throws Exception{
        ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(spId);

        if(null != serviceProviderEntity){
            Set<LocationEntity> locations = serviceProviderEntity.getLocations();
            for(LocationEntity location : locations){
                List<AppointmentEntity> appointmentEntityList = appointmentDao.listBySPLocAndAvail(spId, location.getLocationid(), availabilityV1.getDateTimeFrom(), availabilityV1.getDateTimeTo());
                for(AppointmentEntity appointment : appointmentEntityList){

                    if(appointment.getMyCustomer() != null &&  force != true){
                        throw new AptNotFreeException("This appointment is already booked by a customer, cannot cancel ");
                    }
                    AppointmentStatus appointmentStatus = appointment.getAppointmentStatus();
                    if(!appointmentStatus.equals(AppointmentStatus.CANCELLEDBYSP) && !appointmentStatus.equals(AppointmentStatus.TIMEDOUT)){
                        appointment.setAppointmentStatus(AppointmentStatus.CANCELLEDBYSP);
                        appointmentDao.update(appointment.getAppointmentid(), appointment);

                        if(appointment.getMyCustomer() != null) {
                            emailAPI.sendEmail(appointment.getMyCustomer().getEmail(), Messages.EMAIL_SUBJECT_CANCEL, Messages.EMAIL_BODY_CANCEL);
                            smsAPI.sendSms(Messages.SMS_MESSAGE_CANCEL, appointment.getMyCustomer().getCustomerphonenumber());
                        }
                    }
                }
            }
        }
    }
}
