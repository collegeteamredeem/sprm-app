package com.cps.sprm.service;

import java.util.List;

import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;

public interface IServiceProviderService
{
	ServiceProviderV1 createServiceProvider(ServiceProviderV1 serviceProvider) throws Exception;

	List<ServiceProviderV1> getAllServiceProviders();

    ServiceProviderV1 getServiceProviderById(Long serviceProviderId);

	ServiceProviderV1 getServiceProviderByPhoneNumber(String phoneNumber) throws Exception;

	ServiceProviderV1 addAvailability(Long serviceProviderId, Long locId, AvailabilityV1 availabilityDto) throws Exception;

	List<AppointmentV2> genAppointmentsForThisLocation(Long serviceProviderId, Long locId) throws Exception;

	List<AppointmentV1> genAppointmentsForAllLocations(Long serviceProviderId) throws Exception;

	ServiceProviderV1 deleteAvailability(Long serviceProviderId, Long locId, AvailabilityV1 availabilityDto, boolean force) throws Exception;

	void deleteServiceProviderById(Long serviceProviderId);

	ServiceProviderV1 updateServiceProvider(ServiceProviderV1 serviceProviderUpdateDto, Long serviceProviderId) throws Exception;

	ServiceProviderV1 updateServiceProviderValidity(Long serviceProviderId, Long validMonths) throws Exception;
}
