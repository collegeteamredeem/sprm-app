package com.cps.sprm.service;

/**
 * Created by sumeshs on 2/22/2017.
 */
public interface IOtpService {

    String genOtp(String phoneNumber, boolean generateNew);

    void clearOtp(String phoneNumber);

}
