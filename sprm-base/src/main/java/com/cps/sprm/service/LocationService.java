package com.cps.sprm.service;

import com.cps.sprm.dao.LocationDao;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.sdk.dto.LocationV1;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumeshs on 12/28/2016.
 */
@Service(value="locationServiceImpl")
@Transactional
public class LocationService implements ILocationService {


    private static final Logger logger = Logger.getLogger(LocationService.class);

    //TODO : Change this to singleton
    DozerBeanMapper mapper;

    @Autowired
    private LocationDao locationDao;

    @PostConstruct
    public void populateLocation()
    {
        //initialize session factory on Service provider DAO Object
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-hibernate.xml");
        mapper = (DozerBeanMapper) context.getBean("mapper");
        locationDao.setMapper(mapper);
    }

    @Override
    public LocationV1 createLocation(LocationV1 location) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("createLocation called for " + location.toString());
        }
        LocationEntity locationEntity = mapper.map(location,
                LocationEntity.class);
        locationDao.save(locationEntity);
        LocationV1 persistedLocation = mapper.map(locationEntity, LocationV1.class);
        return persistedLocation;
    }

    @Override
    public LocationV1 updateLocation(Long id, LocationV1 location) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("updateLocation called for " + location.toString());
            logger.debug("updateLocation called for id " + id);
        }
        LocationEntity locationEntity = mapper.map(location,
                LocationEntity.class);
        locationDao.update(id, locationEntity);
        LocationV1 updatedLocation = mapper.map(locationEntity, LocationV1.class);
        return updatedLocation;
    }

    @Override
    public void deleteLocationById(Long locId) throws Exception{
        try {
            LocationEntity locationEntity = locationDao.listById(locId);
            if(null != locationEntity) {
                locationDao.delete(locationEntity);
            }
        }catch (Exception e){
            // log this
            e.printStackTrace();
        }
    }
    @Override
    public List<LocationV1> getAllLocations() throws Exception {
        List<LocationEntity> locationEntities;
        List<LocationV1> locationV1s = null;

        locationEntities = locationDao.list();
        locationV1s = new ArrayList<LocationV1>();
        for(LocationEntity locationEntity : locationEntities)
        {
            locationV1s.add(mapper.map(locationEntity, LocationV1.class));
        }
        if(logger.isDebugEnabled()) {
            logger.debug("getAllLocations called, returning entries " + locationV1s.size());
        }
        return locationV1s;
    }

    @Override
    public LocationV1 getLocationById(Long id) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("getLocationById called for id " + id);
        }
        LocationEntity locationEntity = locationDao.listById(id);
        if(null != locationEntity) {
            LocationV1 locationV1 = mapper.map(locationEntity, LocationV1.class);
            if(logger.isDebugEnabled()) {
                logger.debug("Location found for this id " + locationV1.toString());
            }
            return locationV1;
        }
        if(logger.isDebugEnabled()) {
            logger.debug("Location was not found for this id " + id);
        }
        return null;
    }
}
