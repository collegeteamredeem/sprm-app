package com.cps.sprm.service;

import com.cps.sprm.sdk.dto.CustomerV1;

import java.util.List;

/**
 * Created by sumeshs on 11/8/2016.
 */
public interface ICustomerService {

    CustomerV1 createCustomer(CustomerV1 customer) throws Exception;

    CustomerV1 updateCustomer(Long id, CustomerV1 customer) throws Exception;

    void deleteCustomerById(Long id) throws Exception;

    List<CustomerV1> getAllCustomers() throws Exception;

    CustomerV1 getCustomerById(Long id) throws Exception;

    CustomerV1 getCustomerByPhoneNumber(String phoneNumber) throws Exception;

    void addToCustomerFavourites(Long customerid, Long spId) throws Exception;

    void deleteFromCustomerFavourites(Long customerid, Long spId) throws Exception;
}
