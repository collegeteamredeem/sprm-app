package com.cps.sprm.service;

import com.cps.sprm.sms.ISmsProvider;
import com.cps.sprm.util.Messages;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by sumeshs on 2/22/2017.
 */
@Service(value="otpServiceImpl")
@Transactional
public class OtpService implements IOtpService {

    private static final Logger logger = Logger.getLogger(OtpService.class);

    @Resource(name="smsAPIServiceImpl")
    private ISmsProvider smsAPI;

    @Override
    @Cacheable(cacheNames="otp", key="#phoneNumber")
    public String genOtp(String phoneNumber, boolean generateNew) {
        if(generateNew) {
            int randomPIN = (int) (Math.random() * 9000) + 1000;
            logger.debug("Generated pin " + randomPIN);
            String pinStr = String.valueOf(randomPIN);
            smsAPI.sendSms(pinStr + Messages.SMS_OTP_MESSAGE, phoneNumber);
            return  pinStr;
        }else {
            return null;
        }
    }

    @Override
    @CacheEvict(cacheNames="otp",  key="#phoneNumber")
    public void clearOtp(String phoneNumber) {
        // No Op
    }

}
