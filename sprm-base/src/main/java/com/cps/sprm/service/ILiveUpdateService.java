package com.cps.sprm.service;

import com.cps.sprm.sdk.dto.LiveUpdateV2;
import java.util.List;

/**
 * Created by sumeshs on 1/25/2017.
 */
public interface ILiveUpdateService {

    LiveUpdateV2 createLiveUpdate(LiveUpdateV2 liveUpdate, Long spId, Long locId) throws Exception;

    LiveUpdateV2 updateLiveUpdate(Long id, LiveUpdateV2 liveUpdate) throws Exception;

    LiveUpdateV2 endLiveUpdate(Long id, LiveUpdateV2 liveUpdate) throws Exception;

    void deleteLiveUpdate(LiveUpdateV2 liveUpdate) throws Exception;

    void deleteLiveUpdateById(Long liveUpdateId) throws Exception;

    List<LiveUpdateV2> getLiveUpdates() throws Exception;

    LiveUpdateV2 getLiveUpdateById(Long id) throws Exception;
}
