package com.cps.sprm.service;

import com.cps.sprm.dao.LiveUpdateDao;
import com.cps.sprm.dao.LocationDao;
import com.cps.sprm.dao.ServiceProviderDao;
import com.cps.sprm.entity.LiveUpdateEntity;
import com.cps.sprm.sdk.dto.LiveUpdateV2;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;

/**
 * Created by sumeshs on 1/25/2017.
 */
@Service(value="liveUpdateServiceImpl")
@Transactional
public class LiveUpdateService implements ILiveUpdateService {


    private static final Logger logger = Logger.getLogger(LiveUpdateService.class);

    //TODO : Change this to singleton
    DozerBeanMapper mapper;

    @Autowired
    private LiveUpdateDao liveUpdateDao;
    @Autowired
    private ServiceProviderDao serviceProviderDao;
    @Autowired
    private LocationDao locationDao;

    @PostConstruct
    public void populateLiveUpdates()
    {
        //initialize session factory on Service provider DAO Object
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-hibernate.xml");
        mapper = (DozerBeanMapper) context.getBean("mapper");
        liveUpdateDao.setMapper(mapper);
    }


    @Override
    public LiveUpdateV2 createLiveUpdate(LiveUpdateV2 liveUpdate, Long spId, Long locId) throws Exception {
        if(serviceProviderDao.listById(spId) == null){
            logger.warn("Invalid spId passed , cannot find " + spId);
           throw new Exception("SP NOT Found");
        }
        if(locationDao.listById(locId) == null){
            logger.warn("Invalid locId passed , cannot find " + locId);
            throw new Exception("Location NOT Found");
        }

        LiveUpdateV2 liveUpdateV2ForPersistUpdate = liveUpdate;
        liveUpdateV2ForPersistUpdate.setMyServiceProviderId(spId);
        liveUpdateV2ForPersistUpdate.setMyLocationId(locId);
        LocalDate date = LocalDate.now();
        Long startOfDayEpoch = date.atStartOfDay().toEpochSecond(ZoneOffset.UTC);
        // check if entry already exists

        boolean create = true;
        LiveUpdateV2 liveUpdateV2IfAlreadyPresent = liveUpdateDao.listBySPLocAndDate(spId, locId,startOfDayEpoch );
        if(null != liveUpdateV2IfAlreadyPresent){
            liveUpdateV2ForPersistUpdate = liveUpdateV2IfAlreadyPresent;
            create = false;
        }

        LiveUpdateEntity liveUpdateEntity = mapper.map(liveUpdateV2ForPersistUpdate,
                LiveUpdateEntity.class);
        liveUpdateEntity.setTodayDate(startOfDayEpoch);
        liveUpdateEntity.setConsultingStartTime(System.currentTimeMillis());
        liveUpdateEntity.setAvgConsultingTime(0.0f);
        liveUpdateEntity.setCurrentToken(1);
        if(create) {
            liveUpdateDao.save(liveUpdateEntity, spId, locId);
        }else {
            liveUpdateDao.update(liveUpdateV2IfAlreadyPresent.getLiveupdateid(), liveUpdateEntity);
        }
        LiveUpdateV2 persistedLiveUpdate = mapper.map(liveUpdateEntity, LiveUpdateV2.class);
        return persistedLiveUpdate;

    }

    @Override
    @CachePut(cacheNames="liveupdates", key="#id")
    public LiveUpdateV2 updateLiveUpdate(Long id, LiveUpdateV2 liveUpdate) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("updateLiveUpdate called for " + liveUpdate.toString());
            logger.debug("updateLiveUpdate called for id " + id);
        }
        System.out.println("updateLiveUpdate called for " + liveUpdate.toString());
        System.out.println("updateLiveUpdate called for id " + id);
        LiveUpdateEntity liveUpdateEntity = mapper.map(liveUpdate,
                LiveUpdateEntity.class);
        System.out.println("The liveupateEntity generated is " + liveUpdateEntity.toString());
        if(liveUpdateEntity.getCurrentToken() > 1) {
            Long currentTime = System.currentTimeMillis();
            Float consultingTime = Float.valueOf(((currentTime - liveUpdateEntity.getConsultingStartTime()) / (liveUpdateEntity.getCurrentToken() - 1)));
            liveUpdateEntity.setAvgConsultingTime(consultingTime);
        }
        liveUpdateDao.update(id, liveUpdateEntity);
        LiveUpdateV2 updatedLiveUpdateV2 = mapper.map(liveUpdateEntity, LiveUpdateV2.class);
        return updatedLiveUpdateV2;
    }

    @Override
    @CacheEvict(cacheNames="liveupdates",  key="#id")
    public LiveUpdateV2 endLiveUpdate(Long id, LiveUpdateV2 liveUpdate) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("endLiveUpdate called for " + liveUpdate.toString());
            logger.debug("endLiveUpdate called for id " + id);
        }
        LiveUpdateEntity liveUpdateEntity = mapper.map(liveUpdate,
                LiveUpdateEntity.class);
        liveUpdateEntity.setConsultingEndTime(System.currentTimeMillis());
        liveUpdateDao.update(id, liveUpdateEntity);
        LiveUpdateV2 afterUpdateV2 = mapper.map(liveUpdateEntity, LiveUpdateV2.class);
        return afterUpdateV2;
    }

    @Override
    public void deleteLiveUpdate(LiveUpdateV2 liveUpdate) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("deleteLiveUpdate called for " + liveUpdate.toString());
        }
        LiveUpdateEntity liveUpdateEntity = mapper.map(liveUpdate,
                LiveUpdateEntity.class);
        liveUpdateDao.delete(liveUpdateEntity);
    }

    @Override
    public void deleteLiveUpdateById(Long liveUpdateId) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("deleteLiveUpdate called for " + liveUpdateId);
        }
        try {
            LiveUpdateEntity liveUpdateEntity = liveUpdateDao.listByIdGetObject(liveUpdateId);
            if(null != liveUpdateEntity) {
                liveUpdateDao.delete(liveUpdateEntity);
            }
        }catch (Exception e){
            // log this
            e.printStackTrace();
        }
    }

    @Override
    public List<LiveUpdateV2> getLiveUpdates() throws Exception {
        List<LiveUpdateV2> liveUpdateList = liveUpdateDao.list();
        if(logger.isDebugEnabled()) {
            logger.debug("getLiveUpdates called, returning entries " + liveUpdateList.size());
        }
        return liveUpdateList;
    }

    @Override
    @Cacheable(cacheNames="liveupdates", key="#id")
    public LiveUpdateV2 getLiveUpdateById(Long id) throws Exception {
        if(logger.isDebugEnabled()) {
            logger.debug("getLiveUpdateById called for id " + id);
        }
        LiveUpdateV2 liveUpdateV2 = liveUpdateDao.listById(id);
        if(null != liveUpdateV2) {
            if (logger.isDebugEnabled()) {
                logger.debug("Location was not found for this id " + id);
                logger.debug(liveUpdateV2.toString());
            }
        }
        return liveUpdateV2;
    }
}
