package com.cps.sprm.service;

import com.cps.sprm.dao.CustomerDao;
import com.cps.sprm.dao.ServiceProviderDao;
import com.cps.sprm.entity.CustomerEntity;
import com.cps.sprm.entity.ServiceProviderEntity;
import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sms.ISmsProvider;
import com.cps.sprm.util.ValidityChecker;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumeshs on 11/8/2016.
 */

@Service(value="customerServiceImpl")
@Transactional
public class CustomerService implements ICustomerService
{

    //TODO : Change this to singleton
    DozerBeanMapper mapper;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ServiceProviderDao serviceProviderDao;

    @Resource(name="smsAPIServiceImpl")
    private ISmsProvider smsAPI;


    @PostConstruct
    public void populateCustomer()
    {
        //initialize session factory on Service provider DAO Object
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-hibernate.xml");
        mapper = (DozerBeanMapper) context.getBean("mapper");
        customerDao.setMapper(mapper);
        serviceProviderDao.setMapper(mapper);
    }


    public CustomerV1 createCustomer(CustomerV1 customer) throws Exception{
        CustomerEntity customerEntity = mapper.map(customer,
                CustomerEntity.class);
        customerDao.save(customerEntity);
        CustomerV1 persistedCustomer = mapper.map(customerEntity, CustomerV1.class);
        return persistedCustomer;
    }

    public CustomerV1 updateCustomer(Long id, CustomerV1 customer) throws Exception{
        CustomerEntity customerEntity = mapper.map(customer,
                CustomerEntity.class);
        customerDao.update(id, customerEntity);
        CustomerV1 updatedCustomer = mapper.map(customerEntity, CustomerV1.class);
        return updatedCustomer;
    }

    public void deleteCustomerById(Long customerId) throws Exception{
        try {
            CustomerEntity customerEntity = customerDao.listById(customerId);
            if(null != customerEntity) {
                customerDao.delete(customerEntity);
            }
        }catch (Exception e){
            // log this
            e.printStackTrace();
        }
    }

    public List<CustomerV1> getAllCustomers() throws Exception {

        List<CustomerEntity> customerEntityList;
        List<CustomerV1> customerV1s = null;

        customerEntityList = customerDao.list();
        customerV1s = new ArrayList<CustomerV1>();
        for(CustomerEntity customerEntity : customerEntityList)
        {
            customerV1s.add(mapper.map(customerEntity, CustomerV1.class));
        }
        return customerV1s;
    }

    public CustomerV1 getCustomerById(Long id) throws Exception {
        CustomerEntity customerEntity = customerDao.listById(id);
        if(null != customerEntity) {
            CustomerV1 customerV1 = mapper.map(customerEntity, CustomerV1.class);
            return customerV1;
        }
        return null;
    }



    @Override
    public CustomerV1 getCustomerByPhoneNumber(String phoneNumber) throws Exception {

        if(ValidityChecker.isValidPhoneNo(phoneNumber)) {
            CustomerEntity customerEntity = customerDao.listByPhoneNumber(phoneNumber);
            if (null != customerEntity) {
                CustomerV1 customerV1 = mapper.map(customerEntity, CustomerV1.class);
                return customerV1;
            }
        }else {
            throw new Exception("Invalid phone number");
        }
        return null;
    }

    public void addToCustomerFavourites(Long customerid, Long spId) throws Exception {
        ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(spId);
        customerDao.addToFavourites(customerid, serviceProviderEntity);
    }

    public void deleteFromCustomerFavourites(Long customerid, Long spId) throws Exception {
        ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(spId);
        customerDao.delFromFavourites(customerid, serviceProviderEntity);
    }
}
