package com.cps.sprm.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.cps.sprm.dao.AppointmentDao;
import com.cps.sprm.dao.LocationDao;
import com.cps.sprm.email.IEmailAPI;
import com.cps.sprm.entity.AppointmentEntity;
import com.cps.sprm.entity.ServiceProviderEntity;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;
import com.cps.sprm.entity.AvailabilityEntity;
import com.cps.sprm.util.Messages;
import com.cps.sprm.util.ValidityChecker;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cps.sprm.dao.ServiceProviderDao;

@Service(value="serviceProviderServiceImpl")
@Transactional
public class ServiceProviderService implements IServiceProviderService
{

	private DozerBeanMapper mapper;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	@Autowired
	private LocationDao locationDao;

	@Autowired
	private AppointmentDao appointmentDao;

	@Resource(name="emailAPIServiceImpl")
	private IEmailAPI emailAPI;

	@PostConstruct
	public void populateServiceProvider()
	{
		//initialize session factory on Service provider DAO Object
		ClassPathXmlApplicationContext context =
				new ClassPathXmlApplicationContext("spring-hibernate.xml");
		mapper = (DozerBeanMapper) context.getBean("mapper");
		serviceProviderDao.setMapper(mapper);
	}

	@Transactional
	public ServiceProviderV1 createServiceProvider(ServiceProviderV1 serviceProvider) throws Exception
	{
		ServiceProviderEntity serviceProviderEntity = mapper.map(serviceProvider,
				ServiceProviderEntity.class);
		serviceProviderEntity.setSpecializations(serviceProvider.getSpecializations());
		serviceProviderDao.save(serviceProviderEntity);
		ServiceProviderV1 persistedSP = mapper.map(serviceProviderEntity, ServiceProviderV1.class);
		// send mail
		emailAPI.sendEmail(persistedSP.getEmail(), Messages.EMAIL_SUBJECT_WELCOME, Messages.EMAIL_BODY_WELCOME);
		return persistedSP;
	}

	@Transactional
	public List<ServiceProviderV1> getAllServiceProviders()
	{
		List<ServiceProviderEntity> serviceProviderEntityList;
		List<ServiceProviderV1> serviceProviderList = null;
		try {
			serviceProviderEntityList = serviceProviderDao.list();
			serviceProviderList = new ArrayList<ServiceProviderV1>();
			for (ServiceProviderEntity serviceProviderEnity : serviceProviderEntityList) {
				serviceProviderList.add(mapper.map(serviceProviderEnity, ServiceProviderV1.class));
			}
		}catch (Exception e){
			// log this
			e.printStackTrace();
		}
		return serviceProviderList;
	}

	@Transactional
	public ServiceProviderV1  getServiceProviderById(Long serviceProviderId)
	{
		try {
			System.out.println("##########Trying Get By ID for " + serviceProviderId);
			ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(serviceProviderId);
			if(null != serviceProviderEntity) {
				ServiceProviderV1 serviceProviderOne = mapper.map(serviceProviderEntity, ServiceProviderV1.class);
				return serviceProviderOne;
			}
		}catch (Exception e){
			// log this
			e.printStackTrace();
		}
		return null;
	}

	@Transactional
	public void deleteServiceProviderById(Long serviceProviderId){
		try {
			ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(serviceProviderId);
			if(null != serviceProviderEntity) {
				serviceProviderDao.delete(serviceProviderEntity);
			}
		}catch (Exception e){
			// log this
			e.printStackTrace();
		}
	}

	@Transactional
	public ServiceProviderV1 updateServiceProvider(
			ServiceProviderV1 serviceProviderUpdateDto,
			Long serviceProviderId) throws Exception
	{
		ServiceProviderEntity sp = mapper.map(serviceProviderUpdateDto, ServiceProviderEntity.class);
		sp = serviceProviderDao.updateServiceProvider(sp, serviceProviderId);
		return mapper.map(sp, ServiceProviderV1.class);
	}

	@Transactional
	public ServiceProviderV1 updateServiceProviderValidity(Long serviceProviderId,
														   Long validMonths) throws Exception{
		ServiceProviderEntity sp = serviceProviderDao.updateSPValidity(serviceProviderId, validMonths);
		return mapper.map(sp, ServiceProviderV1.class);
	}

	@Override
	@Transactional
	public ServiceProviderV1 getServiceProviderByPhoneNumber(String phoneNumber) throws Exception {

		if(ValidityChecker.isValidPhoneNo(phoneNumber)) {
			ServiceProviderEntity spEntity = serviceProviderDao.listByPhoneNumber(phoneNumber);
			if (null != spEntity) {
				ServiceProviderV1 serviceProviderV1 = mapper.map(spEntity, ServiceProviderV1.class);
				return serviceProviderV1;
			}
		}else {
			throw new Exception("Invalid phone number");
		}
		return null;
	}

	public ServiceProviderV1 addAvailability(Long serviceProviderId, Long locId, AvailabilityV1 availabilityDto) throws Exception{
		AvailabilityEntity availabilityEntity = mapper.map(availabilityDto, AvailabilityEntity.class);
		ServiceProviderEntity sp = serviceProviderDao.addAvailability(serviceProviderId, locId, availabilityEntity);
		return mapper.map(sp, ServiceProviderV1.class);
	}

	public ServiceProviderV1 deleteAvailability(Long serviceProviderId, Long locId, AvailabilityV1 availabilityDto, boolean force) throws Exception{
		AvailabilityEntity availabilityEntity = mapper.map(availabilityDto, AvailabilityEntity.class);
		ServiceProviderEntity sp = serviceProviderDao.deleteAvailability(serviceProviderId, locId,availabilityEntity, force);
		return mapper.map(sp, ServiceProviderV1.class);
	}

	@Transactional
	public List<AppointmentV2> genAppointmentsForThisLocation(Long serviceProviderId, Long locId) throws Exception{

		List<AppointmentV2> appointmentV2sCreated = new ArrayList<AppointmentV2>();
		try {
			ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(serviceProviderId);
			if(null != serviceProviderEntity) {
				LocationEntity locationEntity = locationDao.listById(locId);
				if(null != locationEntity){

					System.out.println("Dealing with Location " + locationEntity.toString());

					Set<AvailabilityEntity> myAvailabilityInThisLocation = locationDao.getAvailabilityForLocation(locId);
					List<AppointmentEntity> appointmentEntityList = new ArrayList<AppointmentEntity>();

					for(AvailabilityEntity availability: myAvailabilityInThisLocation){

						System.out.println("Dealing with availabilty " + availability.toString());
						int meetingMinutes = availability.getMeetingTimeInMins() != null ? availability.getMeetingTimeInMins().intValue(): 5;

						System.out.println("Dealing with meeting minutes " + meetingMinutes);

						Instant fromInstant = Instant.ofEpochMilli(availability.getDateTimeFrom());
						Instant toInstant = Instant.ofEpochMilli(availability.getDateTimeTo());

						System.out.println("From Instant " + fromInstant.toString());
						System.out.println("From Instant " + toInstant.toString());

						java.time.LocalDateTime fromDateTime = java.time.LocalDateTime.ofInstant(fromInstant, ZoneId.of("UTC"));
						java.time.LocalDateTime toDateTime = java.time.LocalDateTime.ofInstant(toInstant, ZoneId.of("UTC"));

						System.out.println("fromDateTime " + fromDateTime.toString());
						System.out.println("toDateTime " + toDateTime.toString());

						java.time.LocalDate fromDate = fromDateTime.toLocalDate();
						java.time.LocalDate toDate = toDateTime.toLocalDate();

						System.out.println("fromDate " + fromDate.toString());
						System.out.println("toDate " + toDate.toString());

						java.time.LocalTime fromTime = fromDateTime.toLocalTime();
						java.time.LocalTime endTime = toDateTime.toLocalTime();

						System.out.println("fromTime " + fromTime.toString());
						System.out.println("endTime " + endTime.toString());

						Set<java.time.LocalDate> holidays = getLocalDateHolidays(availability.getHolidays());
						Set<Integer> weeklyOffs = availability.getWeeklyoff();

						System.out.println("THe set of weekly off "+ weeklyOffs.toString());
						if(toDate.compareTo(fromDate) != 0 && toDate.isAfter(fromDate)){
							System.out.println("Got into if condition");
							System.out.println("ToDate being bumped up "+ toDate.toString());
							java.time.LocalDate toDateBumpedUp = toDate.plusDays(1);
							System.out.println("ToDate being bumped up to "+ toDate.toString());
							for (java.time.LocalDate date = fromDate; date.isBefore(toDateBumpedUp); date = date.plusDays(1))
							{
								// jump through dates
								// Check if holiday
								// Check if weeklyOff
								System.out.println("Weekly off " + date.getDayOfWeek().getValue());

								if(weeklyOffs.contains(date.getDayOfWeek().getValue())){
									// skip this
									System.out.println("Skipping this date as  weekly off " + date.toString());
									continue;
								}
								if(holidays.contains(date)){
									System.out.println("Skipping this date as  its holiday " + date.toString());
									continue;
								}
								int tokenNumber  = 1 ;
								for(java.time.LocalTime time = fromTime; time.isBefore(endTime); time = time.plusMinutes(meetingMinutes)){
									// jump through time
									LocalDateTime dateTime = LocalDateTime.of(date, time);
									AppointmentEntity appointmentEntity = new AppointmentEntity(tokenNumber, dateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
									appointmentEntity.setMyServiceProvider(serviceProviderEntity);
									appointmentEntity.setMyLocation(locationEntity);
									appointmentEntity.setFromAvailability(availability);
									appointmentEntityList.add(appointmentEntity);
									tokenNumber ++;
								}
							}
						}else {
							System.out.println("THe dates are same " + fromDate.toString() + "   " + toDate.toString());
							// days are same,  only time is different
							int tokenNumber  = 1 ;
							for(java.time.LocalTime time = fromTime; time.isBefore(endTime); time = time.plusMinutes(meetingMinutes)){
								// jump through time
								LocalDateTime dateTime = LocalDateTime.of(toDate, time);
								AppointmentEntity appointmentEntity = new AppointmentEntity(tokenNumber, dateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
								appointmentEntity.setMyServiceProvider(serviceProviderEntity);
								appointmentEntity.setMyLocation(locationEntity);
								appointmentEntity.setFromAvailability(availability);
								appointmentEntityList.add(appointmentEntity);
								tokenNumber ++;
							}
						}

					}
					if(appointmentEntityList.size() > 0){
						for(AppointmentEntity entity: appointmentEntityList){
							try {
                                appointmentV2sCreated.add(appointmentDao.saveAndReturnDto(entity));
							}catch (Exception e){
								e.printStackTrace();
								System.out.println("Exception saving apointment "+ entity.toString());
							}
						}
					}
				}

			}
		}catch (Exception e){
			// log this
			e.printStackTrace();
		}

		return appointmentV2sCreated;
	}

	public List<AppointmentV1> genAppointmentsForAllLocations(Long serviceProviderId) throws Exception {
		return null;
	}

	private Set<java.time.LocalDate> getLocalDateHolidays(Set<Long> holidays){
		Set<java.time.LocalDate> holidaysLocalDate = new HashSet<java.time.LocalDate>();
		for(Long holiday : holidays){
			Instant holidayInstant = Instant.ofEpochMilli(holiday);
			java.time.LocalDate holidayLocalDate = java.time.LocalDateTime.ofInstant(holidayInstant, ZoneId.of("UTC")).toLocalDate();
			System.out.println("The holiday local date " + holidayLocalDate.toString());
			holidaysLocalDate.add(holidayLocalDate);
		}
		return holidaysLocalDate;
	}
}
