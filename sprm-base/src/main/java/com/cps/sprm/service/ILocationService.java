package com.cps.sprm.service;

import com.cps.sprm.sdk.dto.LocationV1;
import java.util.List;

/**
 * Created by sumeshs on 12/28/2016.
 */
public interface ILocationService {

    LocationV1 createLocation(LocationV1 location) throws Exception;

    LocationV1 updateLocation(Long id, LocationV1 location) throws Exception;

    void deleteLocationById(Long locId) throws Exception;

    List<LocationV1> getAllLocations() throws Exception;

    LocationV1 getLocationById(Long id) throws Exception;
}
