package com.cps.sprm.service;

import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.enums.AppointmentStatus;

import java.util.List;

/**
 * Created by sumeshs on 12/29/2016.
 */
public interface IAppointmentService {


    AppointmentV1 createAppointment(AppointmentV1 appointment) throws Exception;

    AppointmentV1 updateAppointment(Long id, AppointmentV1 appointment) throws Exception;

    void deleteAppointmentById(Long appId) throws Exception;

    List<AppointmentV2> getAllAppointments() throws Exception;

    AppointmentV2 getAppointmentById(Long id) throws Exception;

    List<AppointmentV2> getAppointmentsBySPLocAndStatus(Long spId, Long locId, AvailabilityV1 availabilityV1, AppointmentStatus status) throws Exception;

    List<AppointmentV2> getAllAppointmentsBySPLoc(Long spId, Long locId, AvailabilityV1 availabilityV1) throws Exception;

    AppointmentV1 getAppointmentV1ById(Long id) throws Exception;

    AppointmentV1 bookAppointmentByIdOfCustomer(Long custId, Long aptId) throws Exception;

    AppointmentV1 cancelAppointmentByIdOfCustomer(Long custId, Long aptId) throws Exception;

    AppointmentV1 cancelAppointmentByIdOfSP(Long spId, Long aptId, boolean force) throws Exception;

    void cancelAppointmentsByLocSP(Long spId, Long locId, AvailabilityV1 availabilityV1, boolean force) throws Exception;

    void cancelAppointmentsByAllLocSP(Long spId, AvailabilityV1 availabilityV1, boolean force) throws Exception;
}
