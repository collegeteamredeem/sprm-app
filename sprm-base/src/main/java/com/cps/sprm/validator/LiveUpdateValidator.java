package com.cps.sprm.validator;

import com.cps.sprm.sdk.dto.LiveUpdateV2;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by sumeshs on 1/26/2017.
 */
public class LiveUpdateValidator implements Validator {
    @Override
    public boolean supports(Class clazz) {
        return LiveUpdateV2.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors e) {
    }

}
