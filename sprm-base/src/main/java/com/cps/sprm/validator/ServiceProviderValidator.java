package com.cps.sprm.validator;

import com.cps.sprm.sdk.dto.AvailabilityV1;
import com.cps.sprm.sdk.dto.HandShakeV1;
import com.cps.sprm.sdk.dto.ServiceProviderV1;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by sumeshs on 11/7/2016.
 */
public class ServiceProviderValidator implements Validator   {

    public boolean supports(Class clazz) {

        return ServiceProviderV1.class.equals(clazz)
                || AvailabilityV1.class.equals(clazz)
                || HandShakeV1.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        ServiceProviderV1 spE = (ServiceProviderV1) obj;
        ValidationUtils.rejectIfEmptyOrWhitespace(e, spE.getName() , "", "name is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, spE.getEmail() , "", "email is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, spE.getPhonenumber() , "", "phonenumber is empty");
    }
}
