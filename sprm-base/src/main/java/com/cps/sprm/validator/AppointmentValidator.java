package com.cps.sprm.validator;

/**
 * Created by sumeshs on 12/29/2016.
 */
import com.cps.sprm.sdk.dto.AppointmentV1;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.dto.AvailabilityV1;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class AppointmentValidator implements Validator {

    public boolean supports(Class clazz) {
        return AppointmentV1.class.equals(clazz)
                || AppointmentV2.class.equals(clazz)
                || AvailabilityV1.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        AppointmentV1 appointmentV1 = (AppointmentV1) obj;
        ValidationUtils.rejectIfEmptyOrWhitespace(e, appointmentV1.getTokenNumber().toString() , "", "timeSlot is empty");
    }
}