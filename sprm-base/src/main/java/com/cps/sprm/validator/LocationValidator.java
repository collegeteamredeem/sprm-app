package com.cps.sprm.validator;

import com.cps.sprm.sdk.dto.LocationV1;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by sumeshs on 12/28/2016.
 */
public class LocationValidator implements Validator{

    public boolean supports(Class clazz) {
        return LocationV1.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        LocationV1 loc1 = (LocationV1) obj;
        ValidationUtils.rejectIfEmptyOrWhitespace(e, loc1.getName() , "", "name is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, loc1.getCity() , "", "city is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, loc1.getPincode() , "", "pincode is empty");
    }
}
