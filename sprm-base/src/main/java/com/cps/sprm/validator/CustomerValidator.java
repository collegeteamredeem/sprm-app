package com.cps.sprm.validator;

import com.cps.sprm.sdk.dto.CustomerV1;
import com.cps.sprm.sdk.dto.HandShakeV1;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by sumeshs on 11/8/2016.
 */
public class CustomerValidator implements Validator {

    public boolean supports(Class clazz) {
        return CustomerV1.class.equals(clazz)
                || HandShakeV1.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        CustomerV1 custE = (CustomerV1) obj;
        ValidationUtils.rejectIfEmptyOrWhitespace(e, custE.getName() , "", "name is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, custE.getEmail() , "", "email is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, custE.getCustomerphonenumber() , "", "phonenumber is empty");
    }
}
