package com.cps.sprm.entity;

import com.cps.sprm.sdk.enums.SPECIALIZATION;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Version;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.PrePersist;
import javax.persistence.CollectionTable;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name="serviceprovider")
public class ServiceProviderEntity  implements Serializable
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="serviceproviderid")
	private Long serviceproviderid;

	@Version
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="VERSION")
	private long version;

	@org.hibernate.validator.constraints.NotEmpty
	@Column(name="phonenumber")
	private String phonenumber;

	@Column(name="providerSMSId", unique = true)
	@org.hibernate.validator.constraints.NotEmpty
	private String providersmsid;

	@Column(name="name")
	@org.hibernate.validator.constraints.NotEmpty
	private String name;

	@Column(name="email", unique = true)
	@org.hibernate.validator.constraints.Email
	private String email;

	@Column(name="assistantonephoneNo")
	private String assistantonephoneno;

	@Column(name="assistanttwophoneNo")
	private String assistanttwophoneno;

	@Column(name="title")
	private String title;

	@Column(name="qualification")
	private String qualification;

	@Column(name="experience")
	private int experience;

	@Column(name = "validtill")
	private Long validity;

	@PrePersist
	void preInsert() {
		validity = System.currentTimeMillis() + 2592000000L; // Add 1 month by
	}

	@OneToMany(mappedBy = "myserviceprovider", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true 	)
	private Set<AvailabilityEntity> myavailability = new HashSet<AvailabilityEntity>(0);

	@OneToMany(mappedBy = "myServiceProvider",cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
	private Set<AppointmentEntity> myappointments = new HashSet<AppointmentEntity>(0);

	@ManyToMany(fetch = FetchType.LAZY, mappedBy="myfavourites")
	private Set<CustomerEntity> customers =
			new HashSet<CustomerEntity>(0);

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="serviceprovider_location",
			joinColumns={@JoinColumn(name="serviceproviderid")},
			inverseJoinColumns={@JoinColumn(name="locationid")})
	private Set<LocationEntity> locations = new HashSet<LocationEntity>(0);

	@Column(name = "specializations", nullable=false)
	@ElementCollection(targetClass = SPECIALIZATION.class, fetch=FetchType.LAZY)
	@CollectionTable(name="sp_specialization", joinColumns=@JoinColumn(name="phonenumber"))
	@Enumerated(value=EnumType.STRING)
	private Set<SPECIALIZATION> specializations = new HashSet<SPECIALIZATION>();


	@OneToOne
	private LiveUpdateEntity liveupdate;

	public Long getServiceproviderid() {
		return serviceproviderid;
	}

	public void setServiceproviderid(Long serviceproviderid) {
		this.serviceproviderid = serviceproviderid;
	}

	public Set<SPECIALIZATION> getSpecializations() {
		return specializations;
	}

	public void setSpecializations(Set<SPECIALIZATION> specializations) {
		this.specializations = specializations;
	}

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
			property="locationid", scope=LocationEntity.class)
	public Set<LocationEntity> getLocations() {
		return locations;
	}

	public void setLocations(Set<LocationEntity> locations) {
		this.locations = locations;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitle() {	return title;}

	public void setTitle(String title) {this.title = title;	}

	public String getQualification() {return qualification;	}

	public void setQualification(String qualification) {this.qualification = qualification;	}

	public int getExperience() {return experience;}

	public void setExperience(int experience) {this.experience = experience;}

	public Set<CustomerEntity> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<CustomerEntity> customers) {
		this.customers = customers;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getProvidersmsid() {
		return providersmsid;
	}

	public void setProvidersmsid(String providersmsid) {
		this.providersmsid = providersmsid;
	}

	public String getAssistantonephoneno() {
		return assistantonephoneno;
	}

	public void setAssistantonephoneno(String assistantonephoneno) {
		this.assistantonephoneno = assistantonephoneno;
	}

	public String getAssistanttwophoneno() {
		return assistanttwophoneno;
	}

	public void setAssistanttwophoneno(String assistanttwophoneno) {
		this.assistanttwophoneno = assistanttwophoneno;
	}

	public Long getValidity() {
		return validity;
	}

	public void setValidity(Long validity) {
		this.validity = validity;
	}

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
			property="availabilityid", scope=AvailabilityEntity.class)
	public Set<AvailabilityEntity> getMyavailability() {
		return myavailability;
	}

	public void setMyavailability(Set<AvailabilityEntity> myavailability) {
		this.myavailability = myavailability;
	}

	public void addToMyavailability(AvailabilityEntity availability)
	{
		availability.setMyserviceprovider(this);
		this.myavailability.add(availability);
	}
	public void delFromMyavailability(AvailabilityEntity availability) {
		availability.setMyserviceprovider(null);
		this.myavailability.remove(availability);
	}

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
			property="appointmentid", scope=AppointmentEntity.class)
	public Set<AppointmentEntity> getMyappointments() {
		return myappointments;
	}

	public void setMyappointments(Set<AppointmentEntity> myappointments) {
		this.myappointments = myappointments;
	}

	public void addToMyappointments(AppointmentEntity myappointment) {
		myappointment.setMyServiceProvider(this);
		this.myappointments.add(myappointment);
	}

	public void delFromMyappointments(AppointmentEntity myappointment) {
		myappointment.setMyServiceProvider(null);
		this.myappointments.remove(myappointment);
	}

	public LiveUpdateEntity getLiveupdate() {
		return liveupdate;
	}

	public void setLiveupdate(LiveUpdateEntity liveupdate) {
		this.liveupdate = liveupdate;
	}

	@Override
	public String toString() {
		return "ServiceProviderEntity{" +
				"serviceproviderid=" + serviceproviderid +
				", version=" + version +
				", phonenumber='" + phonenumber + '\'' +
				", providersmsid='" + providersmsid + '\'' +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", assistantonephoneno='" + assistantonephoneno + '\'' +
				", assistanttwophoneno='" + assistanttwophoneno + '\'' +
				", title='" + title + '\'' +
				", qualification='" + qualification + '\'' +
				", experience=" + experience +
				", validity=" + validity +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ServiceProviderEntity)) return false;

		ServiceProviderEntity that = (ServiceProviderEntity) o;

		return providersmsid != null ? providersmsid.equals(that.providersmsid) : that.providersmsid == null;

	}

	@Override
	public int hashCode() {
		return providersmsid != null ? providersmsid.hashCode() : 0;
	}
}
