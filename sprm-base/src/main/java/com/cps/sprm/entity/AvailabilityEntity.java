package com.cps.sprm.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;


/**
 * Created by sumeshs on 11/20/2016.
 */
@SuppressWarnings("Since15")
@Entity
@Table(name="availability")
public class AvailabilityEntity {

    @Version
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="VERSION")
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="availabilityid")
    private Long availabilityid;

    @Column
    private Long dateTimeFrom;

    @Column
    private Long dateTimeTo;

    @Column(name ="meetingtime")
    @Min(1)
    @Max(120)
    private Integer meetingTimeInMins;

    @Column(name ="schduledays")
    @Min(1)
    @Max(90)
    private Integer scheduleForNumDays;

    @ElementCollection(fetch= FetchType.LAZY)
    private Set<Integer> weeklyoff = new HashSet<Integer>();

    @ElementCollection(fetch=FetchType.LAZY)
    private Set<Long> holidays = new HashSet<Long>();

    @ManyToOne
    @JoinColumn(name="serviceproviderid")
    private ServiceProviderEntity myserviceprovider;

    @ManyToOne
    @JoinColumn(name="locationid")
    private LocationEntity mylocation;

    @OneToMany(mappedBy = "fromAvailability", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<AppointmentEntity> myappointments = new HashSet<AppointmentEntity>(0);

    public Long getDateTimeFrom() {
        return dateTimeFrom;
    }

    public void setDateTimeFrom(Long dateTimeFrom) {
        this.dateTimeFrom = dateTimeFrom;
    }

    public Long getDateTimeTo() {
        return dateTimeTo;
    }

    public void setDateTimeTo(Long dateTimeTo) {
        this.dateTimeTo = dateTimeTo;
    }

    public Set<Integer> getWeeklyoff() {
        return weeklyoff;
    }

    public void setWeeklyoff(Set<Integer> weeklyoff) {
        this.weeklyoff = weeklyoff;
    }

    public Set<Long> getHolidays() {
        return holidays;
    }

    public void setHolidays(Set<Long> holidays) {
        this.holidays = holidays;
    }

    public Long getAvailabilityid() {
        return availabilityid;
    }

    public void setAvailabilityid(Long availabilityid) {
        this.availabilityid = availabilityid;
    }

    public ServiceProviderEntity getMyserviceprovider() {
        return myserviceprovider;
    }

    public void setMyserviceprovider(ServiceProviderEntity myserviceprovider) {
        this.myserviceprovider = myserviceprovider;
    }

    public LocationEntity getMylocation() {
        return mylocation;
    }

    public void setMylocation(LocationEntity mylocation) {
        this.mylocation = mylocation;
    }

    public Integer getMeetingTimeInMins() {
        return meetingTimeInMins;
    }

    public void setMeetingTimeInMins(Integer meetingTimeInMins) {
        this.meetingTimeInMins = meetingTimeInMins;
    }

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property="appointmentid", scope=AppointmentEntity.class)
    public Set<AppointmentEntity> getMyappointments() {
        return myappointments;
    }

    public void setMyappointments(Set<AppointmentEntity> myappointments) {
        this.myappointments = myappointments;
    }

    public void addToMyappointments(AppointmentEntity myappointment) {
        myappointment.setFromAvailability(this);
        this.myappointments.add(myappointment);
    }
    public void delFromyappointments(AppointmentEntity myappointment) {
        myappointment.setFromAvailability(null);
        this.myappointments.remove(myappointment);
    }

    public Integer getScheduleForNumDays() {
        return scheduleForNumDays;
    }

    public void setScheduleForNumDays(Integer scheduleForNumDays) {
        this.scheduleForNumDays = scheduleForNumDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AvailabilityEntity)) return false;

        AvailabilityEntity that = (AvailabilityEntity) o;

        if (availabilityid != null ? !availabilityid.equals(that.availabilityid) : that.availabilityid != null)
            return false;
        if (dateTimeFrom != null ? !dateTimeFrom.equals(that.dateTimeFrom) : that.dateTimeFrom != null) return false;
        if (dateTimeTo != null ? !dateTimeTo.equals(that.dateTimeTo) : that.dateTimeTo != null) return false;
        if (weeklyoff != null ? !weeklyoff.equals(that.weeklyoff) : that.weeklyoff != null) return false;
        if (holidays != null ? !holidays.equals(that.holidays) : that.holidays != null) return false;
        if (meetingTimeInMins != null ? !meetingTimeInMins.equals(that.meetingTimeInMins) : that.meetingTimeInMins != null) return false;
        if (scheduleForNumDays != null ? !scheduleForNumDays.equals(that.scheduleForNumDays) : that.scheduleForNumDays != null) return false;
        if (myserviceprovider != null ? !myserviceprovider.equals(that.myserviceprovider) : that.myserviceprovider != null)
            return false;
        return mylocation != null ? mylocation.equals(that.mylocation) : that.mylocation == null;

    }

    @Override
    public int hashCode() {
        int result = availabilityid != null ? availabilityid.hashCode() : 0;
        result = 31 * result + (dateTimeFrom != null ? dateTimeFrom.hashCode() : 0);
        result = 31 * result + (dateTimeTo != null ? dateTimeTo.hashCode() : 0);
        result = 31 * result + (weeklyoff != null ? weeklyoff.hashCode() : 0);
        result = 31 * result + (holidays != null ? holidays.hashCode() : 0);
        result = 31 * result + (myserviceprovider != null ? myserviceprovider.hashCode() : 0);
        result = 31 * result + (mylocation != null ? mylocation.hashCode() : 0);
        result = 31 * result + (meetingTimeInMins != null ? meetingTimeInMins.hashCode() : 0);
        result = 31 * result + (scheduleForNumDays != null ? scheduleForNumDays.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AvailabilityEntity{" +
                "version=" + version +
                ", availabilityid=" + availabilityid +
                ", dateTimeFrom=" + dateTimeFrom +
                ", dateTimeTo=" + dateTimeTo +
                ", weeklyoff=" + weeklyoff +
                ", holidays=" + holidays +
                ", myserviceprovider=" + myserviceprovider +
                ", mylocation=" + mylocation +
                ", meetingTimeInMins=" + meetingTimeInMins +
                ", scheduleForNumDays=" + scheduleForNumDays +
                '}';
    }
}
