package com.cps.sprm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="customer",
        uniqueConstraints = @UniqueConstraint(columnNames = {"customerphonenumber", "email"}))
public class CustomerEntity implements Serializable
{

    @Version
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="VERSION")
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="customerid")
    private Long customerid;


    @org.hibernate.validator.constraints.NotEmpty
    @Column(name="customerphonenumber")
    private String customerphonenumber;

    @Column(name="name")
    @org.hibernate.validator.constraints.NotEmpty
    private String name;

    @Column(name="email")
    @org.hibernate.validator.constraints.Email
    private String email;

    //TODO Use DATE datetype for this ?
    @Column(name="dateofbirth")
    @Temporal(TemporalType.DATE)
    private Date dob;

    @Column(name="address")
    private String address;

    @OneToMany(mappedBy = "myCustomer", cascade = {CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<AppointmentEntity> myappointments = new HashSet<AppointmentEntity>(0);

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinTable(name="customer_serviceprovider",
            joinColumns={@JoinColumn(name="customerid")},
            inverseJoinColumns={@JoinColumn(name="serviceproviderid")})
    private Set<ServiceProviderEntity> myfavourites = new HashSet<ServiceProviderEntity>(0);

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property="serviceproviderid", scope=ServiceProviderEntity.class)
    public Set<ServiceProviderEntity> getMyfavourites() {
        return myfavourites;
    }

    public void setMyfavourites(Set<ServiceProviderEntity> myfavourites) {
        this.myfavourites = myfavourites;
    }

    public void delFromfavourites(ServiceProviderEntity serviceProviderEntity) {
        this.myfavourites.remove(serviceProviderEntity);
    }

    public void addToMyfavourites(ServiceProviderEntity serviceProviderEntity) {
        this.myfavourites.add(serviceProviderEntity);
    }

    public String getCustomerphonenumber() {
		return customerphonenumber;
	}

	public void setCustomerphonenumber(String customerphonenumber) {
		this.customerphonenumber = customerphonenumber;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Long customerid) {
        this.customerid = customerid;
    }

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property="appointmentid", scope=AppointmentEntity.class)
    public Set<AppointmentEntity> getMyappointments() {
        return myappointments;
    }

    public void setMyappointments(Set<AppointmentEntity> myappointments) {
        this.myappointments = myappointments;
    }

    public void addToMyappointments(AppointmentEntity myappointment) {
        myappointment.setMyCustomer(this);
        this.myappointments.add(myappointment);
    }
    public void delFromyappointments(AppointmentEntity myappointment) {
        myappointment.setMyCustomer(null);
        this.myappointments.remove(myappointment);
    }

    @Override
    public String toString() {
        return "CustomerEntity{" +
                "version=" + version +
                ", customerid=" + customerid +
                ", customerphonenumber='" + customerphonenumber + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", dob=" + dob +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerEntity)) return false;

        CustomerEntity that = (CustomerEntity) o;

        if (!customerphonenumber.equals(that.customerphonenumber)) return false;
        return email.equals(that.email);

    }

    @Override
    public int hashCode() {
        int result = customerphonenumber.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
