package com.cps.sprm.entity;

import com.cps.sprm.sdk.enums.AppointmentStatus;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

/**
 * Created by sumeshs on 12/28/2016.
 */
/*@NamedQueries({
        @NamedQuery(
                name = "findAppointmentForCancelBySP",
                query = "FROM AppointmentEntity AS apt, LocationEntity AS loc, ServiceProviderEntity AS sp WHERE " +
                        "sp.serviceproviderid = apt.myServiceProvider.serviceproviderid AND " +
                        "apt.timeslot BETWEEN :starttime AND :endtime " +
                        "AND sp.serviceproviderid = :spid"
        )
})*/
@Entity
@Table(name="appointment")
public class AppointmentEntity {

    @Version
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="VERSION")
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="appointmentid")
    private Long appointmentid;

    @Column(name="tokennumber")
    private Integer tokenNumber;

    @Column(name="timeslot")
    private Long timeSlot;

    @Column(name="isfirsttime")
    private Boolean isFirstTime;

    @Column(name="custompatientid")
    private String patientIdInHospital;

    @Column(name="notes", length = 1024)
    private String notes;

    @Column(name="status")
    private AppointmentStatus appointmentStatus = AppointmentStatus.FREE;

    @ManyToOne
    @JoinColumn(name="customerid")
    private CustomerEntity myCustomer;

    @ManyToOne
    @JoinColumn(name="locationid")
    private LocationEntity myLocation;

    @ManyToOne
    @JoinColumn(name="serviceproviderid")
    private ServiceProviderEntity myServiceProvider;

    @ManyToOne
    @JoinColumn(name="availabilityid")
    private AvailabilityEntity fromAvailability;

    public Long getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(Long appointmentid) {
        this.appointmentid = appointmentid;
    }

    public Integer getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(Integer tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public Long getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Long timeSlot) {
        this.timeSlot = timeSlot;
    }

    public Boolean getFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(Boolean firstTime) {
        isFirstTime = firstTime;
    }

    public String getPatientIdInHospital() {
        return patientIdInHospital;
    }

    public void setPatientIdInHospital(String patientIdInHospital) {
        this.patientIdInHospital = patientIdInHospital;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public CustomerEntity getMyCustomer() {
        return myCustomer;
    }

    public void setMyCustomer(CustomerEntity myCustomer) {
        this.myCustomer = myCustomer;
    }

    public LocationEntity getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(LocationEntity myLocation) {
        this.myLocation = myLocation;
    }

    public ServiceProviderEntity getMyServiceProvider() {
        return myServiceProvider;
    }

    public void setMyServiceProvider(ServiceProviderEntity myServiceProvider) {
        this.myServiceProvider = myServiceProvider;
    }

    public AvailabilityEntity getFromAvailability() {
        return fromAvailability;
    }

    public void setFromAvailability(AvailabilityEntity fromAvailability) {
        this.fromAvailability = fromAvailability;
    }

    public AppointmentEntity() {
    }

    public AppointmentEntity(Integer tokenNumber, Long timeSlot) {
        this.tokenNumber = tokenNumber;
        this.timeSlot = timeSlot;
    }

    @Override
    public String toString() {
        return "AppointmentEntity{" +
                "version=" + version +
                ", appointmentid=" + appointmentid +
                ", tokenNumber=" + tokenNumber +
                ", timeSlot=" + timeSlot +
                ", isFirstTime=" + isFirstTime +
                ", patientIdInHospital='" + patientIdInHospital + '\'' +
                ", notes='" + notes + '\'' +
                ", appointmentStatus=" + appointmentStatus +
                ", myCustomer=" + myCustomer +
                ", myLocation=" + myLocation +
                ", myServiceProvider=" + myServiceProvider +
                ", fromAvailability=" + fromAvailability +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AppointmentEntity)) return false;

        AppointmentEntity that = (AppointmentEntity) o;

        if (tokenNumber != null ? !tokenNumber.equals(that.tokenNumber) : that.tokenNumber != null) return false;
        if (timeSlot != null ? !timeSlot.equals(that.timeSlot) : that.timeSlot != null) return false;
        if (myCustomer != null ? !myCustomer.equals(that.myCustomer) : that.myCustomer != null) return false;
        return myServiceProvider != null ? myServiceProvider.equals(that.myServiceProvider) : that.myServiceProvider == null;

    }

    @Override
    public int hashCode() {
        int result = tokenNumber != null ? tokenNumber.hashCode() : 0;
        result = 31 * result + (timeSlot != null ? timeSlot.hashCode() : 0);
        result = 31 * result + (myCustomer != null ? myCustomer.hashCode() : 0);
        result = 31 * result + (myServiceProvider != null ? myServiceProvider.hashCode() : 0);
        return result;
    }
}
