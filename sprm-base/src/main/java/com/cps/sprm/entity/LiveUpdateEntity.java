package com.cps.sprm.entity;


import javax.persistence.*;

/**
 * Created by sumeshs on 1/21/2017.
 */
@Entity
@Table(name="liveupdate")
public class LiveUpdateEntity {

    @Version
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="VERSION")
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="liveupdateid")
    private Long liveupdateid;

    @OneToOne(mappedBy = "liveupdate")
    private ServiceProviderEntity myServiceProvider;

    @OneToOne(mappedBy = "liveupdate")
    private LocationEntity myLocation;

    @Column(name="avgconsultingtime")
    private Float avgConsultingTime;

    @Column(name="day")
    private Long todayDate;

    @Column(name="totaltokens")
    private Integer totalTokens;

    @Column(name="currenttoken")
    private Integer currentToken;

    @Column(name="consultingstartime")
    private Long consultingStartTime;

    @Column(name="consultingendtime")
    private Long consultingEndTime;

    public Long getLiveupdateid() {
        return liveupdateid;
    }

    public void setLiveupdateid(Long liveupdateid) {
        this.liveupdateid = liveupdateid;
    }

    public ServiceProviderEntity getMyServiceProvider() {
        return myServiceProvider;
    }

    public void setMyServiceProvider(ServiceProviderEntity myServiceProvider) {
        this.myServiceProvider = myServiceProvider;
    }

    public LocationEntity getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(LocationEntity myLocation) {
        this.myLocation = myLocation;
    }

    public Float getAvgConsultingTime() {
        return avgConsultingTime;
    }

    public void setAvgConsultingTime(Float avgConsultingTime) {
        this.avgConsultingTime = avgConsultingTime;
    }

    public Integer getTotalTokens() {
        return totalTokens;
    }

    public void setTotalTokens(Integer totalTokens) {
        this.totalTokens = totalTokens;
    }

    public Integer getCurrentToken() {
        return currentToken;
    }

    public void setCurrentToken(Integer currentToken) {
        this.currentToken = currentToken;
    }

    public Long getConsultingStartTime() {
        return consultingStartTime;
    }

    public void setConsultingStartTime(Long consultingStartTime) {
        this.consultingStartTime = consultingStartTime;
    }

    public Long getConsultingEndTime() {
        return consultingEndTime;
    }

    public void setConsultingEndTime(Long consultingEndTime) {
        this.consultingEndTime = consultingEndTime;
    }

    public Long getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(Long todayDate) {
        this.todayDate = todayDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LiveUpdateEntity)) return false;

        LiveUpdateEntity that = (LiveUpdateEntity) o;

        if (myServiceProvider != null ? !myServiceProvider.equals(that.myServiceProvider) : that.myServiceProvider != null)
            return false;
        if (myLocation != null ? !myLocation.equals(that.myLocation) : that.myLocation != null) return false;
        return todayDate != null ? todayDate.equals(that.todayDate) : that.todayDate == null;

    }

    @Override
    public int hashCode() {
        int result = myServiceProvider != null ? myServiceProvider.hashCode() : 0;
        result = 31 * result + (myLocation != null ? myLocation.hashCode() : 0);
        result = 31 * result + (todayDate != null ? todayDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LiveUpdateEntity{" +
                "liveupdateid=" + liveupdateid +
                ", myServiceProvider=" + myServiceProvider +
                ", myLocation=" + myLocation +
                ", avgConsultingTime=" + avgConsultingTime +
                ", totalTokens=" + totalTokens +
                ", currentToken=" + currentToken +
                ", consultingEndTime=" + consultingEndTime +
                ", consultingStartTime=" + consultingStartTime +
                ", todayDate =" + todayDate +
                '}';
    }
}
