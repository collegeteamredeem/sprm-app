package com.cps.sprm.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="location",
		uniqueConstraints = @UniqueConstraint(columnNames = {"name", "pincode"}))
public class LocationEntity implements Serializable
{
	@Version
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="VERSION")
	private long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="locationid")
	private Long locationid;

	@Column(name="fulladdress")
	private String fulladdress;

	@org.hibernate.validator.constraints.NotEmpty
	@Column(name="name")
	private String name;

	@org.hibernate.validator.constraints.NotEmpty
	@Column(name="pincode")
	private String pincode;

	@org.hibernate.validator.constraints.NotEmpty
	@Column(name="city")
	private String city;

	@Column(name="latitude")
	private double lat;

	@Column(name="longitude")
	private double lon;

	@OneToMany(mappedBy = "mylocation", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private Set<AvailabilityEntity> myavailability = new HashSet<AvailabilityEntity>(0);

    @OneToMany(mappedBy = "myLocation", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Set<AppointmentEntity> myappointments = new HashSet<AppointmentEntity>(0);

	@ManyToMany(fetch = FetchType.LAZY, mappedBy="locations")
	private Set<ServiceProviderEntity> serviceProviders =
			new HashSet<ServiceProviderEntity>(0);

	@OneToOne
	private LiveUpdateEntity liveupdate;

	public Long getLocationid() {
		return locationid;
	}

	public void setLocationid(Long locationId) {
		this.locationid = locationId;
	}

	public String getFulladdress() {
		return fulladdress;
	}

	public void setFulladdress(String fulladdress) {
		this.fulladdress = fulladdress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
			property="serviceproviderid", scope=ServiceProviderEntity.class)
	public Set<ServiceProviderEntity> getServiceProviders() {
		return serviceProviders;
	}

	public void setServiceProviders(Set<ServiceProviderEntity> serviceProviders) {
		this.serviceProviders = serviceProviders;
	}

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
			property="availabilityid", scope=AvailabilityEntity.class)
	public Set<AvailabilityEntity> getMyavailability() {
		return myavailability;
	}

	public void setMyavailability(Set<AvailabilityEntity> myavailability) {
		this.myavailability = myavailability;
	}

	public void addToMyavailability(AvailabilityEntity availability) {
		availability.setMylocation(this);
		this.myavailability.add(availability);
	}
	public void delFromMyavailability(AvailabilityEntity availability) {
		availability.setMylocation(null);
		this.myavailability.remove(availability);
	}


    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property="appointmentid", scope=AppointmentEntity.class)
    public Set<AppointmentEntity> getMyappointments() {
        return myappointments;
    }

    public void setMyappointments(Set<AppointmentEntity> myappointments) {
        this.myappointments = myappointments;
    }

    public void addToMyappointments(AppointmentEntity myappointment) {
        myappointment.setMyLocation(this);
        this.myappointments.add(myappointment);
    }
    public void delFromyappointments(AppointmentEntity myappointment) {
        myappointment.setMyLocation(null);
        this.myappointments.remove(myappointment);
    }

    public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public LiveUpdateEntity getLiveupdate() {
		return liveupdate;
	}

	public void setLiveupdate(LiveUpdateEntity liveupdate) {
		this.liveupdate = liveupdate;
	}

	@Override
	public String toString() {
		return "LocationEntity{" +
				"version=" + version +
				", locationid=" + locationid +
				", fulladdress='" + fulladdress + '\'' +
				", name='" + name + '\'' +
				", pincode='" + pincode + '\'' +
				", city='" + city + '\'' +
				", lat=" + lat +
				", lon=" + lon +
				'}';
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationEntity)) return false;

        LocationEntity that = (LocationEntity) o;

        if (!name.equals(that.name)) return false;
        if (!pincode.equals(that.pincode)) return false;
        return city.equals(that.city);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + pincode.hashCode();
        result = 31 * result + city.hashCode();
        return result;
    }
}

