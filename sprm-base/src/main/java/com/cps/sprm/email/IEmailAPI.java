package com.cps.sprm.email;

/**
 * Created by sumeshs on 2/13/2017.
 */
public interface IEmailAPI {
     void sendEmail(String toAddr, String subject, String body);
}
