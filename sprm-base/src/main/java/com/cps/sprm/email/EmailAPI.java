package com.cps.sprm.email;

import com.cps.sprm.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sumeshs on 2/13/2017.
 */
@Service(value="emailAPIServiceImpl")
@Transactional
public class EmailAPI implements IEmailAPI{

    private static final Logger logger = Logger.getLogger(EmailAPI.class);

    @Autowired
    private MailSender mailSender; // MailSender interface defines a strategy

    private void readyMessageToSendEmail(String toAddress, String fromAddress, String subject, String msgBody) {

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(fromAddress);
        simpleMailMessage.setTo(toAddress);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(msgBody);
        mailSender.send(simpleMailMessage);
    }

    public void sendEmail(String toAddr, String subject, String body) {
        if(System.getProperty(Constants.SEND_EMAIL_SWITCH) != null) {

            logger.debug("Sending email");
           // String toAddr = "sumeshnair82@gmail.com";
            String fromAddr = System.getProperty(Constants.EMAIL_FROM_ADDR);
         //   String fromAddr = "appointeasy@gmail.com";

           /* // email subject
            String subject = "Hello Its a test mail";

            // email body
            String body = "There you go.. You got an email.. This is how the welcome mail looks";*/
            readyMessageToSendEmail(toAddr, fromAddr, subject, body);
        }else {
            System.out.println("Not sending mail as Property not set");
        }
    }
}
