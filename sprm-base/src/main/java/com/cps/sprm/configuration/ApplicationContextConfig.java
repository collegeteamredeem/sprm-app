package com.cps.sprm.configuration;

/**
 * Created by sumeshs on 2/10/2017.
 */


import com.cps.sprm.dao.*;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@EnableCaching
@ComponentScan(basePackages = "com.cps.sprm")
public class ApplicationContextConfig {

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/project");
        dataSource.setUsername("postgres");
        dataSource.setPassword("password");

        return dataSource;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) {

        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);

        sessionBuilder.scanPackages("com.cps.sprm.entity");
        sessionBuilder.addProperties(getHibernateProperties());

        return sessionBuilder.buildSessionFactory();
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL81Dialect");
        return properties;
    }

    @Autowired
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(
            SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(
                sessionFactory);

        return transactionManager;
    }

    @Autowired
    @Bean(name = "mailSender")
    public JavaMailSenderImpl getJavaMailSender(){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername("appointeasy@gmail.com");
        mailSender.setPassword("Finally5555$$$$");
        mailSender.setJavaMailProperties(getJavaMailProperties());
        return mailSender;
    }

    private Properties getJavaMailProperties() {
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        return properties;
    }

    @Autowired
    @Bean(name = "serviceProviderDao")
    public ServiceProviderDao getServiceProviderDao(SessionFactory sessionFactory) {
        return new ServiceProviderDaoImpl(sessionFactory);
    }

    @Autowired
    @Bean(name = "locationDao")
    public LocationDao getLocationDao(SessionFactory sessionFactory) {
        return new LocationDaoImpl(sessionFactory);
    }

    @Autowired
    @Bean(name = "liveUpdateDao")
    public LiveUpdateDao getLiveUpdateDao(SessionFactory sessionFactory) {
        return new LiveUpdateDaoImpl(sessionFactory);
    }

    @Autowired
    @Bean(name = "customerDao")
    public CustomerDao getCustomerDao(SessionFactory sessionFactory) {
        return new CustomerDaoImpl(sessionFactory);
    }

    @Autowired
    @Bean(name = "appointmentDao")
    public AppointmentDao getAppointmentDao(SessionFactory sessionFactory) {
        return new AppointmentDaoImpl(sessionFactory);
    }

    @Autowired
    @Bean(name = "cacheManager")
    public SimpleCacheManager getCacheManager() {
        SimpleCacheManager manager = new SimpleCacheManager();
        CaffeineCache liveupdatesCache = buildCache("liveupdates", 60);
        CaffeineCache otpCache = buildCache("otp",  10);
        manager.setCaches(Arrays.asList(liveupdatesCache, otpCache));
 /*       caffeineCacheManager.setCacheNames(Arrays.asList("liveupdates", "otp"));
        caffeineCacheManager.setCaffeineSpec(com.github.benmanes.caffeine.cache.CaffeineSpec.parse("expireAfterAccess=60m"));
        return caffeineCacheManager;
*/
        return manager;
    }

    private CaffeineCache buildCache(String name, int minutesToExpire) {
        return new CaffeineCache(name, Caffeine.newBuilder()
                .expireAfterWrite(minutesToExpire, TimeUnit.MINUTES)
                .build());
    }
}