package com.cps.sprm.configuration;

/**
 * Created by sumeshs on 2/10/2017.
 */

import com.cps.sprm.util.Constants;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class SpringWebAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        appContext.register(ApplicationContextConfig.class);

        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
                "SpringDispatcher", new DispatcherServlet(appContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");

        setUpProperties();

    }

    private void setUpProperties(){
     //   System.setProperty(Constants.SEND_EMAIL_SWITCH, "TRUE");
        System.setProperty(Constants.EMAIL_FROM_ADDR, "appointeasy@gmail.com");

    //    System.setProperty(Constants.SEND_SMS_SWITCH, "TRUE");
        System.setProperty(Constants.SMS_USERNAME, "sumesh.s@rediffmail.com");
        System.setProperty(Constants.SMS_HASH, "1bb26ea00c556cc4632a226f41fd8a0e1d1a6bb2"); // temp hash from textlocal
        System.setProperty(Constants.SMS_SENDER_ID, "TXTLCL");
        System.setProperty(Constants.SMS_HTTP_URL, "https://api.textlocal.in/send/?");
        System.setProperty(Constants.SMS_TEST_ONLY, "true"); // set it to false in prod
    }
}
