package com.cps.sprm.sms;

/**
 * Created by sumeshs on 1/11/2017.
 */
//Temp placeHolder, use backend task infra for this
public interface ISmsProvider {
    String sendSms(String messageStr, String numbersList);
}
