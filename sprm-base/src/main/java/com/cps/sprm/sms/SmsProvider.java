package com.cps.sprm.sms;

import com.cps.sprm.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by sumeshs on 1/11/2017.
 */
@Transactional
@Service(value="smsAPIServiceImpl")
public class SmsProvider implements ISmsProvider {

    private static final Logger logger = Logger.getLogger(SmsProvider.class);

    @Override
    public String sendSms(String messageStr, String numbersList) {

        if(System.getProperty(Constants.SEND_SMS_SWITCH) != null) {
            try {
                // Construct data
                String userName = System.getProperty(Constants.SMS_USERNAME);
                String hashStr = System.getProperty(Constants.SMS_HASH);
                String smsSenderId = System.getProperty(Constants.SMS_SENDER_ID);
                String smsHttpUrl = System.getProperty(Constants.SMS_HTTP_URL);
                String testStr = System.getProperty(Constants.SMS_TEST_ONLY);

               /* String user = "username=" + "sumesh.s@rediffmail.com";
                String hash = "&hash=" + "1bb26ea00c556cc4632a226f41fd8a0e1d1a6bb2";
                // String apiKey = "&apiKey=" + "41K4QrX4ZCQ-2zksTbw9MxOmTzqTMazEioWAOCa3Bl";
                String message = "&message=" + "This is your message";
                String sender = "&sender=" + "TXTLCL";
                String numbers = "&numbers=" + "9035740365";
                String test = "&test=" + "true";*/

                String user = "username=" + userName;
                String hash = "&hash=" + hashStr;
                String message = "&message=" + messageStr;
                String sender = "&sender=" + smsSenderId;
                String numbers = "&numbers=" + numbersList;
                String test = "&test=" + testStr;

                // Send data
               // HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
                HttpURLConnection conn = (HttpURLConnection) new URL(smsHttpUrl).openConnection();
                String data = user + hash + numbers + message + sender + test;
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                conn.getOutputStream().write(data.getBytes("UTF-8"));
                final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                final StringBuffer stringBuffer = new StringBuffer();
                String line;
                while ((line = rd.readLine()) != null) {
                    stringBuffer.append(line);
                }
                rd.close();

                return stringBuffer.toString();
            } catch (Exception e) {
                return "Error " + e;
            }
        }else {
           logger.debug("The SMS toggle switch turned off, cannot send SMS");
        }

        return "SMS Switch not set";
    }
}
