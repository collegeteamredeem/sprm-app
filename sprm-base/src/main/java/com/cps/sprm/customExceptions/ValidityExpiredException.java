package com.cps.sprm.customExceptions;

/**
 * Created by sumeshs on 1/20/2017.
 */
public class ValidityExpiredException extends Exception {
    public ValidityExpiredException(String message) {
        super(message);
    }
}
