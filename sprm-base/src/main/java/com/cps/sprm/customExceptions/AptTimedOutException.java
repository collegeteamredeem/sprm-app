package com.cps.sprm.customExceptions;

/**
 * Created by sumeshs on 1/6/2017.
 */
public class AptTimedOutException extends Exception {

    public AptTimedOutException(String message) {
        super(message);
    }
}
