package com.cps.sprm.customExceptions;

/**
 * Created by sumeshs on 1/6/2017.
 */
public class AptCannotBeCancelledException extends Exception {

    public AptCannotBeCancelledException(String message) {
        super(message);
    }
}
