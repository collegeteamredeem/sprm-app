package com.cps.sprm.customExceptions;

/**
 * Created by sumeshs on 1/6/2017.
 */
public class AptNotOwnedException extends Exception {
    public AptNotOwnedException(String message) {
        super(message);
    }
}
