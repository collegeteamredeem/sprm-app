package com.cps.sprm.customExceptions;

/**
 * Created by sumeshs on 1/6/2017.
 */
public class AptNotFreeException extends Exception {

    public AptNotFreeException(String message) {
        super(message);
    }
}
