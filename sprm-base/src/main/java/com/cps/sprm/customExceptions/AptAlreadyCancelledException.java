package com.cps.sprm.customExceptions;

/**
 * Created by sumeshs on 1/6/2017.
 */
public class AptAlreadyCancelledException extends Exception {
    public AptAlreadyCancelledException(String message) {
        super(message);
    }
}
