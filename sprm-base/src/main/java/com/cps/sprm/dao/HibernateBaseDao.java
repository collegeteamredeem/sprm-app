package com.cps.sprm.dao;

import org.dozer.DozerBeanMapper;

/**
 * Created by sumeshs on 1/21/2017.
 */
public interface HibernateBaseDao {

    void setMapper(DozerBeanMapper mapper);

}
