package com.cps.sprm.dao;

import com.cps.sprm.entity.AvailabilityEntity;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.sdk.dto.LocationV1;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sumeshs on 12/28/2016.
 */
@Repository
public class LocationDaoImpl implements LocationDao {

    private static final Logger logger = Logger.getLogger(LocationDaoImpl.class);

    DozerBeanMapper mapper;

    @Autowired
    private SessionFactory sessionFactory;

    public LocationDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void setMapper(DozerBeanMapper mapper) {
        this.mapper = mapper;
    }


    @Override
    public Long save(LocationEntity locationEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO save called for location " + locationEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            session.save(locationEntity);
            return locationEntity.getLocationid();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }

    }

    @Override
    public void update(Long id, LocationEntity locationEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO update called for location " + locationEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            LocationEntity dbLocationEntity  = session.get(LocationEntity.class, id);
            if (dbLocationEntity == null)
            {
                throw new Exception("Location not found for given Id");
            }
            copyLocationData(locationEntity, dbLocationEntity);
            LocationEntity updatedLocation = (LocationEntity)session.merge(dbLocationEntity);
            session.update(updatedLocation);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public void delete(LocationEntity locationEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO delete called for location " + locationEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            LocationEntity entityToDelete = session.load(LocationEntity.class, locationEntity.getLocationid());
            session.delete(entityToDelete);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public List<LocationEntity> list() throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO list called for location ");
        }
        List<LocationEntity> locationEntityList = new ArrayList<LocationEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            locationEntityList = session.createQuery("from LocationEntity").list();
            return locationEntityList;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Override
    public List<LocationV1> listGetDto() throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO list called for location ");
        }
        List<LocationEntity> locationEntityList = new ArrayList<LocationEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            locationEntityList = session.createQuery("from LocationEntity").list();
            List<LocationV1> locationV1s = new ArrayList<LocationV1>();
            for(LocationEntity locationEntity : locationEntityList)
            {
                locationV1s.add(mapper.map(locationEntity, LocationV1.class));
            }
            return locationV1s;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Override
    public LocationEntity listById(Long id) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO listBy Id for location " + id);
        }
        LocationEntity locationEntity = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            locationEntity = session.get(LocationEntity.class, id);
            return locationEntity;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public Set<AvailabilityEntity> getAvailabilityForLocation(Long id) throws Exception {
        LocationEntity locationEntity = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            locationEntity = session.get(LocationEntity.class, id);
            Set<AvailabilityEntity> tempAvailabilitySet =  new HashSet<AvailabilityEntity>();
            tempAvailabilitySet.addAll(locationEntity.getMyavailability());
            return tempAvailabilitySet;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }
    @Override
    public LocationV1 listByIdGetDto(Long id) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO listBy Id for location " + id);
        }
        LocationEntity locationEntity = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            locationEntity = session.get(LocationEntity.class, id);
            LocationV1 locationV1 = mapper.map(locationEntity, LocationV1.class);
            return locationV1;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    private void copyLocationData(
            LocationEntity updatedLocationObject,
            LocationEntity dbLocationObject)
    {
        if (updatedLocationObject.getCity() != null && !updatedLocationObject.getCity().trim().isEmpty())
        {
            dbLocationObject.setCity(updatedLocationObject.getCity());
        }
        if (updatedLocationObject.getName() != null && !updatedLocationObject.getName().trim().isEmpty())
        {
            dbLocationObject.setName(updatedLocationObject.getName());
        }

        if (updatedLocationObject.getFulladdress() != null &&
                !updatedLocationObject.getFulladdress().trim().isEmpty())
        {
            dbLocationObject.setFulladdress(updatedLocationObject.getFulladdress());
        }
        if (updatedLocationObject.getPincode() != null &&
                !updatedLocationObject.getPincode().trim().isEmpty())
        {
            dbLocationObject.setPincode(updatedLocationObject.getPincode());
        }

        if (updatedLocationObject.getLat() != dbLocationObject.getLat())
        {
            dbLocationObject.setLat(updatedLocationObject.getLat());
        }

        if (updatedLocationObject.getLon() != dbLocationObject.getLon())
        {
            dbLocationObject.setLon(updatedLocationObject.getLon());
        }

    }
}
