package com.cps.sprm.dao;

import com.cps.sprm.entity.AvailabilityEntity;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.sdk.dto.LocationV1;

import java.util.List;
import java.util.Set;

/**
 * Created by sumeshs on 12/28/2016.
 */
public interface LocationDao extends HibernateBaseDao{

    Long save(LocationEntity locationEntity) throws Exception;

    void update(Long id, LocationEntity locationEntity) throws Exception;

    void delete(LocationEntity locationEntity) throws Exception;

    List<LocationEntity> list() throws Exception;

    List<LocationV1> listGetDto() throws Exception;

    Set<AvailabilityEntity> getAvailabilityForLocation(Long id) throws Exception;

    LocationEntity listById(Long id) throws Exception;

    LocationV1 listByIdGetDto(Long id) throws Exception;
}
