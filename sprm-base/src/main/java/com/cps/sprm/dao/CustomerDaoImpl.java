package com.cps.sprm.dao;

import com.cps.sprm.entity.CustomerEntity;
import com.cps.sprm.entity.ServiceProviderEntity;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumeshs on 11/12/2016.
 */
@Repository
public class CustomerDaoImpl implements CustomerDao {

    private static final Logger logger = Logger.getLogger(CustomerDaoImpl.class);

    DozerBeanMapper mapper;

    @Autowired
    private SessionFactory sessionFactory;

    public CustomerDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void setMapper(DozerBeanMapper mapper) {
        this.mapper = mapper;
    }

    public void save(CustomerEntity customerEntity) throws Exception{
        // TODO : Singleton or Cache
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            session.save(customerEntity);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    public void update(Long id, CustomerEntity customerEntity) throws Exception{
        // TODO : Singleton or Cache
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            CustomerEntity dbCustomerEntity  = session.get(CustomerEntity.class, id);
            if (dbCustomerEntity == null)
            {
                throw new Exception("Customer not found for given Id");
            }
            copyCustomerData(customerEntity, dbCustomerEntity);
            CustomerEntity updatedCustomer = (CustomerEntity)session.merge(dbCustomerEntity);
            session.update(updatedCustomer);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    private void copyCustomerData(
            CustomerEntity updatedCustomerObject,
            CustomerEntity dbCustomerObject)
    {
        if (updatedCustomerObject.getEmail() != null && !updatedCustomerObject.getEmail().trim().isEmpty())
        {
            dbCustomerObject.setEmail(updatedCustomerObject.getEmail());
        }
        if (updatedCustomerObject.getName() != null && !updatedCustomerObject.getName().trim().isEmpty())
        {
            dbCustomerObject.setName(updatedCustomerObject.getName());
        }

        if (updatedCustomerObject.getCustomerphonenumber() != null &&
                !updatedCustomerObject.getCustomerphonenumber().trim().isEmpty())
        {
            dbCustomerObject.setCustomerphonenumber(updatedCustomerObject.getCustomerphonenumber());
        }
        if (updatedCustomerObject.getAddress() != null &&
                !updatedCustomerObject.getAddress().trim().isEmpty())
        {
            dbCustomerObject.setAddress(updatedCustomerObject.getAddress());
        }

        if (updatedCustomerObject.getDob() != null)
        {
            dbCustomerObject.setDob(updatedCustomerObject.getDob());
        }
    }

    public void delete(CustomerEntity customerEntity) throws Exception{
        // TODO : Singleton or Cache
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            CustomerEntity entityToDelete = session.load(CustomerEntity.class, customerEntity.getCustomerid());
            entityToDelete.getMyappointments().clear();
            entityToDelete.getMyfavourites().clear();
            //session.save(entityToDelete);
            session.delete(entityToDelete);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }

    }

    public List<CustomerEntity> list() throws Exception
    {
        List<CustomerEntity> customerEntityList = new ArrayList<CustomerEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            customerEntityList = session.createQuery("from CustomerEntity").list();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }

        return customerEntityList;
    }


    public CustomerEntity listById(Long id) throws Exception
    {
        CustomerEntity customerEntity = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            customerEntity = session.get(CustomerEntity.class, id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }

        return customerEntity;
    }

    @Override
    public CustomerEntity listByPhoneNumber(String phoneNumber) throws Exception {
            String hql = "from CustomerEntity where customerphonenumber=:phoneNumber";
            Query query = sessionFactory.getCurrentSession().createQuery(hql);
            query.setParameter("phoneNumber", phoneNumber);
            @SuppressWarnings("unchecked")
            List<CustomerEntity> listCustomer = (List<CustomerEntity>) query.list();

            if (listCustomer != null && !listCustomer.isEmpty()) {
                return listCustomer.get(0);
            }

            return null;
    }

    public void addToFavourites(Long id, ServiceProviderEntity entity) throws Exception{

        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            CustomerEntity updatedCustomer = listById(id);
            if(null != updatedCustomer && null != entity){
                updatedCustomer.addToMyfavourites(entity);
            }
            session.update(updatedCustomer);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }

    }

    public void delFromFavourites(Long id, ServiceProviderEntity entity) throws Exception{
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            CustomerEntity updatedCustomer = listById(id);
            if(null != updatedCustomer && null != entity){
                updatedCustomer.delFromfavourites(entity);
            }
            session.update(updatedCustomer);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }

    }
}
