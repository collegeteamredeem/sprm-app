package com.cps.sprm.dao;

import com.cps.sprm.entity.AppointmentEntity;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumeshs on 12/29/2016.
 */
@Repository
public class AppointmentDaoImpl implements AppointmentDao {
    
    private static final Logger logger = Logger.getLogger(AppointmentDaoImpl.class);

    DozerBeanMapper mapper;

    @Override
    public void setMapper(DozerBeanMapper mapper){
        this.mapper = mapper;
    }

    @Autowired
    private SessionFactory sessionFactory;

    public AppointmentDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    
    @Override
    public void save(AppointmentEntity appointmentEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO save called for appointment " + appointmentEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            session.save(appointmentEntity);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public AppointmentV2 saveAndReturnDto(AppointmentEntity appointmentEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO save called for appointment " + appointmentEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            session.save(appointmentEntity);
            AppointmentV2 persistedAppointment = mapper.map(appointmentEntity, AppointmentV2.class);
            return persistedAppointment;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public void update(Long id, AppointmentEntity appointmentEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO update called for appointment " + appointmentEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            AppointmentEntity dbAppointmentEntity  = session.get(AppointmentEntity.class, id);
            if (dbAppointmentEntity == null)
            {
                throw new Exception("Appointment not found for given Id");
            }
            copyAppointmentData(appointmentEntity, dbAppointmentEntity);
            AppointmentEntity updatedAppointment = (AppointmentEntity)session.merge(dbAppointmentEntity);
            session.update(updatedAppointment);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public void delete(AppointmentEntity appointmentEntity) throws Exception {
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            AppointmentEntity entityToDelete = session.load(AppointmentEntity.class, appointmentEntity.getAppointmentid());
            if(entityToDelete.getAppointmentStatus().equals(AppointmentStatus.BOOKED)){
                // Cannot delete booked appointments, bail out with error
                logger.error("Cannot delete appointment that are in booked status " + appointmentEntity.getAppointmentid());
                throw  new Exception("Cannot delete booked appointment, release it first");
            }else {
                session.delete(entityToDelete);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public List<AppointmentEntity> list() throws Exception {

        List<AppointmentEntity> appointmentEntityList = new ArrayList<AppointmentEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            appointmentEntityList = session.createQuery("from AppointmentEntity").list();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        return appointmentEntityList;
    }

    @Override
    public List<AppointmentEntity> listBySPLocAndAvail(Long spId, Long locId, Long startTime, Long endTime) throws Exception {
        List<AppointmentEntity> appointmentEntityList = new ArrayList<AppointmentEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {

            Query query = session.createQuery("from AppointmentEntity ap where ap.myServiceProvider.serviceproviderid = :spId and ap.myLocation.locationid = :locid " +
                    "and ap.timeSlot between :startTime and :endTime  ORDER BY ap.timeSlot ASC");
            query.setParameter("spId", spId);
            query.setParameter("locid", locId);
            query.setParameter("startTime", startTime);
            query.setParameter("endTime", endTime);
            appointmentEntityList = query.getResultList();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        return appointmentEntityList;
    }

    @Override
    public List<AppointmentEntity> listBySPLocAvailAndStatus(Long spId, Long locId, Long startTime, Long endTime, AppointmentStatus status) throws Exception{
        List<AppointmentEntity> appointmentEntityList = new ArrayList<AppointmentEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {

            Query query = session.createQuery("from AppointmentEntity ap where ap.myServiceProvider.serviceproviderid = :spId and ap.myLocation.locationid = :locid " +
                    "and ap.timeSlot between :startTime and :endTime and ap.appointmentStatus = :aptStatus ORDER BY ap.timeSlot ASC");
            query.setParameter("spId", spId);
            query.setParameter("locid", locId);
            query.setParameter("startTime", startTime);
            query.setParameter("endTime", endTime);
            query.setParameter("aptStatus", status);
            appointmentEntityList = query.getResultList();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        return appointmentEntityList;
    }

    @Override
    public List<AppointmentEntity> listAllByAvailability(Long availabilityId) throws Exception {
        List<AppointmentEntity> appointmentEntityList = new ArrayList<AppointmentEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {

            Query query = session.createQuery("from AppointmentEntity ap where ap.fromAvailability.availabilityid = :availabilityId  ORDER BY ap.timeSlot ASC");
            query.setParameter("availabilityId", availabilityId);
            appointmentEntityList = query.getResultList();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        return appointmentEntityList;
    }

    @Override
    public List<AppointmentEntity> listByAvailabilityAndAptStatus(Long availabilityId, AppointmentStatus status) throws Exception {
        List<AppointmentEntity> appointmentEntityList = new ArrayList<AppointmentEntity>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {

            Query query = session.createQuery("from AppointmentEntity ap where ap.fromAvailability.availabilityid = :availabilityId" +
                    " and ap.appointmentStatus = :appointmentStatus  ORDER BY ap.timeSlot ASC");
            query.setParameter("availabilityId", availabilityId);
            query.setParameter("appointmentStatus", status);
            appointmentEntityList = query.getResultList();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        return appointmentEntityList;
    }

    @Override
    public AppointmentEntity listById(Long id) throws Exception {
        AppointmentEntity appointmentEntity = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            appointmentEntity = session.get(AppointmentEntity.class, id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
        return appointmentEntity;
    }

    private void copyAppointmentData(
            AppointmentEntity updatedAppointmentObject,
            AppointmentEntity dbAppointmentObject)
    {
        // update possible for appointment status only
        if (updatedAppointmentObject.getAppointmentStatus().compareTo(dbAppointmentObject.getAppointmentStatus()) != 0)
        {
            dbAppointmentObject.setAppointmentStatus(updatedAppointmentObject.getAppointmentStatus());
        }

        if(updatedAppointmentObject.getMyCustomer() != null ) {
            dbAppointmentObject.setMyCustomer(updatedAppointmentObject.getMyCustomer());
        }
    }
}
