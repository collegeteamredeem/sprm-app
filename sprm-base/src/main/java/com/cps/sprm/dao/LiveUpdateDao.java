package com.cps.sprm.dao;

import com.cps.sprm.entity.LiveUpdateEntity;
import com.cps.sprm.sdk.dto.LiveUpdateV2;
import java.util.List;

/**
 * Created by sumeshs on 1/25/2017.
 */
public interface LiveUpdateDao extends HibernateBaseDao {

    void save(LiveUpdateEntity liveUpdateEntity, Long spId, Long locId) throws Exception;

    LiveUpdateV2 saveAndReturnDto(LiveUpdateEntity liveUpdateEntity, Long spId, Long locId) throws Exception;

    void update(Long id, LiveUpdateEntity liveUpdateEntity) throws Exception;

    void delete(LiveUpdateEntity liveUpdateEntity) throws Exception;

    List<LiveUpdateV2> list() throws Exception;

    LiveUpdateV2 listBySPLocAndDate(Long spId, Long locId, Long todayDate) throws Exception;

    LiveUpdateV2 listById(Long id) throws Exception;

    LiveUpdateEntity listByIdGetObject(Long id) throws Exception;
}
