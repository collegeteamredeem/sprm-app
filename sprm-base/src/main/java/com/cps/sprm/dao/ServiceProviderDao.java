package com.cps.sprm.dao;

import java.util.List;

import com.cps.sprm.entity.AvailabilityEntity;

import com.cps.sprm.entity.ServiceProviderEntity;

public interface ServiceProviderDao extends HibernateBaseDao
{

	void save(ServiceProviderEntity serviceProvider) throws Exception;

	List<ServiceProviderEntity> list() throws Exception;

	void delete(ServiceProviderEntity serviceProviderEntity) throws Exception;

	ServiceProviderEntity listById(Long id) throws Exception;

	ServiceProviderEntity listByPhoneNumber(String phoneNumber) throws Exception;

	ServiceProviderEntity updateServiceProvider(ServiceProviderEntity serviceProvider, Long serviceProviderId) throws Exception;

	ServiceProviderEntity updateSPValidity(Long serviceProviderId, Long validity) throws Exception;

	ServiceProviderEntity addAvailability(Long serviceProviderId, Long locId, AvailabilityEntity availabilityEntity) throws Exception;

	ServiceProviderEntity deleteAvailability(Long serviceProviderId, Long locId, AvailabilityEntity availabilityEntity, boolean force) throws Exception;
}
