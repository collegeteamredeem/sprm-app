package com.cps.sprm.dao;

import com.cps.sprm.entity.AppointmentEntity;
import com.cps.sprm.sdk.dto.AppointmentV2;
import com.cps.sprm.sdk.enums.AppointmentStatus;

import java.util.List;

/**
 * Created by sumeshs on 12/29/2016.
 */
public interface AppointmentDao extends HibernateBaseDao{

    void save(AppointmentEntity appointmentEntity) throws Exception;

    AppointmentV2 saveAndReturnDto(AppointmentEntity appointmentEntity) throws Exception;

    void update(Long id, AppointmentEntity appointmentEntity) throws Exception;

    void delete(AppointmentEntity appointmentEntity) throws Exception;

    List<AppointmentEntity> list() throws Exception;

    List<AppointmentEntity> listBySPLocAndAvail(Long spId, Long locId, Long startTime, Long endTime) throws Exception;

    List<AppointmentEntity> listBySPLocAvailAndStatus(Long spId, Long locId, Long startTime, Long endTime, AppointmentStatus status) throws Exception;

    List<AppointmentEntity> listAllByAvailability(Long availabilityId) throws Exception;

    List<AppointmentEntity> listByAvailabilityAndAptStatus(Long availabilityId, AppointmentStatus status) throws Exception;

    AppointmentEntity listById(Long id) throws Exception;
}
