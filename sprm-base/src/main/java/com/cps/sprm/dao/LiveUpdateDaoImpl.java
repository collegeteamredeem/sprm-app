package com.cps.sprm.dao;

import com.cps.sprm.entity.LiveUpdateEntity;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.entity.ServiceProviderEntity;
import com.cps.sprm.sdk.dto.LiveUpdateV2;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumeshs on 1/25/2017.
 */
@Repository
public class LiveUpdateDaoImpl implements LiveUpdateDao {

    private static final Logger logger = Logger.getLogger(LiveUpdateDaoImpl.class);

    DozerBeanMapper mapper;

    @Autowired
    private ServiceProviderDao serviceProviderDao;

    @Autowired
    private LocationDao locationDao;

    @Override
    public void setMapper(DozerBeanMapper mapper){
        this.mapper = mapper;
    }

    @Autowired
    private SessionFactory sessionFactory;

    public LiveUpdateDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(LiveUpdateEntity liveUpdateEntity, Long spId, Long locId) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO save called for liveupdate " + liveUpdateEntity.toString());
        }
        System.out.println("DAO sace called for liveupdate " + liveUpdateEntity.toString());
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(spId);
            LocationEntity locationEntity = locationDao.listById(locId);
            System.out.println("ServiceProvider Entity is " + serviceProviderEntity.toString());
            System.out.println("LocationEntity Entity is " + locationEntity.toString());
            session.saveOrUpdate(liveUpdateEntity);

            serviceProviderEntity.setLiveupdate(liveUpdateEntity);
            locationEntity.setLiveupdate(liveUpdateEntity);
            session.update(serviceProviderEntity);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public LiveUpdateV2 saveAndReturnDto(LiveUpdateEntity liveUpdateEntity, Long spId, Long locId) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO save called for liveupdate " + liveUpdateEntity.toString());
        }
        System.out.println("DAO sace called for liveupdate " + liveUpdateEntity.toString());
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            ServiceProviderEntity serviceProviderEntity = serviceProviderDao.listById(spId);
            LocationEntity locationEntity = locationDao.listById(locId);
            session.save(liveUpdateEntity);

            serviceProviderEntity.setLiveupdate(liveUpdateEntity);
            locationEntity.setLiveupdate(liveUpdateEntity);
            session.update(serviceProviderEntity);

            LiveUpdateV2 persistedLiveUpdate = mapper.map(liveUpdateEntity, LiveUpdateV2.class);
            return persistedLiveUpdate;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public void update(Long id, LiveUpdateEntity liveUpdateEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO update called for liveupdate " + liveUpdateEntity.toString());
        }
       System.out.println("###DAO update called for liveupdate " + liveUpdateEntity.toString());
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            LiveUpdateEntity dbLiveUpdateEntity  = session.get(LiveUpdateEntity.class, id);
            if (dbLiveUpdateEntity == null)
            {
                throw new Exception("LiveUpdate not found for given Id");
            }
            copyLiveUpdateData(liveUpdateEntity, dbLiveUpdateEntity);
            System.out.println("###After update in DB " + dbLiveUpdateEntity.toString());
            LiveUpdateEntity updatedLiveUpdate = (LiveUpdateEntity)session.merge(dbLiveUpdateEntity);
            System.out.println("###After merge in DB " + updatedLiveUpdate.toString());
            session.update(updatedLiveUpdate);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public void delete(LiveUpdateEntity liveUpdateEntity) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO delete called for liveupdate " + liveUpdateEntity.toString());
        }
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            LiveUpdateEntity entityToDelete = session.load(LiveUpdateEntity.class, liveUpdateEntity.getLiveupdateid());
            session.delete(entityToDelete);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
    }

    @Override
    public List<LiveUpdateV2> list() throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO list called for liveupdate ");
        }
        List<LiveUpdateEntity> liveUpdateEntityList = new ArrayList<LiveUpdateEntity>();
        List<LiveUpdateV2> liveUpdateV2s = new ArrayList<LiveUpdateV2>();
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            liveUpdateEntityList = session.createQuery("from LiveUpdateEntity").list();

            for(LiveUpdateEntity liveUpdateEntity : liveUpdateEntityList)
            {
                liveUpdateV2s.add(mapper.map(liveUpdateEntity, LiveUpdateV2.class));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        return liveUpdateV2s;
    }

    @Override
    public LiveUpdateEntity listByIdGetObject(Long id) throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("DAO listByIdGetObject for liveupdate " + id);
        }
        LiveUpdateEntity liveUpdateEntity = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            liveUpdateEntity = session.get(LiveUpdateEntity.class, id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
      return liveUpdateEntity;
    }

    @Override
    public LiveUpdateV2 listById(Long id) throws Exception {

        if(logger.isDebugEnabled()){
            logger.debug("DAO listBy Id for liveupdate " + id);
        }
        LiveUpdateEntity liveUpdateEntity = null;
        LiveUpdateV2 liveUpdateV2 = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {
            liveUpdateEntity = session.get(LiveUpdateEntity.class, id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw  ex;
        }
        if(null != liveUpdateEntity){
            liveUpdateV2 = mapper.map(liveUpdateEntity, LiveUpdateV2.class);
        }
        return liveUpdateV2;

    }

    @Override
    public LiveUpdateV2 listBySPLocAndDate(Long spId, Long locId, Long todayDate) throws Exception{
        LiveUpdateEntity liveUpdateEntity = null;
        LiveUpdateV2 liveUpdateV2 = null;
        Session session = this.sessionFactory.getCurrentSession();
        try
        {

            Query query = session.createQuery("from LiveUpdateEntity lup where lup.myServiceProvider.serviceproviderid = :spId and lup.myLocation.locationid = :locid and " +
                    " lup.todayDate = :date");
            query.setParameter("spId", spId);
            query.setParameter("locid", locId);
            query.setParameter("date", todayDate);
            liveUpdateEntity = (LiveUpdateEntity) query.getSingleResult();
        }
        catch (javax.persistence.NoResultException e){
            logger.debug("Did not find Live updateEntity for the query");
            return  null;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        if(null != liveUpdateEntity){
            liveUpdateV2 = mapper.map(liveUpdateEntity, LiveUpdateV2.class);
        }
        return liveUpdateV2;
    }

    private void copyLiveUpdateData(
            LiveUpdateEntity updatedLiveUpdateObject,
            LiveUpdateEntity dbLiveUpdateObject)
    {
        if (updatedLiveUpdateObject.getCurrentToken() != null)
        {
            dbLiveUpdateObject.setCurrentToken(updatedLiveUpdateObject.getCurrentToken());
        }
        if (updatedLiveUpdateObject.getTotalTokens() != null ){
            dbLiveUpdateObject.setTotalTokens(updatedLiveUpdateObject.getTotalTokens());
        }
        if (updatedLiveUpdateObject.getAvgConsultingTime() != null ){
            dbLiveUpdateObject.setAvgConsultingTime(updatedLiveUpdateObject.getAvgConsultingTime());
        }
        if(updatedLiveUpdateObject.getConsultingEndTime() != null){
            dbLiveUpdateObject.setConsultingEndTime(updatedLiveUpdateObject.getConsultingEndTime());
        }

    }
}
