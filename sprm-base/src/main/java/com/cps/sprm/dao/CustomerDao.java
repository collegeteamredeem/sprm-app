package com.cps.sprm.dao;

import com.cps.sprm.entity.CustomerEntity;
import com.cps.sprm.entity.ServiceProviderEntity;

import java.util.List;

/**
 * Created by sumeshs on 11/8/2016.
 */
public interface CustomerDao extends HibernateBaseDao
{
    void save(CustomerEntity customerEntity) throws Exception;

    void update(Long id, CustomerEntity customerEntity) throws Exception;

    void delete(CustomerEntity customerEntity) throws Exception;

    List<CustomerEntity> list() throws Exception;

    CustomerEntity listById(Long id) throws Exception;

    CustomerEntity listByPhoneNumber(String phoneNumber) throws Exception;

    void addToFavourites(Long id, ServiceProviderEntity entity) throws Exception;

    void delFromFavourites(Long id, ServiceProviderEntity entity) throws Exception;
}

