package com.cps.sprm.dao;

import java.time.LocalDateTime;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cps.sprm.customExceptions.AptNotFreeException;
import com.cps.sprm.entity.AppointmentEntity;
import com.cps.sprm.entity.AvailabilityEntity;
import com.cps.sprm.entity.LocationEntity;
import com.cps.sprm.sdk.enums.AppointmentStatus;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cps.sprm.entity.ServiceProviderEntity;

@Repository
public class ServiceProviderDaoImpl implements ServiceProviderDao
{

	private static final Logger logger = Logger.getLogger(ServiceProviderDaoImpl.class);

	@Autowired
	private AppointmentDao appointmentDao;

	@Autowired
	private SessionFactory sessionFactory;

	public ServiceProviderDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	DozerBeanMapper mapper;

	@Override
	public void setMapper(DozerBeanMapper mapper) {
		this.mapper = mapper;
	}

	public void save(ServiceProviderEntity serviceProvider) throws Exception {
		// TODO : Singleton or Cache
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			session.save(serviceProvider);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}

	public List<ServiceProviderEntity> list() throws Exception
	{
		List<ServiceProviderEntity> serviceProviderList = new ArrayList<ServiceProviderEntity>();
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			serviceProviderList = session.createQuery("from ServiceProviderEntity").list();
			return serviceProviderList;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}

	public ServiceProviderEntity listById(Long id) throws Exception
	{
		ServiceProviderEntity serviceProviderEntity = null;
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			serviceProviderEntity = session.get(ServiceProviderEntity.class, id);
			return serviceProviderEntity;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw  ex;
		}
	}

	@Override
	public ServiceProviderEntity listByPhoneNumber(String phoneNumber) throws Exception {
		String hql = "from ServiceProviderEntity where phonenumber=:phoneNumber";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("phoneNumber", phoneNumber);
		@SuppressWarnings("unchecked")
		List<ServiceProviderEntity> listSps = (List<ServiceProviderEntity>) query.list();

		if (listSps != null && !listSps.isEmpty()) {
			return listSps.get(0);
		}

		return null;
	}



	public ServiceProviderEntity updateServiceProvider(
			ServiceProviderEntity serviceProviderUpdateObj, Long serviceProviderId) throws Exception
	{
		ServiceProviderEntity serviceProviderEntity;
		Session session = null;
		try
		{
			session = this.sessionFactory.getCurrentSession();
			// first get service provider by id
			serviceProviderEntity  = session.get(ServiceProviderEntity.class, serviceProviderId);
			if (serviceProviderEntity == null)
			{
				throw new Exception("Service Provider not found for given Id");
			}
			copyServiceProviderData(serviceProviderUpdateObj, serviceProviderEntity);
			session.update(serviceProviderEntity);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		return serviceProviderEntity;
	}

	public void delete(ServiceProviderEntity serviceProviderEntity) throws Exception{
		// TODO : Singleton or Cache
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			ServiceProviderEntity entityToDelete = session.load(ServiceProviderEntity.class, serviceProviderEntity.getServiceproviderid());
			entityToDelete.getMyappointments().clear();
			entityToDelete.getSpecializations().clear();
			entityToDelete.getCustomers().clear();
			entityToDelete.getMyavailability().clear();
			entityToDelete.getLocations().clear();
			session.delete(entityToDelete);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw  ex;
		}
	}

	public ServiceProviderEntity updateSPValidity(Long serviceProviderId, Long validity) throws Exception {
		ServiceProviderEntity serviceProviderEntity ;
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			serviceProviderEntity = session.load(ServiceProviderEntity.class, serviceProviderId);
			Long oldValidity = serviceProviderEntity.getValidity();

			LocalDateTime oldValidityDateTime = java.time.LocalDateTime.ofInstant(Instant.ofEpochMilli(oldValidity), ZoneId.of("UTC"));
			LocalDateTime newValidityDateTime = oldValidityDateTime.plusMonths(validity);
			Long newValidity = newValidityDateTime.toEpochSecond(ZoneOffset.UTC) * 1000;

			serviceProviderEntity.setValidity(newValidity);
			session.update(serviceProviderEntity);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw  ex;
		}
		return serviceProviderEntity;
	}

	public ServiceProviderEntity addAvailability(Long serviceProviderId, Long locId, AvailabilityEntity availabilityEntity) throws Exception{
		ServiceProviderEntity serviceProviderEntity ;
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			serviceProviderEntity = session.load(ServiceProviderEntity.class, serviceProviderId);
			availabilityEntity.setMyserviceprovider(serviceProviderEntity);
			for(Iterator<LocationEntity> lIter = serviceProviderEntity.getLocations().iterator(); lIter.hasNext();){
				LocationEntity locationEntity = lIter.next();
				if(locationEntity.getLocationid().equals(locId)){
					logger.debug("Found location with id " + locId);
					locationEntity.addToMyavailability(availabilityEntity);
				}
			}
			serviceProviderEntity.addToMyavailability(availabilityEntity);
			session.save(serviceProviderEntity);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw  ex;
		}
		return serviceProviderEntity;
	}

	//TODO - add support for checking booked appointments, under this availability
	public ServiceProviderEntity deleteAvailability(Long serviceProviderId, Long locId, AvailabilityEntity availabilityEntity, boolean force) throws Exception{
		ServiceProviderEntity serviceProviderEntity ;
		Session session = this.sessionFactory.getCurrentSession();
		try
		{
			serviceProviderEntity = session.load(ServiceProviderEntity.class, serviceProviderId);
			AvailabilityEntity availabilityEntityToDelete = session.load(AvailabilityEntity.class, availabilityEntity.getAvailabilityid());
			List<AppointmentEntity> appointmentsForThisAvail = appointmentDao.listByAvailabilityAndAptStatus(availabilityEntityToDelete.getAvailabilityid(), AppointmentStatus.BOOKED);
			if(!force && appointmentsForThisAvail.size() > 0){
				throw new AptNotFreeException("This availability contains many booked appointments, cannot delete");
			}
			serviceProviderEntity.delFromMyavailability(availabilityEntityToDelete);
			session.save(serviceProviderEntity);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw  ex;
		}
		return serviceProviderEntity;
	}


	private void copyServiceProviderData(
			ServiceProviderEntity serviceProviderUpdateObj,
			ServiceProviderEntity serviceProviderEntity)
	{
		if (serviceProviderUpdateObj.getEmail() != null && !serviceProviderUpdateObj.getEmail().trim().isEmpty())
		{
			serviceProviderEntity.setEmail(serviceProviderUpdateObj.getEmail());
		}
		if (serviceProviderUpdateObj.getName() != null && !serviceProviderUpdateObj.getName().trim().isEmpty())
		{
			serviceProviderEntity.setName(serviceProviderUpdateObj.getName());
		}
		if (serviceProviderUpdateObj.getAssistantonephoneno() != null &&
				!serviceProviderUpdateObj.getAssistantonephoneno().trim().isEmpty())
		{
			serviceProviderEntity.setAssistantonephoneno(serviceProviderUpdateObj.getAssistantonephoneno());
		}
		if (serviceProviderUpdateObj.getAssistanttwophoneno() != null &&
				!serviceProviderUpdateObj.getAssistanttwophoneno().trim().isEmpty())
		{
			serviceProviderEntity.setAssistanttwophoneno(serviceProviderUpdateObj.getAssistanttwophoneno());
		}
		if (serviceProviderUpdateObj.getExperience() > 0)
		{
			serviceProviderEntity.setExperience(serviceProviderUpdateObj.getExperience());
		}
		if (serviceProviderUpdateObj.getSpecializations() != null && !serviceProviderUpdateObj.getSpecializations().isEmpty())
		{
			serviceProviderEntity.setSpecializations(serviceProviderUpdateObj.getSpecializations());
		}
	}
}
